# Peertube::MRSSPeerLink

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**href** | **String** |  | [optional] 
**type** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::MRSSPeerLink.new(href: null,
                                 type: null)
```


