# Peertube::InlineObject3

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**new_video_from_subscription** | [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**new_comment_on_my_video** | [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**abuse_as_moderator** | [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**video_auto_blacklist_as_moderator** | [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**blacklist_on_my_video** | [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**my_video_published** | [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**my_video_import_finished** | [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**new_follow** | [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**new_user_registration** | [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**comment_mention** | [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**new_instance_follower** | [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 
**auto_instance_following** | [**NotificationSettingValue**](NotificationSettingValue.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject3.new(new_video_from_subscription: null,
                                 new_comment_on_my_video: null,
                                 abuse_as_moderator: null,
                                 video_auto_blacklist_as_moderator: null,
                                 blacklist_on_my_video: null,
                                 my_video_published: null,
                                 my_video_import_finished: null,
                                 new_follow: null,
                                 new_user_registration: null,
                                 comment_mention: null,
                                 new_instance_follower: null,
                                 auto_instance_following: null)
```


