# Peertube::ServerConfigCustomCachePreviews

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**size** | **Integer** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigCustomCachePreviews.new(size: null)
```


