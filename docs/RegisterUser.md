# Peertube::RegisterUser

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** | The username of the user | 
**password** | **String** | The password of the user | 
**email** | **String** | The email of the user | 
**display_name** | **String** | The user display name | [optional] 
**channel** | [**RegisterUserChannel**](RegisterUserChannel.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::RegisterUser.new(username: null,
                                 password: null,
                                 email: null,
                                 display_name: null,
                                 channel: null)
```


