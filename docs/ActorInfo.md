# Peertube::ActorInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**name** | **String** |  | [optional] 
**display_name** | **String** |  | [optional] 
**host** | **String** |  | [optional] 
**avatar** | [**ActorInfoAvatar**](ActorInfoAvatar.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ActorInfo.new(id: 11,
                                 name: null,
                                 display_name: null,
                                 host: null,
                                 avatar: null)
```


