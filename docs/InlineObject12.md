# Peertube::InlineObject12

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**display_name** | **String** | Video playlist display name | 
**thumbnailfile** | **File** | Video playlist thumbnail file | [optional] 
**privacy** | [**VideoPlaylistPrivacySet**](VideoPlaylistPrivacySet.md) |  | [optional] 
**description** | **String** | Video playlist description | [optional] 
**video_channel_id** | **Integer** | Video channel in which the playlist will be published | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject12.new(display_name: null,
                                 thumbnailfile: null,
                                 privacy: null,
                                 description: null,
                                 video_channel_id: null)
```


