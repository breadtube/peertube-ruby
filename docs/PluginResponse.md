# Peertube::PluginResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Integer** |  | [optional] 
**data** | [**Array&lt;Plugin&gt;**](Plugin.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::PluginResponse.new(total: 1,
                                 data: null)
```


