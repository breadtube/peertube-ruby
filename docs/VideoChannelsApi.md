# Peertube::VideoChannelsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accounts_name_video_channels_get**](VideoChannelsApi.md#accounts_name_video_channels_get) | **GET** /accounts/{name}/video-channels | List video channels of an account
[**video_channels_channel_handle_delete**](VideoChannelsApi.md#video_channels_channel_handle_delete) | **DELETE** /video-channels/{channelHandle} | Delete a video channel
[**video_channels_channel_handle_get**](VideoChannelsApi.md#video_channels_channel_handle_get) | **GET** /video-channels/{channelHandle} | Get a video channel
[**video_channels_channel_handle_put**](VideoChannelsApi.md#video_channels_channel_handle_put) | **PUT** /video-channels/{channelHandle} | Update a video channel
[**video_channels_channel_handle_videos_get**](VideoChannelsApi.md#video_channels_channel_handle_videos_get) | **GET** /video-channels/{channelHandle}/videos | List videos of a video channel
[**video_channels_get**](VideoChannelsApi.md#video_channels_get) | **GET** /video-channels | List video channels
[**video_channels_post**](VideoChannelsApi.md#video_channels_post) | **POST** /video-channels | Create a video channel



## accounts_name_video_channels_get

> Array&lt;VideoChannel&gt; accounts_name_video_channels_get(name, opts)

List video channels of an account

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoChannelsApi.new
name = 'chocobozzz | chocobozzz@example.org' # String | The name of the account
opts = {
  with_stats: true, # Boolean | include view statistics for the last 30 days (only if authentified as the account user)
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: '-createdAt' # String | Sort column
}

begin
  #List video channels of an account
  result = api_instance.accounts_name_video_channels_get(name, opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoChannelsApi->accounts_name_video_channels_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| The name of the account | 
 **with_stats** | **Boolean**| include view statistics for the last 30 days (only if authentified as the account user) | [optional] 
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort column | [optional] 

### Return type

[**Array&lt;VideoChannel&gt;**](VideoChannel.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## video_channels_channel_handle_delete

> video_channels_channel_handle_delete(channel_handle)

Delete a video channel

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoChannelsApi.new
channel_handle = 'my_username | my_username@example.com' # String | The video channel handle

begin
  #Delete a video channel
  api_instance.video_channels_channel_handle_delete(channel_handle)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoChannelsApi->video_channels_channel_handle_delete: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_handle** | **String**| The video channel handle | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## video_channels_channel_handle_get

> VideoChannel video_channels_channel_handle_get(channel_handle)

Get a video channel

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoChannelsApi.new
channel_handle = 'my_username | my_username@example.com' # String | The video channel handle

begin
  #Get a video channel
  result = api_instance.video_channels_channel_handle_get(channel_handle)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoChannelsApi->video_channels_channel_handle_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_handle** | **String**| The video channel handle | 

### Return type

[**VideoChannel**](VideoChannel.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## video_channels_channel_handle_put

> video_channels_channel_handle_put(channel_handle, opts)

Update a video channel

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoChannelsApi.new
channel_handle = 'my_username | my_username@example.com' # String | The video channel handle
opts = {
  video_channel_update: Peertube::VideoChannelUpdate.new # VideoChannelUpdate | 
}

begin
  #Update a video channel
  api_instance.video_channels_channel_handle_put(channel_handle, opts)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoChannelsApi->video_channels_channel_handle_put: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_handle** | **String**| The video channel handle | 
 **video_channel_update** | [**VideoChannelUpdate**](VideoChannelUpdate.md)|  | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## video_channels_channel_handle_videos_get

> VideoListResponse video_channels_channel_handle_videos_get(channel_handle, opts)

List videos of a video channel

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoChannelsApi.new
channel_handle = 'my_username | my_username@example.com' # String | The video channel handle
opts = {
  category_one_of: Peertube::OneOfintegerarray.new, # OneOfintegerarray | category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get))
  tags_one_of: Peertube::OneOfstringarray.new, # OneOfstringarray | tag(s) of the video
  tags_all_of: Peertube::OneOfstringarray.new, # OneOfstringarray | tag(s) of the video, where all should be present in the video
  licence_one_of: Peertube::OneOfintegerarray.new, # OneOfintegerarray | licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get))
  language_one_of: Peertube::OneOfstringarray.new, # OneOfstringarray | language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use `_unknown` to filter on videos that don't have a video language
  nsfw: 'nsfw_example', # String | whether to include nsfw videos, if any
  filter: 'filter_example', # String | Special filters (local for instance) which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges) 
  skip_count: 'false', # String | if you don't need the `total` in the response
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: 'sort_example' # String | Sort videos by criteria
}

begin
  #List videos of a video channel
  result = api_instance.video_channels_channel_handle_videos_get(channel_handle, opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoChannelsApi->video_channels_channel_handle_videos_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_handle** | **String**| The video channel handle | 
 **category_one_of** | [**OneOfintegerarray**](.md)| category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get)) | [optional] 
 **tags_one_of** | [**OneOfstringarray**](.md)| tag(s) of the video | [optional] 
 **tags_all_of** | [**OneOfstringarray**](.md)| tag(s) of the video, where all should be present in the video | [optional] 
 **licence_one_of** | [**OneOfintegerarray**](.md)| licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get)) | [optional] 
 **language_one_of** | [**OneOfstringarray**](.md)| language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | [optional] 
 **nsfw** | **String**| whether to include nsfw videos, if any | [optional] 
 **filter** | **String**| Special filters (local for instance) which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  | [optional] 
 **skip_count** | **String**| if you don&#39;t need the &#x60;total&#x60; in the response | [optional] [default to &#39;false&#39;]
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort videos by criteria | [optional] 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## video_channels_get

> InlineResponse2003 video_channels_get(opts)

List video channels

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoChannelsApi.new
opts = {
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: '-createdAt' # String | Sort column
}

begin
  #List video channels
  result = api_instance.video_channels_get(opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoChannelsApi->video_channels_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort column | [optional] 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## video_channels_post

> video_channels_post(opts)

Create a video channel

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoChannelsApi.new
opts = {
  video_channel_create: Peertube::VideoChannelCreate.new # VideoChannelCreate | 
}

begin
  #Create a video channel
  api_instance.video_channels_post(opts)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoChannelsApi->video_channels_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **video_channel_create** | [**VideoChannelCreate**](VideoChannelCreate.md)|  | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

