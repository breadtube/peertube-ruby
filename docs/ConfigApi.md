# Peertube::ConfigApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**config_about_get**](ConfigApi.md#config_about_get) | **GET** /config/about | Get instance \&quot;About\&quot; information
[**config_custom_delete**](ConfigApi.md#config_custom_delete) | **DELETE** /config/custom | Delete instance runtime configuration
[**config_custom_get**](ConfigApi.md#config_custom_get) | **GET** /config/custom | Get instance runtime configuration
[**config_custom_put**](ConfigApi.md#config_custom_put) | **PUT** /config/custom | Set instance runtime configuration
[**config_get**](ConfigApi.md#config_get) | **GET** /config | Get instance public configuration



## config_about_get

> ServerConfigAbout config_about_get

Get instance \"About\" information

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::ConfigApi.new

begin
  #Get instance \"About\" information
  result = api_instance.config_about_get
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling ConfigApi->config_about_get: #{e}"
end
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**ServerConfigAbout**](ServerConfigAbout.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## config_custom_delete

> config_custom_delete

Delete instance runtime configuration

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::ConfigApi.new

begin
  #Delete instance runtime configuration
  api_instance.config_custom_delete
rescue Peertube::ApiError => e
  puts "Exception when calling ConfigApi->config_custom_delete: #{e}"
end
```

### Parameters

This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## config_custom_get

> ServerConfigCustom config_custom_get

Get instance runtime configuration

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::ConfigApi.new

begin
  #Get instance runtime configuration
  result = api_instance.config_custom_get
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling ConfigApi->config_custom_get: #{e}"
end
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**ServerConfigCustom**](ServerConfigCustom.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## config_custom_put

> config_custom_put

Set instance runtime configuration

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::ConfigApi.new

begin
  #Set instance runtime configuration
  api_instance.config_custom_put
rescue Peertube::ApiError => e
  puts "Exception when calling ConfigApi->config_custom_put: #{e}"
end
```

### Parameters

This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## config_get

> ServerConfig config_get

Get instance public configuration

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::ConfigApi.new

begin
  #Get instance public configuration
  result = api_instance.config_get
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling ConfigApi->config_get: #{e}"
end
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**ServerConfig**](ServerConfig.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

