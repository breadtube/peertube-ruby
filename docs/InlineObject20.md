# Peertube::InlineObject20

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**host** | **String** | server domain to block | 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject20.new(host: null)
```


