# Peertube::InlineObject5

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  | 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject5.new(username: null)
```


