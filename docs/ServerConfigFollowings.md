# Peertube::ServerConfigFollowings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**instance** | [**ServerConfigFollowingsInstance**](ServerConfigFollowingsInstance.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigFollowings.new(instance: null)
```


