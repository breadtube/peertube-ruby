# Peertube::VideoUploadResponseVideo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**uuid** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoUploadResponseVideo.new(id: 8,
                                 uuid: 9c9de5e8-0a1e-484a-b099-e80766180a6d)
```


