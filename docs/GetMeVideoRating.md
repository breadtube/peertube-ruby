# Peertube::GetMeVideoRating

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Id of the video | 
**rating** | **Float** | Rating of the video | 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::GetMeVideoRating.new(id: null,
                                 rating: null)
```


