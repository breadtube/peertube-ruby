# Peertube::VideoUserHistory

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**current_time** | **Integer** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoUserHistory.new(current_time: null)
```


