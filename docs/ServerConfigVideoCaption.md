# Peertube::ServerConfigVideoCaption

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | [**ServerConfigVideoCaptionFile**](ServerConfigVideoCaptionFile.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigVideoCaption.new(file: null)
```


