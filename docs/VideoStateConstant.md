# Peertube::VideoStateConstant

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** | The video state (Published &#x3D; &#x60;1&#x60;, to transcode &#x3D; &#x60;2&#x60;, to import &#x3D; &#x60;3&#x60;) | [optional] 
**label** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoStateConstant.new(id: null,
                                 label: null)
```


