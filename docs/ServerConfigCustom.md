# Peertube::ServerConfigCustom

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**instance** | [**ServerConfigCustomInstance**](ServerConfigCustomInstance.md) |  | [optional] 
**theme** | [**ServerConfigCustomTheme**](ServerConfigCustomTheme.md) |  | [optional] 
**services** | [**ServerConfigCustomServices**](ServerConfigCustomServices.md) |  | [optional] 
**cache** | [**ServerConfigCustomCache**](ServerConfigCustomCache.md) |  | [optional] 
**signup** | [**ServerConfigCustomSignup**](ServerConfigCustomSignup.md) |  | [optional] 
**admin** | [**ServerConfigCustomAdmin**](ServerConfigCustomAdmin.md) |  | [optional] 
**contact_form** | [**ServerConfigEmail**](ServerConfigEmail.md) |  | [optional] 
**user** | [**ServerConfigUser**](ServerConfigUser.md) |  | [optional] 
**transcoding** | [**ServerConfigCustomTranscoding**](ServerConfigCustomTranscoding.md) |  | [optional] 
**import** | [**ServerConfigImport**](ServerConfigImport.md) |  | [optional] 
**auto_blacklist** | [**ServerConfigAutoBlacklist**](ServerConfigAutoBlacklist.md) |  | [optional] 
**followers** | [**ServerConfigCustomFollowers**](ServerConfigCustomFollowers.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigCustom.new(instance: null,
                                 theme: null,
                                 services: null,
                                 cache: null,
                                 signup: null,
                                 admin: null,
                                 contact_form: null,
                                 user: null,
                                 transcoding: null,
                                 import: null,
                                 auto_blacklist: null,
                                 followers: null)
```


