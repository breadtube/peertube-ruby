# Peertube::ServerConfigAvatarFileSize

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max** | **Integer** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigAvatarFileSize.new(max: null)
```


