# Peertube::VideoBlocksApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**videos_blacklist_get**](VideoBlocksApi.md#videos_blacklist_get) | **GET** /videos/blacklist | List video blocks
[**videos_id_blacklist_delete**](VideoBlocksApi.md#videos_id_blacklist_delete) | **DELETE** /videos/{id}/blacklist | Unblock a video by its id
[**videos_id_blacklist_post**](VideoBlocksApi.md#videos_id_blacklist_post) | **POST** /videos/{id}/blacklist | Block a video



## videos_blacklist_get

> InlineResponse2001 videos_blacklist_get(opts)

List video blocks

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoBlocksApi.new
opts = {
  type: 56, # Integer | list only blocks that match this type: - `1`: manual block - `2`: automatic block that needs review 
  search: 'search_example', # String | plain search that will match with video titles, and more
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: 'sort_example' # String | Sort blacklists by criteria
}

begin
  #List video blocks
  result = api_instance.videos_blacklist_get(opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoBlocksApi->videos_blacklist_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **Integer**| list only blocks that match this type: - &#x60;1&#x60;: manual block - &#x60;2&#x60;: automatic block that needs review  | [optional] 
 **search** | **String**| plain search that will match with video titles, and more | [optional] 
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort blacklists by criteria | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## videos_id_blacklist_delete

> videos_id_blacklist_delete(id)

Unblock a video by its id

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoBlocksApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid

begin
  #Unblock a video by its id
  api_instance.videos_id_blacklist_delete(id)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoBlocksApi->videos_id_blacklist_delete: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## videos_id_blacklist_post

> videos_id_blacklist_post(id)

Block a video

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoBlocksApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid

begin
  #Block a video
  api_instance.videos_id_blacklist_post(id)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoBlocksApi->videos_id_blacklist_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

