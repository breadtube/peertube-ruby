# Peertube::AbusesAccount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Float** | Account id to report | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::AbusesAccount.new(id: null)
```


