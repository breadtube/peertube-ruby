# Peertube::VideoResolutionConstant

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** | Video resolution (240, 360, 720 ...) | [optional] 
**label** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoResolutionConstant.new(id: 240,
                                 label: 240p)
```


