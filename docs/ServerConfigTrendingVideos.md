# Peertube::ServerConfigTrendingVideos

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**interval_days** | **Integer** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigTrendingVideos.new(interval_days: null)
```


