# Peertube::InlineObject8

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**torrentfile** | **File** | Torrent File | [optional] 
**target_url** | **String** | HTTP target URL | [optional] 
**magnet_uri** | **String** | Magnet URI | [optional] 
**channel_id** | **Integer** | Channel id that will contain this video | 
**thumbnailfile** | **File** | Video thumbnail file | [optional] 
**previewfile** | **File** | Video preview file | [optional] 
**privacy** | [**VideoPrivacySet**](VideoPrivacySet.md) |  | [optional] 
**category** | **String** | Video category | [optional] 
**licence** | **String** | Video licence | [optional] 
**language** | **String** | Video language | [optional] 
**description** | **String** | Video description | [optional] 
**wait_transcoding** | **String** | Whether or not we wait transcoding before publish the video | [optional] 
**support** | **String** | A text tell the audience how to support the video creator | [optional] 
**nsfw** | **String** | Whether or not this video contains sensitive content | [optional] 
**name** | **String** | Video name | 
**tags** | **Array&lt;String&gt;** | Video tags (maximum 5 tags each between 2 and 30 characters) | [optional] 
**comments_enabled** | **String** | Enable or disable comments for this video | [optional] 
**schedule_update** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject8.new(torrentfile: null,
                                 target_url: null,
                                 magnet_uri: null,
                                 channel_id: null,
                                 thumbnailfile: null,
                                 previewfile: null,
                                 privacy: null,
                                 category: null,
                                 licence: null,
                                 language: null,
                                 description: null,
                                 wait_transcoding: null,
                                 support: Please support my work on &lt;insert crowdfunding plateform&gt;! &lt;3,
                                 nsfw: null,
                                 name: null,
                                 tags: null,
                                 comments_enabled: null,
                                 schedule_update: null)
```


