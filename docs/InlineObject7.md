# Peertube::InlineObject7

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**videofile** | **File** | Video file | 
**channel_id** | **Integer** | Channel id that will contain this video | 
**thumbnailfile** | **File** | Video thumbnail file | [optional] 
**previewfile** | **File** | Video preview file | [optional] 
**privacy** | [**VideoPrivacySet**](VideoPrivacySet.md) |  | [optional] 
**category** | **Integer** | Video category | [optional] 
**licence** | **String** | Video licence | [optional] 
**language** | **Integer** | Video language | [optional] 
**description** | **String** | Video description | [optional] 
**wait_transcoding** | **String** | Whether or not we wait transcoding before publish the video | [optional] 
**support** | **String** | A text tell the audience how to support the video creator | [optional] 
**nsfw** | **Boolean** | Whether or not this video contains sensitive content | [optional] 
**name** | **String** | Video name | 
**tags** | **Array&lt;String&gt;** | Video tags (maximum 5 tags each between 2 and 30 characters) | [optional] 
**comments_enabled** | **Boolean** | Enable or disable comments for this video | [optional] 
**originally_published_at** | **DateTime** | Date when the content was originally published | [optional] 
**schedule_update** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject7.new(videofile: null,
                                 channel_id: null,
                                 thumbnailfile: null,
                                 previewfile: null,
                                 privacy: null,
                                 category: 4,
                                 licence: null,
                                 language: 2,
                                 description: null,
                                 wait_transcoding: null,
                                 support: Please support my work on &lt;insert crowdfunding plateform&gt;! &lt;3,
                                 nsfw: null,
                                 name: null,
                                 tags: null,
                                 comments_enabled: null,
                                 originally_published_at: null,
                                 schedule_update: null)
```


