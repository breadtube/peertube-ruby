# Peertube::AccountsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accounts_get**](AccountsApi.md#accounts_get) | **GET** /accounts | List accounts
[**accounts_name_get**](AccountsApi.md#accounts_name_get) | **GET** /accounts/{name} | Get an account
[**accounts_name_ratings_get**](AccountsApi.md#accounts_name_ratings_get) | **GET** /accounts/{name}/ratings | List ratings of an account
[**accounts_name_video_channels_get**](AccountsApi.md#accounts_name_video_channels_get) | **GET** /accounts/{name}/video-channels | List video channels of an account
[**accounts_name_videos_get**](AccountsApi.md#accounts_name_videos_get) | **GET** /accounts/{name}/videos | List videos of an account



## accounts_get

> Array&lt;Account&gt; accounts_get(opts)

List accounts

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::AccountsApi.new
opts = {
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: '-createdAt' # String | Sort column
}

begin
  #List accounts
  result = api_instance.accounts_get(opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling AccountsApi->accounts_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort column | [optional] 

### Return type

[**Array&lt;Account&gt;**](Account.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## accounts_name_get

> Account accounts_name_get(name)

Get an account

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::AccountsApi.new
name = 'chocobozzz | chocobozzz@example.org' # String | The name of the account

begin
  #Get an account
  result = api_instance.accounts_name_get(name)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling AccountsApi->accounts_name_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| The name of the account | 

### Return type

[**Account**](Account.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## accounts_name_ratings_get

> Array&lt;VideoRating&gt; accounts_name_ratings_get(name, opts)

List ratings of an account

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::AccountsApi.new
name = 'chocobozzz | chocobozzz@example.org' # String | The name of the account
opts = {
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: '-createdAt', # String | Sort column
  rating: 'rating_example' # String | Optionally filter which ratings to retrieve
}

begin
  #List ratings of an account
  result = api_instance.accounts_name_ratings_get(name, opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling AccountsApi->accounts_name_ratings_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| The name of the account | 
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort column | [optional] 
 **rating** | **String**| Optionally filter which ratings to retrieve | [optional] 

### Return type

[**Array&lt;VideoRating&gt;**](VideoRating.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## accounts_name_video_channels_get

> Array&lt;VideoChannel&gt; accounts_name_video_channels_get(name, opts)

List video channels of an account

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::AccountsApi.new
name = 'chocobozzz | chocobozzz@example.org' # String | The name of the account
opts = {
  with_stats: true, # Boolean | include view statistics for the last 30 days (only if authentified as the account user)
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: '-createdAt' # String | Sort column
}

begin
  #List video channels of an account
  result = api_instance.accounts_name_video_channels_get(name, opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling AccountsApi->accounts_name_video_channels_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| The name of the account | 
 **with_stats** | **Boolean**| include view statistics for the last 30 days (only if authentified as the account user) | [optional] 
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort column | [optional] 

### Return type

[**Array&lt;VideoChannel&gt;**](VideoChannel.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## accounts_name_videos_get

> VideoListResponse accounts_name_videos_get(name, opts)

List videos of an account

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::AccountsApi.new
name = 'chocobozzz | chocobozzz@example.org' # String | The name of the account
opts = {
  category_one_of: Peertube::OneOfintegerarray.new, # OneOfintegerarray | category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get))
  tags_one_of: Peertube::OneOfstringarray.new, # OneOfstringarray | tag(s) of the video
  tags_all_of: Peertube::OneOfstringarray.new, # OneOfstringarray | tag(s) of the video, where all should be present in the video
  licence_one_of: Peertube::OneOfintegerarray.new, # OneOfintegerarray | licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get))
  language_one_of: Peertube::OneOfstringarray.new, # OneOfstringarray | language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use `_unknown` to filter on videos that don't have a video language
  nsfw: 'nsfw_example', # String | whether to include nsfw videos, if any
  filter: 'filter_example', # String | Special filters (local for instance) which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges) 
  skip_count: 'false', # String | if you don't need the `total` in the response
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: 'sort_example' # String | Sort videos by criteria
}

begin
  #List videos of an account
  result = api_instance.accounts_name_videos_get(name, opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling AccountsApi->accounts_name_videos_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| The name of the account | 
 **category_one_of** | [**OneOfintegerarray**](.md)| category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get)) | [optional] 
 **tags_one_of** | [**OneOfstringarray**](.md)| tag(s) of the video | [optional] 
 **tags_all_of** | [**OneOfstringarray**](.md)| tag(s) of the video, where all should be present in the video | [optional] 
 **licence_one_of** | [**OneOfintegerarray**](.md)| licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get)) | [optional] 
 **language_one_of** | [**OneOfstringarray**](.md)| language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | [optional] 
 **nsfw** | **String**| whether to include nsfw videos, if any | [optional] 
 **filter** | **String**| Special filters (local for instance) which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  | [optional] 
 **skip_count** | **String**| if you don&#39;t need the &#x60;total&#x60; in the response | [optional] [default to &#39;false&#39;]
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort videos by criteria | [optional] 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

