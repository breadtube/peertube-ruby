# Peertube::ServerConfigInstanceCustomizations

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**javascript** | **String** |  | [optional] 
**css** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigInstanceCustomizations.new(javascript: null,
                                 css: null)
```


