# Peertube::MRSSGroupContent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  | [optional] 
**file_size** | **Integer** |  | [optional] 
**type** | **String** |  | [optional] 
**framerate** | **Integer** |  | [optional] 
**duration** | **Integer** |  | [optional] 
**height** | **Integer** |  | [optional] 
**lang** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::MRSSGroupContent.new(url: null,
                                 file_size: null,
                                 type: null,
                                 framerate: null,
                                 duration: null,
                                 height: null,
                                 lang: null)
```


