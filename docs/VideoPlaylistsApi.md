# Peertube::VideoPlaylistsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**users_me_video_playlists_videos_exist_get**](VideoPlaylistsApi.md#users_me_video_playlists_videos_exist_get) | **GET** /users/me/video-playlists/videos-exist | Check video exists in my playlists
[**video_playlists_get**](VideoPlaylistsApi.md#video_playlists_get) | **GET** /video-playlists | List video playlists
[**video_playlists_id_delete**](VideoPlaylistsApi.md#video_playlists_id_delete) | **DELETE** /video-playlists/{id} | Delete a video playlist
[**video_playlists_id_get**](VideoPlaylistsApi.md#video_playlists_id_get) | **GET** /video-playlists/{id} | Get a video playlist
[**video_playlists_id_put**](VideoPlaylistsApi.md#video_playlists_id_put) | **PUT** /video-playlists/{id} | Update a video playlist
[**video_playlists_id_videos_get**](VideoPlaylistsApi.md#video_playlists_id_videos_get) | **GET** /video-playlists/{id}/videos | List videos of a playlist
[**video_playlists_id_videos_playlist_element_id_delete**](VideoPlaylistsApi.md#video_playlists_id_videos_playlist_element_id_delete) | **DELETE** /video-playlists/{id}/videos/{playlistElementId} | Delete an element from a playlist
[**video_playlists_id_videos_playlist_element_id_put**](VideoPlaylistsApi.md#video_playlists_id_videos_playlist_element_id_put) | **PUT** /video-playlists/{id}/videos/{playlistElementId} | Update a playlist element
[**video_playlists_id_videos_post**](VideoPlaylistsApi.md#video_playlists_id_videos_post) | **POST** /video-playlists/{id}/videos | Add a video in a playlist
[**video_playlists_id_videos_reorder_post**](VideoPlaylistsApi.md#video_playlists_id_videos_reorder_post) | **POST** /video-playlists/{id}/videos/reorder | Reorder a playlist
[**video_playlists_post**](VideoPlaylistsApi.md#video_playlists_post) | **POST** /video-playlists | Create a video playlist
[**video_playlists_privacies_get**](VideoPlaylistsApi.md#video_playlists_privacies_get) | **GET** /video-playlists/privacies | List available playlist privacies



## users_me_video_playlists_videos_exist_get

> InlineResponse2007 users_me_video_playlists_videos_exist_get(video_ids)

Check video exists in my playlists

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoPlaylistsApi.new
video_ids = [56] # Array<Integer> | The video ids to check

begin
  #Check video exists in my playlists
  result = api_instance.users_me_video_playlists_videos_exist_get(video_ids)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoPlaylistsApi->users_me_video_playlists_videos_exist_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **video_ids** | [**Array&lt;Integer&gt;**](Integer.md)| The video ids to check | 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## video_playlists_get

> InlineResponse2004 video_playlists_get(opts)

List video playlists

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoPlaylistsApi.new
opts = {
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: '-createdAt' # String | Sort column
}

begin
  #List video playlists
  result = api_instance.video_playlists_get(opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoPlaylistsApi->video_playlists_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort column | [optional] 

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## video_playlists_id_delete

> video_playlists_id_delete(id)

Delete a video playlist

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoPlaylistsApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid

begin
  #Delete a video playlist
  api_instance.video_playlists_id_delete(id)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoPlaylistsApi->video_playlists_id_delete: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## video_playlists_id_get

> VideoPlaylist video_playlists_id_get(id)

Get a video playlist

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoPlaylistsApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid

begin
  #Get a video playlist
  result = api_instance.video_playlists_id_get(id)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoPlaylistsApi->video_playlists_id_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

[**VideoPlaylist**](VideoPlaylist.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## video_playlists_id_put

> video_playlists_id_put(id, opts)

Update a video playlist

If the video playlist is set as public, the playlist must have a assigned channel.

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoPlaylistsApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid
opts = {
  display_name: 'display_name_example', # String | Video playlist display name
  thumbnailfile: File.new('/path/to/file'), # File | Video playlist thumbnail file
  privacy: Peertube::VideoPlaylistPrivacySet.new, # VideoPlaylistPrivacySet | 
  description: 'description_example', # String | Video playlist description
  video_channel_id: 56 # Integer | Video channel in which the playlist will be published
}

begin
  #Update a video playlist
  api_instance.video_playlists_id_put(id, opts)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoPlaylistsApi->video_playlists_id_put: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **display_name** | **String**| Video playlist display name | [optional] 
 **thumbnailfile** | **File**| Video playlist thumbnail file | [optional] 
 **privacy** | [**VideoPlaylistPrivacySet**](VideoPlaylistPrivacySet.md)|  | [optional] 
 **description** | **String**| Video playlist description | [optional] 
 **video_channel_id** | **Integer**| Video channel in which the playlist will be published | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: Not defined


## video_playlists_id_videos_get

> VideoListResponse video_playlists_id_videos_get(id)

List videos of a playlist

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoPlaylistsApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid

begin
  #List videos of a playlist
  result = api_instance.video_playlists_id_videos_get(id)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoPlaylistsApi->video_playlists_id_videos_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## video_playlists_id_videos_playlist_element_id_delete

> video_playlists_id_videos_playlist_element_id_delete(id, playlist_element_id)

Delete an element from a playlist

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoPlaylistsApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid
playlist_element_id = 56 # Integer | Playlist element id

begin
  #Delete an element from a playlist
  api_instance.video_playlists_id_videos_playlist_element_id_delete(id, playlist_element_id)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoPlaylistsApi->video_playlists_id_videos_playlist_element_id_delete: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **playlist_element_id** | **Integer**| Playlist element id | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## video_playlists_id_videos_playlist_element_id_put

> video_playlists_id_videos_playlist_element_id_put(id, playlist_element_id, opts)

Update a playlist element

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoPlaylistsApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid
playlist_element_id = 56 # Integer | Playlist element id
opts = {
  inline_object16: Peertube::InlineObject16.new # InlineObject16 | 
}

begin
  #Update a playlist element
  api_instance.video_playlists_id_videos_playlist_element_id_put(id, playlist_element_id, opts)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoPlaylistsApi->video_playlists_id_videos_playlist_element_id_put: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **playlist_element_id** | **Integer**| Playlist element id | 
 **inline_object16** | [**InlineObject16**](InlineObject16.md)|  | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## video_playlists_id_videos_post

> InlineResponse2006 video_playlists_id_videos_post(id, opts)

Add a video in a playlist

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoPlaylistsApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid
opts = {
  inline_object14: Peertube::InlineObject14.new # InlineObject14 | 
}

begin
  #Add a video in a playlist
  result = api_instance.video_playlists_id_videos_post(id, opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoPlaylistsApi->video_playlists_id_videos_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **inline_object14** | [**InlineObject14**](InlineObject14.md)|  | [optional] 

### Return type

[**InlineResponse2006**](InlineResponse2006.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## video_playlists_id_videos_reorder_post

> video_playlists_id_videos_reorder_post(id, opts)

Reorder a playlist

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoPlaylistsApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid
opts = {
  inline_object15: Peertube::InlineObject15.new # InlineObject15 | 
}

begin
  #Reorder a playlist
  api_instance.video_playlists_id_videos_reorder_post(id, opts)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoPlaylistsApi->video_playlists_id_videos_reorder_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **inline_object15** | [**InlineObject15**](InlineObject15.md)|  | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## video_playlists_post

> InlineResponse2005 video_playlists_post(display_name, opts)

Create a video playlist

If the video playlist is set as public, the videoChannelId is mandatory.

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoPlaylistsApi.new
display_name = 'display_name_example' # String | Video playlist display name
opts = {
  thumbnailfile: File.new('/path/to/file'), # File | Video playlist thumbnail file
  privacy: Peertube::VideoPlaylistPrivacySet.new, # VideoPlaylistPrivacySet | 
  description: 'description_example', # String | Video playlist description
  video_channel_id: 56 # Integer | Video channel in which the playlist will be published
}

begin
  #Create a video playlist
  result = api_instance.video_playlists_post(display_name, opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoPlaylistsApi->video_playlists_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **display_name** | **String**| Video playlist display name | 
 **thumbnailfile** | **File**| Video playlist thumbnail file | [optional] 
 **privacy** | [**VideoPlaylistPrivacySet**](VideoPlaylistPrivacySet.md)|  | [optional] 
 **description** | **String**| Video playlist description | [optional] 
 **video_channel_id** | **Integer**| Video channel in which the playlist will be published | [optional] 

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## video_playlists_privacies_get

> Array&lt;String&gt; video_playlists_privacies_get

List available playlist privacies

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoPlaylistsApi.new

begin
  #List available playlist privacies
  result = api_instance.video_playlists_privacies_get
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoPlaylistsApi->video_playlists_privacies_get: #{e}"
end
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Array&lt;String&gt;**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

