# Peertube::VideoChannel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**display_name** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**is_local** | **Boolean** |  | [optional] 
**owner_account** | [**VideoChannelOwnerAccount**](VideoChannelOwnerAccount.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoChannel.new(display_name: null,
                                 description: null,
                                 is_local: null,
                                 owner_account: null)
```


