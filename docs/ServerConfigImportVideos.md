# Peertube::ServerConfigImportVideos

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**http** | [**ServerConfigEmail**](ServerConfigEmail.md) |  | [optional] 
**torrent** | [**ServerConfigEmail**](ServerConfigEmail.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigImportVideos.new(http: null,
                                 torrent: null)
```


