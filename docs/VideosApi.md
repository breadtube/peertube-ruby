# Peertube::VideosApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**users_me_subscriptions_videos_get**](VideosApi.md#users_me_subscriptions_videos_get) | **GET** /users/me/subscriptions/videos | List videos of subscriptions of my user
[**users_me_videos_get**](VideosApi.md#users_me_videos_get) | **GET** /users/me/videos | Get videos of my user
[**users_me_videos_imports_get**](VideosApi.md#users_me_videos_imports_get) | **GET** /users/me/videos/imports | Get video imports of my user
[**video_playlists_id_videos_get**](VideosApi.md#video_playlists_id_videos_get) | **GET** /video-playlists/{id}/videos | List videos of a playlist
[**video_playlists_id_videos_post**](VideosApi.md#video_playlists_id_videos_post) | **POST** /video-playlists/{id}/videos | Add a video in a playlist



## users_me_subscriptions_videos_get

> VideoListResponse users_me_subscriptions_videos_get(opts)

List videos of subscriptions of my user

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideosApi.new
opts = {
  category_one_of: Peertube::OneOfintegerarray.new, # OneOfintegerarray | category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get))
  tags_one_of: Peertube::OneOfstringarray.new, # OneOfstringarray | tag(s) of the video
  tags_all_of: Peertube::OneOfstringarray.new, # OneOfstringarray | tag(s) of the video, where all should be present in the video
  licence_one_of: Peertube::OneOfintegerarray.new, # OneOfintegerarray | licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get))
  language_one_of: Peertube::OneOfstringarray.new, # OneOfstringarray | language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use `_unknown` to filter on videos that don't have a video language
  nsfw: 'nsfw_example', # String | whether to include nsfw videos, if any
  filter: 'filter_example', # String | Special filters (local for instance) which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges) 
  skip_count: 'false', # String | if you don't need the `total` in the response
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: 'sort_example' # String | Sort videos by criteria
}

begin
  #List videos of subscriptions of my user
  result = api_instance.users_me_subscriptions_videos_get(opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideosApi->users_me_subscriptions_videos_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category_one_of** | [**OneOfintegerarray**](.md)| category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get)) | [optional] 
 **tags_one_of** | [**OneOfstringarray**](.md)| tag(s) of the video | [optional] 
 **tags_all_of** | [**OneOfstringarray**](.md)| tag(s) of the video, where all should be present in the video | [optional] 
 **licence_one_of** | [**OneOfintegerarray**](.md)| licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get)) | [optional] 
 **language_one_of** | [**OneOfstringarray**](.md)| language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | [optional] 
 **nsfw** | **String**| whether to include nsfw videos, if any | [optional] 
 **filter** | **String**| Special filters (local for instance) which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  | [optional] 
 **skip_count** | **String**| if you don&#39;t need the &#x60;total&#x60; in the response | [optional] [default to &#39;false&#39;]
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort videos by criteria | [optional] 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## users_me_videos_get

> VideoListResponse users_me_videos_get(opts)

Get videos of my user

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideosApi.new
opts = {
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: '-createdAt' # String | Sort column
}

begin
  #Get videos of my user
  result = api_instance.users_me_videos_get(opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideosApi->users_me_videos_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort column | [optional] 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## users_me_videos_imports_get

> VideoImport users_me_videos_imports_get(opts)

Get video imports of my user

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideosApi.new
opts = {
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: '-createdAt' # String | Sort column
}

begin
  #Get video imports of my user
  result = api_instance.users_me_videos_imports_get(opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideosApi->users_me_videos_imports_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort column | [optional] 

### Return type

[**VideoImport**](VideoImport.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## video_playlists_id_videos_get

> VideoListResponse video_playlists_id_videos_get(id)

List videos of a playlist

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideosApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid

begin
  #List videos of a playlist
  result = api_instance.video_playlists_id_videos_get(id)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideosApi->video_playlists_id_videos_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## video_playlists_id_videos_post

> InlineResponse2006 video_playlists_id_videos_post(id, opts)

Add a video in a playlist

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideosApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid
opts = {
  inline_object14: Peertube::InlineObject14.new # InlineObject14 | 
}

begin
  #Add a video in a playlist
  result = api_instance.video_playlists_id_videos_post(id, opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideosApi->video_playlists_id_videos_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **inline_object14** | [**InlineObject14**](InlineObject14.md)|  | [optional] 

### Return type

[**InlineResponse2006**](InlineResponse2006.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

