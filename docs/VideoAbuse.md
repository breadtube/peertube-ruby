# Peertube::VideoAbuse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**reason** | **String** |  | [optional] 
**predefined_reasons** | **Array&lt;String&gt;** |  | [optional] 
**reporter_account** | [**Account**](Account.md) |  | [optional] 
**state** | [**AbuseStateConstant**](AbuseStateConstant.md) |  | [optional] 
**moderation_comment** | **String** |  | [optional] 
**video** | [**VideoAbuseVideo**](VideoAbuseVideo.md) |  | [optional] 
**created_at** | **DateTime** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoAbuse.new(id: 7,
                                 reason: The video is a spam,
                                 predefined_reasons: [&quot;spamOrMisleading&quot;],
                                 reporter_account: null,
                                 state: null,
                                 moderation_comment: Decided to ban the server since it spams us regularly,
                                 video: null,
                                 created_at: null)
```


