# Peertube::VideoPlaylist

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 
**description** | **String** |  | [optional] 
**uuid** | **String** |  | [optional] 
**display_name** | **String** |  | [optional] 
**is_local** | **Boolean** |  | [optional] 
**video_length** | **Integer** |  | [optional] 
**thumbnail_path** | **String** |  | [optional] 
**privacy** | [**VideoPlaylistPrivacyConstant**](VideoPlaylistPrivacyConstant.md) |  | [optional] 
**type** | [**VideoPlaylistTypeConstant**](VideoPlaylistTypeConstant.md) |  | [optional] 
**owner_account** | [**AccountSummary**](AccountSummary.md) |  | [optional] 
**video_channel** | [**VideoChannelSummary**](VideoChannelSummary.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoPlaylist.new(id: null,
                                 created_at: null,
                                 updated_at: null,
                                 description: null,
                                 uuid: 9c9de5e8-0a1e-484a-b099-e80766180a6d,
                                 display_name: null,
                                 is_local: null,
                                 video_length: null,
                                 thumbnail_path: null,
                                 privacy: null,
                                 type: null,
                                 owner_account: null,
                                 video_channel: null)
```


