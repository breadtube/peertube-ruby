# Peertube::InlineResponse2001

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Integer** |  | [optional] 
**data** | [**Array&lt;VideoBlacklist&gt;**](VideoBlacklist.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineResponse2001.new(total: 1,
                                 data: null)
```


