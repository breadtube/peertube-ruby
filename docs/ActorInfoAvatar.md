# Peertube::ActorInfoAvatar

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ActorInfoAvatar.new(path: null)
```


