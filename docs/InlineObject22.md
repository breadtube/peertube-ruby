# Peertube::InlineObject22

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**video_id** | **Integer** |  | 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject22.new(video_id: null)
```


