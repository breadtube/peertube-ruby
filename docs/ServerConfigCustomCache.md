# Peertube::ServerConfigCustomCache

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**previews** | [**ServerConfigCustomCachePreviews**](ServerConfigCustomCachePreviews.md) |  | [optional] 
**captions** | [**ServerConfigCustomCachePreviews**](ServerConfigCustomCachePreviews.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigCustomCache.new(previews: null,
                                 captions: null)
```


