# Peertube::InlineObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hosts** | **Array&lt;String&gt;** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject.new(hosts: null)
```


