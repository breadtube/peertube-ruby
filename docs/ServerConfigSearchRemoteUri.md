# Peertube::ServerConfigSearchRemoteUri

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**users** | **Boolean** |  | [optional] 
**anonymous** | **Boolean** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigSearchRemoteUri.new(users: null,
                                 anonymous: null)
```


