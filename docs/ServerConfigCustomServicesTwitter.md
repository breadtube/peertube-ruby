# Peertube::ServerConfigCustomServicesTwitter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  | [optional] 
**whitelisted** | **Boolean** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigCustomServicesTwitter.new(username: null,
                                 whitelisted: null)
```


