# Peertube::InlineObject2

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ids** | **Array&lt;Integer&gt;** | ids of the notifications to mark as read | 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject2.new(ids: null)
```


