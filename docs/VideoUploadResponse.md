# Peertube::VideoUploadResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**video** | [**VideoUploadResponseVideo**](VideoUploadResponseVideo.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoUploadResponse.new(video: null)
```


