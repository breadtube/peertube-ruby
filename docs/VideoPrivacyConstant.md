# Peertube::VideoPrivacyConstant

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**VideoPrivacySet**](VideoPrivacySet.md) |  | [optional] 
**label** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoPrivacyConstant.new(id: null,
                                 label: null)
```


