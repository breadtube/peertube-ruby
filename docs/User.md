# Peertube::User

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] [readonly] 
**username** | **String** | The user username | [optional] 
**email** | **String** | The user email | [optional] 
**theme** | **String** | Theme enabled by this user | [optional] 
**email_verified** | **Boolean** | Has the user confirmed their email address? | [optional] 
**nsfw_policy** | [**NSFWPolicy**](NSFWPolicy.md) |  | [optional] 
**webtorrent_enabled** | **Boolean** | Enable P2P in the player | [optional] 
**auto_play_video** | **Boolean** | Automatically start playing the video on the watch page | [optional] 
**role** | [**UserRole**](UserRole.md) |  | [optional] 
**role_label** | **String** |  | [optional] 
**video_quota** | **Integer** | The user video quota | [optional] 
**video_quota_daily** | **Integer** | The user daily video quota | [optional] 
**videos_count** | **Integer** |  | [optional] 
**abuses_count** | **Integer** |  | [optional] 
**abuses_accepted_count** | **Integer** |  | [optional] 
**abuses_created_count** | **Integer** |  | [optional] 
**video_comments_count** | **Integer** |  | [optional] 
**no_instance_config_warning_modal** | **Boolean** |  | [optional] 
**no_welcome_modal** | **Boolean** |  | [optional] 
**blocked** | **Boolean** |  | [optional] 
**blocked_reason** | **String** |  | [optional] 
**created_at** | **String** |  | [optional] 
**account** | [**Account**](Account.md) |  | [optional] 
**video_channels** | [**Array&lt;VideoChannel&gt;**](VideoChannel.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::User.new(id: null,
                                 username: null,
                                 email: null,
                                 theme: null,
                                 email_verified: null,
                                 nsfw_policy: null,
                                 webtorrent_enabled: null,
                                 auto_play_video: null,
                                 role: null,
                                 role_label: null,
                                 video_quota: null,
                                 video_quota_daily: null,
                                 videos_count: null,
                                 abuses_count: null,
                                 abuses_accepted_count: null,
                                 abuses_created_count: null,
                                 video_comments_count: null,
                                 no_instance_config_warning_modal: null,
                                 no_welcome_modal: null,
                                 blocked: null,
                                 blocked_reason: null,
                                 created_at: null,
                                 account: null,
                                 video_channels: null)
```


