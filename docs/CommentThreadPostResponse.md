# Peertube::CommentThreadPostResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**comment** | [**VideoComment**](VideoComment.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::CommentThreadPostResponse.new(comment: null)
```


