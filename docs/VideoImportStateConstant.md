# Peertube::VideoImportStateConstant

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** | The video import state (Pending &#x3D; &#x60;1&#x60;, Success &#x3D; &#x60;2&#x60;, Failed &#x3D; &#x60;3&#x60;) | [optional] 
**label** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoImportStateConstant.new(id: null,
                                 label: Pending)
```


