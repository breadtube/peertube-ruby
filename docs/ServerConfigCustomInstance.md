# Peertube::ServerConfigCustomInstance

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**short_description** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**terms** | **String** |  | [optional] 
**default_client_route** | **String** |  | [optional] 
**is_nsfw** | **Boolean** |  | [optional] 
**default_nsfw_policy** | **String** |  | [optional] 
**customizations** | [**ServerConfigInstanceCustomizations**](ServerConfigInstanceCustomizations.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigCustomInstance.new(name: null,
                                 short_description: null,
                                 description: null,
                                 terms: null,
                                 default_client_route: null,
                                 is_nsfw: null,
                                 default_nsfw_policy: null,
                                 customizations: null)
```


