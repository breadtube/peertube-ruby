# Peertube::InlineObject9

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reason** | **String** | Reason why the user reports this video | 
**predefined_reasons** | **Array&lt;String&gt;** | Reason categories that help triage reports | [optional] 
**video** | [**AbusesVideo**](AbusesVideo.md) |  | [optional] 
**comment** | [**AbusesComment**](AbusesComment.md) |  | [optional] 
**account** | [**AbusesAccount**](AbusesAccount.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject9.new(reason: null,
                                 predefined_reasons: null,
                                 video: null,
                                 comment: null,
                                 account: null)
```


