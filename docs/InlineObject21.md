# Peertube::InlineObject21

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**redundancy_allowed** | **Boolean** | allow mirroring of the host&#39;s local videos | 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject21.new(redundancy_allowed: null)
```


