# Peertube::AbusesApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**abuses_abuse_id_delete**](AbusesApi.md#abuses_abuse_id_delete) | **DELETE** /abuses/{abuseId} | Delete an abuse
[**abuses_abuse_id_put**](AbusesApi.md#abuses_abuse_id_put) | **PUT** /abuses/{abuseId} | Update an abuse
[**abuses_get**](AbusesApi.md#abuses_get) | **GET** /abuses | List abuses
[**abuses_post**](AbusesApi.md#abuses_post) | **POST** /abuses | Report an abuse



## abuses_abuse_id_delete

> abuses_abuse_id_delete(abuse_id)

Delete an abuse

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::AbusesApi.new
abuse_id = 56 # Integer | Abuse id

begin
  #Delete an abuse
  api_instance.abuses_abuse_id_delete(abuse_id)
rescue Peertube::ApiError => e
  puts "Exception when calling AbusesApi->abuses_abuse_id_delete: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **abuse_id** | **Integer**| Abuse id | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## abuses_abuse_id_put

> abuses_abuse_id_put(abuse_id, opts)

Update an abuse

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::AbusesApi.new
abuse_id = 56 # Integer | Abuse id
opts = {
  inline_object10: Peertube::InlineObject10.new # InlineObject10 | 
}

begin
  #Update an abuse
  api_instance.abuses_abuse_id_put(abuse_id, opts)
rescue Peertube::ApiError => e
  puts "Exception when calling AbusesApi->abuses_abuse_id_put: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **abuse_id** | **Integer**| Abuse id | 
 **inline_object10** | [**InlineObject10**](InlineObject10.md)|  | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## abuses_get

> Array&lt;VideoAbuse&gt; abuses_get(opts)

List abuses

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::AbusesApi.new
opts = {
  id: 56, # Integer | only list the report with this id
  predefined_reason: ['predefined_reason_example'], # Array<String> | predefined reason the listed reports should contain
  search: 'search_example', # String | plain search that will match with video titles, reporter names and more
  state: 56, # Integer | The abuse state (Pending = `1`, Rejected = `2`, Accepted = `3`)
  search_reporter: 'search_reporter_example', # String | only list reports of a specific reporter
  search_reportee: 'search_reportee_example', # String | only list reports of a specific reportee
  search_video: 'search_video_example', # String | only list reports of a specific video
  search_video_channel: 'search_video_channel_example', # String | only list reports of a specific video channel
  video_is: 'video_is_example', # String | only list blacklisted or deleted videos
  filter: 'filter_example', # String | only list account, comment or video reports
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: 'sort_example' # String | Sort abuses by criteria
}

begin
  #List abuses
  result = api_instance.abuses_get(opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling AbusesApi->abuses_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| only list the report with this id | [optional] 
 **predefined_reason** | [**Array&lt;String&gt;**](String.md)| predefined reason the listed reports should contain | [optional] 
 **search** | **String**| plain search that will match with video titles, reporter names and more | [optional] 
 **state** | **Integer**| The abuse state (Pending &#x3D; &#x60;1&#x60;, Rejected &#x3D; &#x60;2&#x60;, Accepted &#x3D; &#x60;3&#x60;) | [optional] 
 **search_reporter** | **String**| only list reports of a specific reporter | [optional] 
 **search_reportee** | **String**| only list reports of a specific reportee | [optional] 
 **search_video** | **String**| only list reports of a specific video | [optional] 
 **search_video_channel** | **String**| only list reports of a specific video channel | [optional] 
 **video_is** | **String**| only list blacklisted or deleted videos | [optional] 
 **filter** | **String**| only list account, comment or video reports | [optional] 
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort abuses by criteria | [optional] 

### Return type

[**Array&lt;VideoAbuse&gt;**](VideoAbuse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## abuses_post

> abuses_post(inline_object9)

Report an abuse

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::AbusesApi.new
inline_object9 = Peertube::InlineObject9.new # InlineObject9 | 

begin
  #Report an abuse
  api_instance.abuses_post(inline_object9)
rescue Peertube::ApiError => e
  puts "Exception when calling AbusesApi->abuses_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object9** | [**InlineObject9**](InlineObject9.md)|  | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

