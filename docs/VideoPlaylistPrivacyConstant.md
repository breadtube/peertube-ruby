# Peertube::VideoPlaylistPrivacyConstant

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**VideoPlaylistPrivacySet**](VideoPlaylistPrivacySet.md) |  | [optional] 
**label** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoPlaylistPrivacyConstant.new(id: null,
                                 label: null)
```


