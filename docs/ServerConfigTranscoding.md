# Peertube::ServerConfigTranscoding

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hls** | [**ServerConfigEmail**](ServerConfigEmail.md) |  | [optional] 
**webtorrent** | [**ServerConfigEmail**](ServerConfigEmail.md) |  | [optional] 
**enabled_resolutions** | **Array&lt;Integer&gt;** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigTranscoding.new(hls: null,
                                 webtorrent: null,
                                 enabled_resolutions: null)
```


