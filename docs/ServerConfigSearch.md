# Peertube::ServerConfigSearch

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**remote_uri** | [**ServerConfigSearchRemoteUri**](ServerConfigSearchRemoteUri.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigSearch.new(remote_uri: null)
```


