# Peertube::VideoListResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Integer** |  | [optional] 
**data** | [**Array&lt;Video&gt;**](Video.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoListResponse.new(total: 1,
                                 data: null)
```


