# Peertube::InlineResponse2006VideoPlaylistElement

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineResponse2006VideoPlaylistElement.new(id: null)
```


