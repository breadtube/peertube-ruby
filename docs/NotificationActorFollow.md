# Peertube::NotificationActorFollow

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**follower** | [**ActorInfo**](ActorInfo.md) |  | [optional] 
**state** | **String** |  | [optional] 
**following** | [**NotificationActorFollowFollowing**](NotificationActorFollowFollowing.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::NotificationActorFollow.new(id: null,
                                 follower: null,
                                 state: null,
                                 following: null)
```


