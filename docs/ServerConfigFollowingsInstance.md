# Peertube::ServerConfigFollowingsInstance

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_follow_index** | [**ServerConfigFollowingsInstanceAutoFollowIndex**](ServerConfigFollowingsInstanceAutoFollowIndex.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigFollowingsInstance.new(auto_follow_index: null)
```


