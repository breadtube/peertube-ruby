# Peertube::VideoAbuseVideo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**name** | **String** |  | [optional] 
**uuid** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoAbuseVideo.new(id: null,
                                 name: null,
                                 uuid: 9c9de5e8-0a1e-484a-b099-e80766180a6d)
```


