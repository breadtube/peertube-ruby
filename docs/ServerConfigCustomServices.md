# Peertube::ServerConfigCustomServices

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**twitter** | [**ServerConfigCustomServicesTwitter**](ServerConfigCustomServicesTwitter.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigCustomServices.new(twitter: null)
```


