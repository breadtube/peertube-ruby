# Peertube::ServerConfigAutoBlacklist

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**videos** | [**ServerConfigAutoBlacklistVideos**](ServerConfigAutoBlacklistVideos.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigAutoBlacklist.new(videos: null)
```


