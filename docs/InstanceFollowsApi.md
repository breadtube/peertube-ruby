# Peertube::InstanceFollowsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**server_followers_get**](InstanceFollowsApi.md#server_followers_get) | **GET** /server/followers | List instance followers
[**server_following_get**](InstanceFollowsApi.md#server_following_get) | **GET** /server/following | List instances followed by the server
[**server_following_host_delete**](InstanceFollowsApi.md#server_following_host_delete) | **DELETE** /server/following/{host} | Unfollow a server
[**server_following_post**](InstanceFollowsApi.md#server_following_post) | **POST** /server/following | Follow a server



## server_followers_get

> Array&lt;Follow&gt; server_followers_get(opts)

List instance followers

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::InstanceFollowsApi.new
opts = {
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: '-createdAt' # String | Sort column
}

begin
  #List instance followers
  result = api_instance.server_followers_get(opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling InstanceFollowsApi->server_followers_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort column | [optional] 

### Return type

[**Array&lt;Follow&gt;**](Follow.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## server_following_get

> Array&lt;Follow&gt; server_following_get(opts)

List instances followed by the server

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::InstanceFollowsApi.new
opts = {
  state: 'state_example', # String | 
  actor_type: 'actor_type_example', # String | 
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: '-createdAt' # String | Sort column
}

begin
  #List instances followed by the server
  result = api_instance.server_following_get(opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling InstanceFollowsApi->server_following_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **state** | **String**|  | [optional] 
 **actor_type** | **String**|  | [optional] 
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort column | [optional] 

### Return type

[**Array&lt;Follow&gt;**](Follow.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## server_following_host_delete

> server_following_host_delete(host)

Unfollow a server

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::InstanceFollowsApi.new
host = 'host_example' # String | The host to unfollow 

begin
  #Unfollow a server
  api_instance.server_following_host_delete(host)
rescue Peertube::ApiError => e
  puts "Exception when calling InstanceFollowsApi->server_following_host_delete: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host** | **String**| The host to unfollow  | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## server_following_post

> server_following_post(opts)

Follow a server

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::InstanceFollowsApi.new
opts = {
  inline_object: Peertube::InlineObject.new # InlineObject | 
}

begin
  #Follow a server
  api_instance.server_following_post(opts)
rescue Peertube::ApiError => e
  puts "Exception when calling InstanceFollowsApi->server_following_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object** | [**InlineObject**](InlineObject.md)|  | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

