# Peertube::InlineObject17

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**text** | **String** | Text comment | 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject17.new(text: null)
```


