# Peertube::VideoDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**uuid** | **String** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**published_at** | **DateTime** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 
**originally_published_at** | **DateTime** |  | [optional] 
**category** | [**VideoConstantNumber**](VideoConstantNumber.md) |  | [optional] 
**licence** | [**VideoConstantNumber**](VideoConstantNumber.md) |  | [optional] 
**language** | [**VideoConstantString**](VideoConstantString.md) |  | [optional] 
**privacy** | [**VideoPrivacyConstant**](VideoPrivacyConstant.md) |  | [optional] 
**description** | **String** |  | [optional] 
**duration** | **Integer** |  | [optional] 
**is_local** | **Boolean** |  | [optional] 
**name** | **String** |  | [optional] 
**thumbnail_path** | **String** |  | [optional] 
**preview_path** | **String** |  | [optional] 
**embed_path** | **String** |  | [optional] 
**views** | **Integer** |  | [optional] 
**likes** | **Integer** |  | [optional] 
**dislikes** | **Integer** |  | [optional] 
**nsfw** | **Boolean** |  | [optional] 
**wait_transcoding** | **Boolean** |  | [optional] 
**state** | [**VideoStateConstant**](VideoStateConstant.md) |  | [optional] 
**scheduled_update** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md) |  | [optional] 
**blacklisted** | **Boolean** |  | [optional] 
**blacklisted_reason** | **String** |  | [optional] 
**account** | [**Account**](Account.md) |  | [optional] 
**channel** | [**VideoChannel**](VideoChannel.md) |  | [optional] 
**user_history** | [**VideoUserHistory**](VideoUserHistory.md) |  | [optional] 
**description_path** | **String** |  | [optional] 
**support** | **String** | A text tell the audience how to support the video creator | [optional] 
**tags** | **Array&lt;String&gt;** |  | [optional] 
**files** | [**Array&lt;VideoFile&gt;**](VideoFile.md) |  | [optional] 
**comments_enabled** | **Boolean** |  | [optional] 
**download_enabled** | **Boolean** |  | [optional] 
**tracker_urls** | **Array&lt;String&gt;** |  | [optional] 
**streaming_playlists** | [**Array&lt;VideoStreamingPlaylists&gt;**](VideoStreamingPlaylists.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoDetails.new(id: 8,
                                 uuid: 9c9de5e8-0a1e-484a-b099-e80766180a6d,
                                 created_at: null,
                                 published_at: null,
                                 updated_at: null,
                                 originally_published_at: null,
                                 category: null,
                                 licence: null,
                                 language: null,
                                 privacy: null,
                                 description: null,
                                 duration: 1419,
                                 is_local: null,
                                 name: What is PeerTube?,
                                 thumbnail_path: /static/thumbnails/a65bc12f-9383-462e-81ae-8207e8b434ee.jpg,
                                 preview_path: /static/previews/a65bc12f-9383-462e-81ae-8207e8b434ee.jpg,
                                 embed_path: /videos/embed/a65bc12f-9383-462e-81ae-8207e8b434ee,
                                 views: 1337,
                                 likes: 42,
                                 dislikes: 7,
                                 nsfw: null,
                                 wait_transcoding: null,
                                 state: null,
                                 scheduled_update: null,
                                 blacklisted: null,
                                 blacklisted_reason: null,
                                 account: null,
                                 channel: null,
                                 user_history: null,
                                 description_path: null,
                                 support: Please support my work on &lt;insert crowdfunding plateform&gt;! &lt;3,
                                 tags: [&quot;flowers&quot;,&quot;gardening&quot;],
                                 files: null,
                                 comments_enabled: null,
                                 download_enabled: null,
                                 tracker_urls: null,
                                 streaming_playlists: null)
```


