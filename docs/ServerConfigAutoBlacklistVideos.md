# Peertube::ServerConfigAutoBlacklistVideos

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**of_users** | [**ServerConfigEmail**](ServerConfigEmail.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigAutoBlacklistVideos.new(of_users: null)
```


