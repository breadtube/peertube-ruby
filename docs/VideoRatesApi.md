# Peertube::VideoRatesApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**users_me_videos_video_id_rating_get**](VideoRatesApi.md#users_me_videos_video_id_rating_get) | **GET** /users/me/videos/{videoId}/rating | Get rate of my user for a video
[**videos_id_rate_put**](VideoRatesApi.md#videos_id_rate_put) | **PUT** /videos/{id}/rate | Like/dislike a video



## users_me_videos_video_id_rating_get

> GetMeVideoRating users_me_videos_video_id_rating_get(video_id)

Get rate of my user for a video

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoRatesApi.new
video_id = 'video_id_example' # String | The video id 

begin
  #Get rate of my user for a video
  result = api_instance.users_me_videos_video_id_rating_get(video_id)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoRatesApi->users_me_videos_video_id_rating_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **video_id** | **String**| The video id  | 

### Return type

[**GetMeVideoRating**](GetMeVideoRating.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## videos_id_rate_put

> videos_id_rate_put(id)

Like/dislike a video

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoRatesApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid

begin
  #Like/dislike a video
  api_instance.videos_id_rate_put(id)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoRatesApi->videos_id_rate_put: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

