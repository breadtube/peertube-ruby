# Peertube::ServerConfigFollowingsInstanceAutoFollowIndex

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index_url** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigFollowingsInstanceAutoFollowIndex.new(index_url: null)
```


