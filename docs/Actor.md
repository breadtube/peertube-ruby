# Peertube::Actor

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**url** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**host** | **String** |  | [optional] 
**following_count** | **Integer** |  | [optional] 
**followers_count** | **Integer** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 
**avatar** | [**Avatar**](Avatar.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::Actor.new(id: 11,
                                 url: null,
                                 name: null,
                                 host: null,
                                 following_count: null,
                                 followers_count: null,
                                 created_at: null,
                                 updated_at: null,
                                 avatar: null)
```


