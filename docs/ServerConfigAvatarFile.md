# Peertube::ServerConfigAvatarFile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**size** | [**ServerConfigAvatarFileSize**](ServerConfigAvatarFileSize.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigAvatarFile.new(size: null)
```


