# Peertube::AccountAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_id** | **String** |  | [optional] 
**display_name** | **String** |  | [optional] 
**description** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::AccountAllOf.new(user_id: 2,
                                 display_name: null,
                                 description: null)
```


