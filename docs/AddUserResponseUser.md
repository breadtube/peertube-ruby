# Peertube::AddUserResponseUser

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**account** | [**AddUserResponseUserAccount**](AddUserResponseUserAccount.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::AddUserResponseUser.new(id: 8,
                                 account: null)
```


