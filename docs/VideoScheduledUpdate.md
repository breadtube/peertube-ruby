# Peertube::VideoScheduledUpdate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**privacy** | [**VideoPrivacySet**](VideoPrivacySet.md) |  | [optional] 
**update_at** | **Date** | When to update the video | 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoScheduledUpdate.new(privacy: null,
                                 update_at: null)
```


