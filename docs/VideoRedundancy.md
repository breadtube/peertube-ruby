# Peertube::VideoRedundancy

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**name** | **String** |  | [optional] 
**url** | **String** |  | [optional] 
**uuid** | **String** |  | [optional] 
**redundancies** | [**VideoRedundancyRedundancies**](VideoRedundancyRedundancies.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoRedundancy.new(id: null,
                                 name: null,
                                 url: null,
                                 uuid: 9c9de5e8-0a1e-484a-b099-e80766180a6d,
                                 redundancies: null)
```


