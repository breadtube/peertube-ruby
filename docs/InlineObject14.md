# Peertube::InlineObject14

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**video_id** | **Integer** | Video to add in the playlist | 
**start_timestamp** | **Integer** | Start the video at this specific timestamp (in seconds) | [optional] 
**stop_timestamp** | **Integer** | Stop the video at this specific timestamp (in seconds) | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject14.new(video_id: null,
                                 start_timestamp: null,
                                 stop_timestamp: null)
```


