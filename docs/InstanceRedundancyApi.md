# Peertube::InstanceRedundancyApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**redundancy_host_put**](InstanceRedundancyApi.md#redundancy_host_put) | **PUT** /redundancy/{host} | Update a server redundancy policy



## redundancy_host_put

> redundancy_host_put(host, opts)

Update a server redundancy policy

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::InstanceRedundancyApi.new
host = 'host_example' # String | server domain to mirror
opts = {
  inline_object21: Peertube::InlineObject21.new # InlineObject21 | 
}

begin
  #Update a server redundancy policy
  api_instance.redundancy_host_put(host, opts)
rescue Peertube::ApiError => e
  puts "Exception when calling InstanceRedundancyApi->redundancy_host_put: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host** | **String**| server domain to mirror | 
 **inline_object21** | [**InlineObject21**](InlineObject21.md)|  | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

