# Peertube::MyNotificationsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**users_me_notification_settings_put**](MyNotificationsApi.md#users_me_notification_settings_put) | **PUT** /users/me/notification-settings | Update my notification settings
[**users_me_notifications_get**](MyNotificationsApi.md#users_me_notifications_get) | **GET** /users/me/notifications | List my notifications
[**users_me_notifications_read_all_post**](MyNotificationsApi.md#users_me_notifications_read_all_post) | **POST** /users/me/notifications/read-all | Mark all my notification as read
[**users_me_notifications_read_post**](MyNotificationsApi.md#users_me_notifications_read_post) | **POST** /users/me/notifications/read | Mark notifications as read by their id



## users_me_notification_settings_put

> users_me_notification_settings_put(opts)

Update my notification settings

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::MyNotificationsApi.new
opts = {
  inline_object3: Peertube::InlineObject3.new # InlineObject3 | 
}

begin
  #Update my notification settings
  api_instance.users_me_notification_settings_put(opts)
rescue Peertube::ApiError => e
  puts "Exception when calling MyNotificationsApi->users_me_notification_settings_put: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object3** | [**InlineObject3**](InlineObject3.md)|  | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## users_me_notifications_get

> NotificationListResponse users_me_notifications_get(opts)

List my notifications

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::MyNotificationsApi.new
opts = {
  unread: true, # Boolean | only list unread notifications
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: '-createdAt' # String | Sort column
}

begin
  #List my notifications
  result = api_instance.users_me_notifications_get(opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling MyNotificationsApi->users_me_notifications_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unread** | **Boolean**| only list unread notifications | [optional] 
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort column | [optional] 

### Return type

[**NotificationListResponse**](NotificationListResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## users_me_notifications_read_all_post

> users_me_notifications_read_all_post

Mark all my notification as read

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::MyNotificationsApi.new

begin
  #Mark all my notification as read
  api_instance.users_me_notifications_read_all_post
rescue Peertube::ApiError => e
  puts "Exception when calling MyNotificationsApi->users_me_notifications_read_all_post: #{e}"
end
```

### Parameters

This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## users_me_notifications_read_post

> users_me_notifications_read_post(opts)

Mark notifications as read by their id

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::MyNotificationsApi.new
opts = {
  inline_object2: Peertube::InlineObject2.new # InlineObject2 | 
}

begin
  #Mark notifications as read by their id
  api_instance.users_me_notifications_read_post(opts)
rescue Peertube::ApiError => e
  puts "Exception when calling MyNotificationsApi->users_me_notifications_read_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object2** | [**InlineObject2**](InlineObject2.md)|  | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

