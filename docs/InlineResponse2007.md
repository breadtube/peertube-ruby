# Peertube::InlineResponse2007

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**video_id** | [**Array&lt;InlineResponse2007VideoId&gt;**](InlineResponse2007VideoId.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineResponse2007.new(video_id: null)
```


