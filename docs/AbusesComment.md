# Peertube::AbusesComment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Float** | Comment id to report | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::AbusesComment.new(id: null)
```


