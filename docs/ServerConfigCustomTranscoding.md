# Peertube::ServerConfigCustomTranscoding

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **Boolean** |  | [optional] 
**allow_additional_extensions** | **Boolean** |  | [optional] 
**allow_audio_files** | **Boolean** |  | [optional] 
**threads** | **Integer** |  | [optional] 
**resolutions** | [**ServerConfigCustomTranscodingResolutions**](ServerConfigCustomTranscodingResolutions.md) |  | [optional] 
**hls** | [**ServerConfigEmail**](ServerConfigEmail.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigCustomTranscoding.new(enabled: null,
                                 allow_additional_extensions: null,
                                 allow_audio_files: null,
                                 threads: null,
                                 resolutions: null,
                                 hls: null)
```


