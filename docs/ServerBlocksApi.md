# Peertube::ServerBlocksApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**blocklist_servers_get**](ServerBlocksApi.md#blocklist_servers_get) | **GET** /blocklist/servers | List server blocks
[**blocklist_servers_host_delete**](ServerBlocksApi.md#blocklist_servers_host_delete) | **DELETE** /blocklist/servers/{host} | Unblock a server by its domain
[**blocklist_servers_post**](ServerBlocksApi.md#blocklist_servers_post) | **POST** /blocklist/servers | Block a server



## blocklist_servers_get

> blocklist_servers_get(opts)

List server blocks

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::ServerBlocksApi.new
opts = {
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: '-createdAt' # String | Sort column
}

begin
  #List server blocks
  api_instance.blocklist_servers_get(opts)
rescue Peertube::ApiError => e
  puts "Exception when calling ServerBlocksApi->blocklist_servers_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort column | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## blocklist_servers_host_delete

> blocklist_servers_host_delete(host)

Unblock a server by its domain

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::ServerBlocksApi.new
host = 'host_example' # String | server domain to unblock

begin
  #Unblock a server by its domain
  api_instance.blocklist_servers_host_delete(host)
rescue Peertube::ApiError => e
  puts "Exception when calling ServerBlocksApi->blocklist_servers_host_delete: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host** | **String**| server domain to unblock | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## blocklist_servers_post

> blocklist_servers_post(opts)

Block a server

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::ServerBlocksApi.new
opts = {
  inline_object20: Peertube::InlineObject20.new # InlineObject20 | 
}

begin
  #Block a server
  api_instance.blocklist_servers_post(opts)
rescue Peertube::ApiError => e
  puts "Exception when calling ServerBlocksApi->blocklist_servers_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object20** | [**InlineObject20**](InlineObject20.md)|  | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

