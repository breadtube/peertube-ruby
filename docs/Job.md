# Peertube::Job

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**state** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**data** | **Hash&lt;String, Object&gt;** |  | [optional] 
**error** | **Hash&lt;String, Object&gt;** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**finished_on** | **DateTime** |  | [optional] 
**processed_on** | **DateTime** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::Job.new(id: 42,
                                 state: null,
                                 type: null,
                                 data: null,
                                 error: null,
                                 created_at: null,
                                 finished_on: null,
                                 processed_on: null)
```


