# Peertube::VideoMirroringApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**redundancy_videos_get**](VideoMirroringApi.md#redundancy_videos_get) | **GET** /redundancy/videos | List videos being mirrored
[**redundancy_videos_post**](VideoMirroringApi.md#redundancy_videos_post) | **POST** /redundancy/videos | Mirror a video
[**redundancy_videos_redundancy_id_delete**](VideoMirroringApi.md#redundancy_videos_redundancy_id_delete) | **DELETE** /redundancy/videos/{redundancyId} | Delete a mirror done on a video



## redundancy_videos_get

> Array&lt;VideoRedundancy&gt; redundancy_videos_get(target, opts)

List videos being mirrored

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoMirroringApi.new
target = 'target_example' # String | direction of the mirror
opts = {
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: 'sort_example' # String | Sort abuses by criteria
}

begin
  #List videos being mirrored
  result = api_instance.redundancy_videos_get(target, opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoMirroringApi->redundancy_videos_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **target** | **String**| direction of the mirror | 
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort abuses by criteria | [optional] 

### Return type

[**Array&lt;VideoRedundancy&gt;**](VideoRedundancy.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## redundancy_videos_post

> redundancy_videos_post(opts)

Mirror a video

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoMirroringApi.new
opts = {
  inline_object22: Peertube::InlineObject22.new # InlineObject22 | 
}

begin
  #Mirror a video
  api_instance.redundancy_videos_post(opts)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoMirroringApi->redundancy_videos_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object22** | [**InlineObject22**](InlineObject22.md)|  | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## redundancy_videos_redundancy_id_delete

> redundancy_videos_redundancy_id_delete(redundancy_id)

Delete a mirror done on a video

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoMirroringApi.new
redundancy_id = 'redundancy_id_example' # String | id of an existing redundancy on a video

begin
  #Delete a mirror done on a video
  api_instance.redundancy_videos_redundancy_id_delete(redundancy_id)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoMirroringApi->redundancy_videos_redundancy_id_delete: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **redundancy_id** | **String**| id of an existing redundancy on a video | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

