# Peertube::UsersApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**del_user_id**](UsersApi.md#del_user_id) | **DELETE** /users/{id} | Delete a user
[**get_user_id**](UsersApi.md#get_user_id) | **GET** /users/{id} | Get a user
[**put_user_id**](UsersApi.md#put_user_id) | **PUT** /users/{id} | Update a user
[**users_get**](UsersApi.md#users_get) | **GET** /users | List users
[**users_post**](UsersApi.md#users_post) | **POST** /users | Create a user
[**users_register_post**](UsersApi.md#users_register_post) | **POST** /users/register | Register a user



## del_user_id

> del_user_id(id)

Delete a user

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::UsersApi.new
id = 42 # Integer | The user id

begin
  #Delete a user
  api_instance.del_user_id(id)
rescue Peertube::ApiError => e
  puts "Exception when calling UsersApi->del_user_id: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| The user id | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## get_user_id

> User get_user_id(id)

Get a user

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::UsersApi.new
id = 42 # Integer | The user id

begin
  #Get a user
  result = api_instance.get_user_id(id)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling UsersApi->get_user_id: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| The user id | 

### Return type

[**User**](User.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## put_user_id

> put_user_id(id, update_user)

Update a user

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::UsersApi.new
id = 42 # Integer | The user id
update_user = Peertube::UpdateUser.new # UpdateUser | 

begin
  #Update a user
  api_instance.put_user_id(id, update_user)
rescue Peertube::ApiError => e
  puts "Exception when calling UsersApi->put_user_id: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| The user id | 
 **update_user** | [**UpdateUser**](UpdateUser.md)|  | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## users_get

> Array&lt;User&gt; users_get(opts)

List users

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::UsersApi.new
opts = {
  search: 'search_example', # String | Plain text search that will match with user usernames or emails
  blocked: true, # Boolean | Filter results down to (un)banned users
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: 'sort_example' # String | Sort users by criteria
}

begin
  #List users
  result = api_instance.users_get(opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling UsersApi->users_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search** | **String**| Plain text search that will match with user usernames or emails | [optional] 
 **blocked** | **Boolean**| Filter results down to (un)banned users | [optional] 
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort users by criteria | [optional] 

### Return type

[**Array&lt;User&gt;**](User.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## users_post

> AddUserResponse users_post(add_user)

Create a user

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::UsersApi.new
add_user = Peertube::AddUser.new # AddUser | User to create

begin
  #Create a user
  result = api_instance.users_post(add_user)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling UsersApi->users_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **add_user** | [**AddUser**](AddUser.md)| User to create | 

### Return type

[**AddUserResponse**](AddUserResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## users_register_post

> users_register_post(register_user)

Register a user

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::UsersApi.new
register_user = Peertube::RegisterUser.new # RegisterUser | 

begin
  #Register a user
  api_instance.users_register_post(register_user)
rescue Peertube::ApiError => e
  puts "Exception when calling UsersApi->users_register_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **register_user** | [**RegisterUser**](RegisterUser.md)|  | 

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

