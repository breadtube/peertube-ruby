# Peertube::VideoChannelOwnerAccount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**uuid** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoChannelOwnerAccount.new(id: null,
                                 uuid: 9c9de5e8-0a1e-484a-b099-e80766180a6d)
```


