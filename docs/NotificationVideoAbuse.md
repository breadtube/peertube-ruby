# Peertube::NotificationVideoAbuse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**video** | [**VideoInfo**](VideoInfo.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::NotificationVideoAbuse.new(id: null,
                                 video: null)
```


