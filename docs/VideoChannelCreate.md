# Peertube::VideoChannelCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**display_name** | **String** |  | 
**description** | **String** |  | [optional] 
**support** | **String** | A text shown by default on all videos of this channel, to tell the audience how to support it | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoChannelCreate.new(name: null,
                                 display_name: null,
                                 description: null,
                                 support: Please support my work on &lt;insert crowdfunding plateform&gt;! &lt;3)
```


