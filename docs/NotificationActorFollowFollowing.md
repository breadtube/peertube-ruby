# Peertube::NotificationActorFollowFollowing

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**display_name** | **String** |  | [optional] 
**host** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::NotificationActorFollowFollowing.new(type: null,
                                 name: null,
                                 display_name: null,
                                 host: null)
```


