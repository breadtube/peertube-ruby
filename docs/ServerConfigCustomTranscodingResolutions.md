# Peertube::ServerConfigCustomTranscodingResolutions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_240p** | **Boolean** |  | [optional] 
**_360p** | **Boolean** |  | [optional] 
**_480p** | **Boolean** |  | [optional] 
**_720p** | **Boolean** |  | [optional] 
**_1080p** | **Boolean** |  | [optional] 
**_2160p** | **Boolean** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigCustomTranscodingResolutions.new(_240p: null,
                                 _360p: null,
                                 _480p: null,
                                 _720p: null,
                                 _1080p: null,
                                 _2160p: null)
```


