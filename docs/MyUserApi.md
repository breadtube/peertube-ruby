# Peertube::MyUserApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**users_me_avatar_pick_post**](MyUserApi.md#users_me_avatar_pick_post) | **POST** /users/me/avatar/pick | Update my user avatar
[**users_me_get**](MyUserApi.md#users_me_get) | **GET** /users/me | Get my user information
[**users_me_put**](MyUserApi.md#users_me_put) | **PUT** /users/me | Update my user information
[**users_me_video_quota_used_get**](MyUserApi.md#users_me_video_quota_used_get) | **GET** /users/me/video-quota-used | Get my user used quota
[**users_me_videos_get**](MyUserApi.md#users_me_videos_get) | **GET** /users/me/videos | Get videos of my user
[**users_me_videos_imports_get**](MyUserApi.md#users_me_videos_imports_get) | **GET** /users/me/videos/imports | Get video imports of my user
[**users_me_videos_video_id_rating_get**](MyUserApi.md#users_me_videos_video_id_rating_get) | **GET** /users/me/videos/{videoId}/rating | Get rate of my user for a video



## users_me_avatar_pick_post

> Avatar users_me_avatar_pick_post(opts)

Update my user avatar

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::MyUserApi.new
opts = {
  avatarfile: File.new('/path/to/file') # File | The file to upload.
}

begin
  #Update my user avatar
  result = api_instance.users_me_avatar_pick_post(opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling MyUserApi->users_me_avatar_pick_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **avatarfile** | **File**| The file to upload. | [optional] 

### Return type

[**Avatar**](Avatar.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## users_me_get

> Array&lt;User&gt; users_me_get

Get my user information

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::MyUserApi.new

begin
  #Get my user information
  result = api_instance.users_me_get
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling MyUserApi->users_me_get: #{e}"
end
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**Array&lt;User&gt;**](User.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## users_me_put

> users_me_put(update_me)

Update my user information

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::MyUserApi.new
update_me = Peertube::UpdateMe.new # UpdateMe | 

begin
  #Update my user information
  api_instance.users_me_put(update_me)
rescue Peertube::ApiError => e
  puts "Exception when calling MyUserApi->users_me_put: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **update_me** | [**UpdateMe**](UpdateMe.md)|  | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## users_me_video_quota_used_get

> Float users_me_video_quota_used_get

Get my user used quota

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::MyUserApi.new

begin
  #Get my user used quota
  result = api_instance.users_me_video_quota_used_get
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling MyUserApi->users_me_video_quota_used_get: #{e}"
end
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Float**

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## users_me_videos_get

> VideoListResponse users_me_videos_get(opts)

Get videos of my user

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::MyUserApi.new
opts = {
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: '-createdAt' # String | Sort column
}

begin
  #Get videos of my user
  result = api_instance.users_me_videos_get(opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling MyUserApi->users_me_videos_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort column | [optional] 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## users_me_videos_imports_get

> VideoImport users_me_videos_imports_get(opts)

Get video imports of my user

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::MyUserApi.new
opts = {
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: '-createdAt' # String | Sort column
}

begin
  #Get video imports of my user
  result = api_instance.users_me_videos_imports_get(opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling MyUserApi->users_me_videos_imports_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort column | [optional] 

### Return type

[**VideoImport**](VideoImport.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## users_me_videos_video_id_rating_get

> GetMeVideoRating users_me_videos_video_id_rating_get(video_id)

Get rate of my user for a video

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::MyUserApi.new
video_id = 'video_id_example' # String | The video id 

begin
  #Get rate of my user for a video
  result = api_instance.users_me_videos_video_id_rating_get(video_id)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling MyUserApi->users_me_videos_video_id_rating_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **video_id** | **String**| The video id  | 

### Return type

[**GetMeVideoRating**](GetMeVideoRating.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

