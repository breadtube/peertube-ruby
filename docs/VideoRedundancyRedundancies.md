# Peertube::VideoRedundancyRedundancies

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**files** | [**Array&lt;FileRedundancyInformation&gt;**](FileRedundancyInformation.md) |  | [optional] 
**streaming_playlists** | [**Array&lt;FileRedundancyInformation&gt;**](FileRedundancyInformation.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoRedundancyRedundancies.new(files: null,
                                 streaming_playlists: null)
```


