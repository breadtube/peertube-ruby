# Peertube::RegisterUserChannel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The name for the default channel | [optional] 
**display_name** | **String** | The display name for the default channel | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::RegisterUserChannel.new(name: null,
                                 display_name: null)
```


