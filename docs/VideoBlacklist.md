# Peertube::VideoBlacklist

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**video_id** | **Integer** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 
**name** | **String** |  | [optional] 
**uuid** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**duration** | **Integer** |  | [optional] 
**views** | **Integer** |  | [optional] 
**likes** | **Integer** |  | [optional] 
**dislikes** | **Integer** |  | [optional] 
**nsfw** | **Boolean** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoBlacklist.new(id: null,
                                 video_id: null,
                                 created_at: null,
                                 updated_at: null,
                                 name: null,
                                 uuid: 9c9de5e8-0a1e-484a-b099-e80766180a6d,
                                 description: null,
                                 duration: null,
                                 views: null,
                                 likes: null,
                                 dislikes: null,
                                 nsfw: null)
```


