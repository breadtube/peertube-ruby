# Peertube::VideoDetailsAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description_path** | **String** |  | [optional] 
**support** | **String** | A text tell the audience how to support the video creator | [optional] 
**channel** | [**VideoChannel**](VideoChannel.md) |  | [optional] 
**account** | [**Account**](Account.md) |  | [optional] 
**tags** | **Array&lt;String&gt;** |  | [optional] 
**files** | [**Array&lt;VideoFile&gt;**](VideoFile.md) |  | [optional] 
**comments_enabled** | **Boolean** |  | [optional] 
**download_enabled** | **Boolean** |  | [optional] 
**tracker_urls** | **Array&lt;String&gt;** |  | [optional] 
**streaming_playlists** | [**Array&lt;VideoStreamingPlaylists&gt;**](VideoStreamingPlaylists.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoDetailsAllOf.new(description_path: null,
                                 support: Please support my work on &lt;insert crowdfunding plateform&gt;! &lt;3,
                                 channel: null,
                                 account: null,
                                 tags: [&quot;flowers&quot;,&quot;gardening&quot;],
                                 files: null,
                                 comments_enabled: null,
                                 download_enabled: null,
                                 tracker_urls: null,
                                 streaming_playlists: null)
```


