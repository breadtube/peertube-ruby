# Peertube::InlineObject10

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**state** | [**AbuseStateSet**](AbuseStateSet.md) |  | [optional] 
**moderation_comment** | **String** | Update the report comment visible only to the moderation team | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject10.new(state: null,
                                 moderation_comment: null)
```


