# Peertube::ServerConfigCustomSignup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **Boolean** |  | [optional] 
**limit** | **Integer** |  | [optional] 
**requires_email_verification** | **Boolean** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigCustomSignup.new(enabled: null,
                                 limit: null,
                                 requires_email_verification: null)
```


