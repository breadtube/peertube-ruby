# Peertube::AddUser

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** | The user username | 
**password** | **String** | The user password. If the smtp server is configured, you can leave empty and an email will be sent | 
**email** | **String** | The user email | 
**video_quota** | **Integer** | The user video quota | 
**video_quota_daily** | **Integer** | The user daily video quota | 
**role** | [**UserRole**](UserRole.md) |  | 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::AddUser.new(username: null,
                                 password: null,
                                 email: null,
                                 video_quota: null,
                                 video_quota_daily: null,
                                 role: null)
```


