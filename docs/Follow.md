# Peertube::Follow

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**follower** | [**Actor**](Actor.md) |  | [optional] 
**following** | [**Actor**](Actor.md) |  | [optional] 
**score** | **Float** | score reflecting the reachability of the actor, with steps of &#x60;10&#x60; and a base score of &#x60;1000&#x60;. | [optional] 
**state** | **String** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::Follow.new(id: null,
                                 follower: null,
                                 following: null,
                                 score: null,
                                 state: null,
                                 created_at: null,
                                 updated_at: null)
```


