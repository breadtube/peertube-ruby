# Peertube::PlaylistElement

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**position** | **Integer** |  | [optional] 
**start_timestamp** | **Integer** |  | [optional] 
**stop_timestamp** | **Integer** |  | [optional] 
**video** | [**Video**](Video.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::PlaylistElement.new(position: null,
                                 start_timestamp: null,
                                 stop_timestamp: null,
                                 video: null)
```


