# Peertube::ServerConfigUser

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**video_quota** | **Integer** |  | [optional] 
**video_quota_daily** | **Integer** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigUser.new(video_quota: null,
                                 video_quota_daily: null)
```


