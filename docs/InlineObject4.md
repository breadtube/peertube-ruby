# Peertube::InlineObject4

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**avatarfile** | **File** | The file to upload. | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject4.new(avatarfile: null)
```


