# Peertube::ServerConfigTrending

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**videos** | [**ServerConfigTrendingVideos**](ServerConfigTrendingVideos.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigTrending.new(videos: null)
```


