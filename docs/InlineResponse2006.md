# Peertube::InlineResponse2006

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**video_playlist_element** | [**InlineResponse2006VideoPlaylistElement**](InlineResponse2006VideoPlaylistElement.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineResponse2006.new(video_playlist_element: null)
```


