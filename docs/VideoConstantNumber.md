# Peertube::VideoConstantNumber

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**label** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoConstantNumber.new(id: null,
                                 label: null)
```


