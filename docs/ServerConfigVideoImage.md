# Peertube::ServerConfigVideoImage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**extensions** | **Array&lt;String&gt;** |  | [optional] 
**size** | [**ServerConfigAvatarFileSize**](ServerConfigAvatarFileSize.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigVideoImage.new(extensions: null,
                                 size: null)
```


