# Peertube::ServerConfigVideo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**image** | [**ServerConfigVideoImage**](ServerConfigVideoImage.md) |  | [optional] 
**file** | [**ServerConfigVideoFile**](ServerConfigVideoFile.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigVideo.new(image: null,
                                 file: null)
```


