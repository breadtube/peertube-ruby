# Peertube::AbusesVideo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Float** | Video id to report | [optional] 
**start_at** | **Integer** | Timestamp in the video that marks the beginning of the report | [optional] 
**end_at** | **Integer** | Timestamp in the video that marks the ending of the report | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::AbusesVideo.new(id: null,
                                 start_at: null,
                                 end_at: null)
```


