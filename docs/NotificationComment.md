# Peertube::NotificationComment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**thread_id** | **Integer** |  | [optional] 
**video** | [**VideoInfo**](VideoInfo.md) |  | [optional] 
**account** | [**ActorInfo**](ActorInfo.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::NotificationComment.new(id: null,
                                 thread_id: null,
                                 video: null,
                                 account: null)
```


