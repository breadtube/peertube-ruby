# Peertube::UpdateMe

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**password** | **String** | Your new password | 
**email** | **String** | Your new email | 
**display_nsfw** | **String** | Your new displayNSFW | 
**auto_play_video** | **Boolean** | Your new autoPlayVideo | 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::UpdateMe.new(password: null,
                                 email: null,
                                 display_nsfw: null,
                                 auto_play_video: null)
```


