# Peertube::ServerConfigPlugin

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**registered** | **Array&lt;String&gt;** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigPlugin.new(registered: null)
```


