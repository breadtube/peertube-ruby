# Peertube::InlineObject16

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_timestamp** | **Integer** | Start the video at this specific timestamp (in seconds) | [optional] 
**stop_timestamp** | **Integer** | Stop the video at this specific timestamp (in seconds) | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject16.new(start_timestamp: null,
                                 stop_timestamp: null)
```


