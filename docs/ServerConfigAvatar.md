# Peertube::ServerConfigAvatar

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | [**ServerConfigAvatarFile**](ServerConfigAvatarFile.md) |  | [optional] 
**extensions** | **Array&lt;String&gt;** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigAvatar.new(file: null,
                                 extensions: null)
```


