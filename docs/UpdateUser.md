# Peertube::UpdateUser

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | The user id | 
**email** | **String** | The updated email of the user | 
**video_quota** | **Integer** | The updated video quota of the user | 
**video_quota_daily** | **Integer** | The updated daily video quota of the user | 
**role** | [**UserRole**](UserRole.md) |  | 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::UpdateUser.new(id: null,
                                 email: null,
                                 video_quota: null,
                                 video_quota_daily: null,
                                 role: null)
```


