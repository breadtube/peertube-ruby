# Peertube::InlineObject24

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**settings** | **Hash&lt;String, Object&gt;** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject24.new(settings: null)
```


