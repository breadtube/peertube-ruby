# Peertube::VideoCommentsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**videos_id_comment_threads_get**](VideoCommentsApi.md#videos_id_comment_threads_get) | **GET** /videos/{id}/comment-threads | List threads of a video
[**videos_id_comment_threads_post**](VideoCommentsApi.md#videos_id_comment_threads_post) | **POST** /videos/{id}/comment-threads | Create a thread
[**videos_id_comment_threads_thread_id_get**](VideoCommentsApi.md#videos_id_comment_threads_thread_id_get) | **GET** /videos/{id}/comment-threads/{threadId} | Get a thread
[**videos_id_comments_comment_id_delete**](VideoCommentsApi.md#videos_id_comments_comment_id_delete) | **DELETE** /videos/{id}/comments/{commentId} | Delete a comment or a reply
[**videos_id_comments_comment_id_post**](VideoCommentsApi.md#videos_id_comments_comment_id_post) | **POST** /videos/{id}/comments/{commentId} | Reply to a thread of a video



## videos_id_comment_threads_get

> CommentThreadResponse videos_id_comment_threads_get(id, opts)

List threads of a video

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoCommentsApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid
opts = {
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: 'sort_example' # String | Sort comments by criteria
}

begin
  #List threads of a video
  result = api_instance.videos_id_comment_threads_get(id, opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoCommentsApi->videos_id_comment_threads_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort comments by criteria | [optional] 

### Return type

[**CommentThreadResponse**](CommentThreadResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## videos_id_comment_threads_post

> CommentThreadPostResponse videos_id_comment_threads_post(id, opts)

Create a thread

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoCommentsApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid
opts = {
  inline_object17: Peertube::InlineObject17.new # InlineObject17 | 
}

begin
  #Create a thread
  result = api_instance.videos_id_comment_threads_post(id, opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoCommentsApi->videos_id_comment_threads_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **inline_object17** | [**InlineObject17**](InlineObject17.md)|  | [optional] 

### Return type

[**CommentThreadPostResponse**](CommentThreadPostResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## videos_id_comment_threads_thread_id_get

> VideoCommentThreadTree videos_id_comment_threads_thread_id_get(id, thread_id)

Get a thread

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoCommentsApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid
thread_id = 56 # Integer | The thread id (root comment id)

begin
  #Get a thread
  result = api_instance.videos_id_comment_threads_thread_id_get(id, thread_id)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoCommentsApi->videos_id_comment_threads_thread_id_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **thread_id** | **Integer**| The thread id (root comment id) | 

### Return type

[**VideoCommentThreadTree**](VideoCommentThreadTree.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## videos_id_comments_comment_id_delete

> videos_id_comments_comment_id_delete(id, comment_id)

Delete a comment or a reply

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoCommentsApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid
comment_id = 56 # Integer | The comment id

begin
  #Delete a comment or a reply
  api_instance.videos_id_comments_comment_id_delete(id, comment_id)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoCommentsApi->videos_id_comments_comment_id_delete: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **comment_id** | **Integer**| The comment id | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## videos_id_comments_comment_id_post

> CommentThreadPostResponse videos_id_comments_comment_id_post(id, comment_id, opts)

Reply to a thread of a video

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoCommentsApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid
comment_id = 56 # Integer | The comment id
opts = {
  inline_object18: Peertube::InlineObject18.new # InlineObject18 | 
}

begin
  #Reply to a thread of a video
  result = api_instance.videos_id_comments_comment_id_post(id, comment_id, opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoCommentsApi->videos_id_comments_comment_id_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **comment_id** | **Integer**| The comment id | 
 **inline_object18** | [**InlineObject18**](InlineObject18.md)|  | [optional] 

### Return type

[**CommentThreadPostResponse**](CommentThreadPostResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

