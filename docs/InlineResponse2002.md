# Peertube::InlineResponse2002

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Integer** |  | [optional] 
**data** | [**Array&lt;VideoCaption&gt;**](VideoCaption.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineResponse2002.new(total: 1,
                                 data: null)
```


