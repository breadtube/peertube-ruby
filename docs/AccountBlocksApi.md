# Peertube::AccountBlocksApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**blocklist_accounts_account_name_delete**](AccountBlocksApi.md#blocklist_accounts_account_name_delete) | **DELETE** /blocklist/accounts/{accountName} | Unblock an account by its handle
[**blocklist_accounts_get**](AccountBlocksApi.md#blocklist_accounts_get) | **GET** /blocklist/accounts | List account blocks
[**blocklist_accounts_post**](AccountBlocksApi.md#blocklist_accounts_post) | **POST** /blocklist/accounts | Block an account



## blocklist_accounts_account_name_delete

> blocklist_accounts_account_name_delete(account_name)

Unblock an account by its handle

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::AccountBlocksApi.new
account_name = 'account_name_example' # String | account to unblock, in the form `username@domain`

begin
  #Unblock an account by its handle
  api_instance.blocklist_accounts_account_name_delete(account_name)
rescue Peertube::ApiError => e
  puts "Exception when calling AccountBlocksApi->blocklist_accounts_account_name_delete: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_name** | **String**| account to unblock, in the form &#x60;username@domain&#x60; | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## blocklist_accounts_get

> blocklist_accounts_get(opts)

List account blocks

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::AccountBlocksApi.new
opts = {
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: '-createdAt' # String | Sort column
}

begin
  #List account blocks
  api_instance.blocklist_accounts_get(opts)
rescue Peertube::ApiError => e
  puts "Exception when calling AccountBlocksApi->blocklist_accounts_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort column | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## blocklist_accounts_post

> blocklist_accounts_post(opts)

Block an account

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::AccountBlocksApi.new
opts = {
  inline_object19: Peertube::InlineObject19.new # InlineObject19 | 
}

begin
  #Block an account
  api_instance.blocklist_accounts_post(opts)
rescue Peertube::ApiError => e
  puts "Exception when calling AccountBlocksApi->blocklist_accounts_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object19** | [**InlineObject19**](InlineObject19.md)|  | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

