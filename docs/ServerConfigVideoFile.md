# Peertube::ServerConfigVideoFile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**extensions** | **Array&lt;String&gt;** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigVideoFile.new(extensions: null)
```


