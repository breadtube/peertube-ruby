# Peertube::NotificationVideoImport

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**video** | [**VideoInfo**](VideoInfo.md) |  | [optional] 
**torrent_name** | **String** |  | [optional] 
**magnet_uri** | **String** |  | [optional] 
**target_uri** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::NotificationVideoImport.new(id: null,
                                 video: null,
                                 torrent_name: null,
                                 magnet_uri: null,
                                 target_uri: null)
```


