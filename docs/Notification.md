# Peertube::Notification

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**type** | **Integer** | Notification type, following the &#x60;UserNotificationType&#x60; enum: - &#x60;1&#x60; NEW_VIDEO_FROM_SUBSCRIPTION - &#x60;2&#x60; NEW_COMMENT_ON_MY_VIDEO - &#x60;3&#x60; NEW_ABUSE_FOR_MODERATORS - &#x60;4&#x60; BLACKLIST_ON_MY_VIDEO - &#x60;5&#x60; UNBLACKLIST_ON_MY_VIDEO - &#x60;6&#x60; MY_VIDEO_PUBLISHED - &#x60;7&#x60; MY_VIDEO_IMPORT_SUCCESS - &#x60;8&#x60; MY_VIDEO_IMPORT_ERROR - &#x60;9&#x60; NEW_USER_REGISTRATION - &#x60;10&#x60; NEW_FOLLOW - &#x60;11&#x60; COMMENT_MENTION - &#x60;12&#x60; VIDEO_AUTO_BLACKLIST_FOR_MODERATORS - &#x60;13&#x60; NEW_INSTANCE_FOLLOWER - &#x60;14&#x60; AUTO_INSTANCE_FOLLOWING  | [optional] 
**read** | **Boolean** |  | [optional] 
**video** | [**VideoInfo**](VideoInfo.md) |  | [optional] 
**video_import** | [**NotificationVideoImport**](NotificationVideoImport.md) |  | [optional] 
**comment** | [**NotificationComment**](NotificationComment.md) |  | [optional] 
**video_abuse** | [**NotificationVideoAbuse**](NotificationVideoAbuse.md) |  | [optional] 
**video_blacklist** | [**NotificationVideoAbuse**](NotificationVideoAbuse.md) |  | [optional] 
**account** | [**ActorInfo**](ActorInfo.md) |  | [optional] 
**actor_follow** | [**NotificationActorFollow**](NotificationActorFollow.md) |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::Notification.new(id: null,
                                 type: null,
                                 read: null,
                                 video: null,
                                 video_import: null,
                                 comment: null,
                                 video_abuse: null,
                                 video_blacklist: null,
                                 account: null,
                                 actor_follow: null,
                                 created_at: null,
                                 updated_at: null)
```


