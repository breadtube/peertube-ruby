# Peertube::MySubscriptionsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**users_me_subscriptions_exist_get**](MySubscriptionsApi.md#users_me_subscriptions_exist_get) | **GET** /users/me/subscriptions/exist | Get if subscriptions exist for my user
[**users_me_subscriptions_get**](MySubscriptionsApi.md#users_me_subscriptions_get) | **GET** /users/me/subscriptions | Get my user subscriptions
[**users_me_subscriptions_post**](MySubscriptionsApi.md#users_me_subscriptions_post) | **POST** /users/me/subscriptions | Add subscription to my user
[**users_me_subscriptions_subscription_handle_delete**](MySubscriptionsApi.md#users_me_subscriptions_subscription_handle_delete) | **DELETE** /users/me/subscriptions/{subscriptionHandle} | Delete subscription of my user
[**users_me_subscriptions_subscription_handle_get**](MySubscriptionsApi.md#users_me_subscriptions_subscription_handle_get) | **GET** /users/me/subscriptions/{subscriptionHandle} | Get subscription of my user
[**users_me_subscriptions_videos_get**](MySubscriptionsApi.md#users_me_subscriptions_videos_get) | **GET** /users/me/subscriptions/videos | List videos of subscriptions of my user



## users_me_subscriptions_exist_get

> Object users_me_subscriptions_exist_get(uris)

Get if subscriptions exist for my user

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::MySubscriptionsApi.new
uris = ['uris_example'] # Array<String> | list of uris to check if each is part of the user subscriptions

begin
  #Get if subscriptions exist for my user
  result = api_instance.users_me_subscriptions_exist_get(uris)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling MySubscriptionsApi->users_me_subscriptions_exist_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uris** | [**Array&lt;String&gt;**](String.md)| list of uris to check if each is part of the user subscriptions | 

### Return type

**Object**

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## users_me_subscriptions_get

> users_me_subscriptions_get(opts)

Get my user subscriptions

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::MySubscriptionsApi.new
opts = {
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: '-createdAt' # String | Sort column
}

begin
  #Get my user subscriptions
  api_instance.users_me_subscriptions_get(opts)
rescue Peertube::ApiError => e
  puts "Exception when calling MySubscriptionsApi->users_me_subscriptions_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort column | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## users_me_subscriptions_post

> users_me_subscriptions_post(opts)

Add subscription to my user

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::MySubscriptionsApi.new
opts = {
  inline_object1: Peertube::InlineObject1.new # InlineObject1 | 
}

begin
  #Add subscription to my user
  api_instance.users_me_subscriptions_post(opts)
rescue Peertube::ApiError => e
  puts "Exception when calling MySubscriptionsApi->users_me_subscriptions_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object1** | [**InlineObject1**](InlineObject1.md)|  | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## users_me_subscriptions_subscription_handle_delete

> users_me_subscriptions_subscription_handle_delete(subscription_handle)

Delete subscription of my user

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::MySubscriptionsApi.new
subscription_handle = 'my_username | my_username@example.com' # String | The subscription handle

begin
  #Delete subscription of my user
  api_instance.users_me_subscriptions_subscription_handle_delete(subscription_handle)
rescue Peertube::ApiError => e
  puts "Exception when calling MySubscriptionsApi->users_me_subscriptions_subscription_handle_delete: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscription_handle** | **String**| The subscription handle | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## users_me_subscriptions_subscription_handle_get

> VideoChannel users_me_subscriptions_subscription_handle_get(subscription_handle)

Get subscription of my user

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::MySubscriptionsApi.new
subscription_handle = 'my_username | my_username@example.com' # String | The subscription handle

begin
  #Get subscription of my user
  result = api_instance.users_me_subscriptions_subscription_handle_get(subscription_handle)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling MySubscriptionsApi->users_me_subscriptions_subscription_handle_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscription_handle** | **String**| The subscription handle | 

### Return type

[**VideoChannel**](VideoChannel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## users_me_subscriptions_videos_get

> VideoListResponse users_me_subscriptions_videos_get(opts)

List videos of subscriptions of my user

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::MySubscriptionsApi.new
opts = {
  category_one_of: Peertube::OneOfintegerarray.new, # OneOfintegerarray | category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get))
  tags_one_of: Peertube::OneOfstringarray.new, # OneOfstringarray | tag(s) of the video
  tags_all_of: Peertube::OneOfstringarray.new, # OneOfstringarray | tag(s) of the video, where all should be present in the video
  licence_one_of: Peertube::OneOfintegerarray.new, # OneOfintegerarray | licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get))
  language_one_of: Peertube::OneOfstringarray.new, # OneOfstringarray | language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use `_unknown` to filter on videos that don't have a video language
  nsfw: 'nsfw_example', # String | whether to include nsfw videos, if any
  filter: 'filter_example', # String | Special filters (local for instance) which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges) 
  skip_count: 'false', # String | if you don't need the `total` in the response
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: 'sort_example' # String | Sort videos by criteria
}

begin
  #List videos of subscriptions of my user
  result = api_instance.users_me_subscriptions_videos_get(opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling MySubscriptionsApi->users_me_subscriptions_videos_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category_one_of** | [**OneOfintegerarray**](.md)| category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get)) | [optional] 
 **tags_one_of** | [**OneOfstringarray**](.md)| tag(s) of the video | [optional] 
 **tags_all_of** | [**OneOfstringarray**](.md)| tag(s) of the video, where all should be present in the video | [optional] 
 **licence_one_of** | [**OneOfintegerarray**](.md)| licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get)) | [optional] 
 **language_one_of** | [**OneOfstringarray**](.md)| language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | [optional] 
 **nsfw** | **String**| whether to include nsfw videos, if any | [optional] 
 **filter** | **String**| Special filters (local for instance) which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  | [optional] 
 **skip_count** | **String**| if you don&#39;t need the &#x60;total&#x60; in the response | [optional] [default to &#39;false&#39;]
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort videos by criteria | [optional] 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

