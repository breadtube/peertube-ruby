# Peertube::ServerConfigCustomFollowers

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**instance** | [**ServerConfigCustomFollowersInstance**](ServerConfigCustomFollowersInstance.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigCustomFollowers.new(instance: null)
```


