# Peertube::PluginsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**plugins_available_get**](PluginsApi.md#plugins_available_get) | **GET** /plugins/available | List available plugins
[**plugins_get**](PluginsApi.md#plugins_get) | **GET** /plugins | List plugins
[**plugins_install_post**](PluginsApi.md#plugins_install_post) | **POST** /plugins/install | Install a plugin
[**plugins_npm_name_get**](PluginsApi.md#plugins_npm_name_get) | **GET** /plugins/{npmName} | Get a plugin
[**plugins_npm_name_public_settings_get**](PluginsApi.md#plugins_npm_name_public_settings_get) | **GET** /plugins/{npmName}/public-settings | Get a plugin&#39;s public settings
[**plugins_npm_name_registered_settings_get**](PluginsApi.md#plugins_npm_name_registered_settings_get) | **GET** /plugins/{npmName}/registered-settings | Get a plugin&#39;s registered settings
[**plugins_npm_name_settings_put**](PluginsApi.md#plugins_npm_name_settings_put) | **PUT** /plugins/{npmName}/settings | Set a plugin&#39;s settings
[**plugins_uninstall_post**](PluginsApi.md#plugins_uninstall_post) | **POST** /plugins/uninstall | Uninstall a plugin
[**plugins_update_post**](PluginsApi.md#plugins_update_post) | **POST** /plugins/update | Update a plugin



## plugins_available_get

> PluginResponse plugins_available_get(opts)

List available plugins

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::PluginsApi.new
opts = {
  search: 'search_example', # String | 
  plugin_type: 56, # Integer | 
  current_peer_tube_engine: 'current_peer_tube_engine_example', # String | 
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: '-createdAt' # String | Sort column
}

begin
  #List available plugins
  result = api_instance.plugins_available_get(opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling PluginsApi->plugins_available_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search** | **String**|  | [optional] 
 **plugin_type** | **Integer**|  | [optional] 
 **current_peer_tube_engine** | **String**|  | [optional] 
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort column | [optional] 

### Return type

[**PluginResponse**](PluginResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## plugins_get

> PluginResponse plugins_get(opts)

List plugins

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::PluginsApi.new
opts = {
  plugin_type: 56, # Integer | 
  uninstalled: true, # Boolean | 
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: '-createdAt' # String | Sort column
}

begin
  #List plugins
  result = api_instance.plugins_get(opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling PluginsApi->plugins_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plugin_type** | **Integer**|  | [optional] 
 **uninstalled** | **Boolean**|  | [optional] 
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort column | [optional] 

### Return type

[**PluginResponse**](PluginResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## plugins_install_post

> plugins_install_post(opts)

Install a plugin

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::PluginsApi.new
opts = {
  unknown_base_type: Peertube::UNKNOWN_BASE_TYPE.new # UNKNOWN_BASE_TYPE | 
}

begin
  #Install a plugin
  api_instance.plugins_install_post(opts)
rescue Peertube::ApiError => e
  puts "Exception when calling PluginsApi->plugins_install_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unknown_base_type** | [**UNKNOWN_BASE_TYPE**](UNKNOWN_BASE_TYPE.md)|  | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## plugins_npm_name_get

> Plugin plugins_npm_name_get(npm_name)

Get a plugin

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::PluginsApi.new
npm_name = 'peertube-plugin-auth-ldap' # String | name of the plugin/theme on npmjs.com or in its package.json

begin
  #Get a plugin
  result = api_instance.plugins_npm_name_get(npm_name)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling PluginsApi->plugins_npm_name_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **npm_name** | **String**| name of the plugin/theme on npmjs.com or in its package.json | 

### Return type

[**Plugin**](Plugin.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## plugins_npm_name_public_settings_get

> Hash&lt;String, Object&gt; plugins_npm_name_public_settings_get(npm_name)

Get a plugin's public settings

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::PluginsApi.new
npm_name = 'peertube-plugin-auth-ldap' # String | name of the plugin/theme on npmjs.com or in its package.json

begin
  #Get a plugin's public settings
  result = api_instance.plugins_npm_name_public_settings_get(npm_name)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling PluginsApi->plugins_npm_name_public_settings_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **npm_name** | **String**| name of the plugin/theme on npmjs.com or in its package.json | 

### Return type

**Hash&lt;String, Object&gt;**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## plugins_npm_name_registered_settings_get

> Hash&lt;String, Object&gt; plugins_npm_name_registered_settings_get(npm_name)

Get a plugin's registered settings

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::PluginsApi.new
npm_name = 'peertube-plugin-auth-ldap' # String | name of the plugin/theme on npmjs.com or in its package.json

begin
  #Get a plugin's registered settings
  result = api_instance.plugins_npm_name_registered_settings_get(npm_name)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling PluginsApi->plugins_npm_name_registered_settings_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **npm_name** | **String**| name of the plugin/theme on npmjs.com or in its package.json | 

### Return type

**Hash&lt;String, Object&gt;**

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## plugins_npm_name_settings_put

> plugins_npm_name_settings_put(npm_name, opts)

Set a plugin's settings

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::PluginsApi.new
npm_name = 'peertube-plugin-auth-ldap' # String | name of the plugin/theme on npmjs.com or in its package.json
opts = {
  inline_object24: Peertube::InlineObject24.new # InlineObject24 | 
}

begin
  #Set a plugin's settings
  api_instance.plugins_npm_name_settings_put(npm_name, opts)
rescue Peertube::ApiError => e
  puts "Exception when calling PluginsApi->plugins_npm_name_settings_put: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **npm_name** | **String**| name of the plugin/theme on npmjs.com or in its package.json | 
 **inline_object24** | [**InlineObject24**](InlineObject24.md)|  | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## plugins_uninstall_post

> plugins_uninstall_post(opts)

Uninstall a plugin

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::PluginsApi.new
opts = {
  inline_object23: Peertube::InlineObject23.new # InlineObject23 | 
}

begin
  #Uninstall a plugin
  api_instance.plugins_uninstall_post(opts)
rescue Peertube::ApiError => e
  puts "Exception when calling PluginsApi->plugins_uninstall_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object23** | [**InlineObject23**](InlineObject23.md)|  | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## plugins_update_post

> plugins_update_post(opts)

Update a plugin

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::PluginsApi.new
opts = {
  unknown_base_type: Peertube::UNKNOWN_BASE_TYPE.new # UNKNOWN_BASE_TYPE | 
}

begin
  #Update a plugin
  api_instance.plugins_update_post(opts)
rescue Peertube::ApiError => e
  puts "Exception when calling PluginsApi->plugins_update_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unknown_base_type** | [**UNKNOWN_BASE_TYPE**](UNKNOWN_BASE_TYPE.md)|  | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

