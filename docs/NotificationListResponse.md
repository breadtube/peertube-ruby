# Peertube::NotificationListResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Integer** |  | [optional] 
**data** | [**Array&lt;Notification&gt;**](Notification.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::NotificationListResponse.new(total: 1,
                                 data: null)
```


