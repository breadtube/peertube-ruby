# Peertube::ServerConfigAbout

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**instance** | [**ServerConfigAboutInstance**](ServerConfigAboutInstance.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigAbout.new(instance: null)
```


