# Peertube::FileRedundancyInformation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**file_url** | **String** |  | [optional] 
**strategy** | **String** |  | [optional] 
**size** | **Integer** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 
**expires_on** | **DateTime** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::FileRedundancyInformation.new(id: null,
                                 file_url: null,
                                 strategy: null,
                                 size: null,
                                 created_at: null,
                                 updated_at: null,
                                 expires_on: null)
```


