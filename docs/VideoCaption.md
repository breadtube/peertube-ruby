# Peertube::VideoCaption

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | [**VideoConstantString**](VideoConstantString.md) |  | [optional] 
**caption_path** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoCaption.new(language: null,
                                 caption_path: null)
```


