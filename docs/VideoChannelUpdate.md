# Peertube::VideoChannelUpdate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**display_name** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**support** | **String** | A text shown by default on all videos of this channel, to tell the audience how to support it | [optional] 
**bulk_videos_support_update** | **Boolean** | Update the support field for all videos of this channel | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoChannelUpdate.new(display_name: null,
                                 description: null,
                                 support: Please support my work on &lt;insert crowdfunding plateform&gt;! &lt;3,
                                 bulk_videos_support_update: null)
```


