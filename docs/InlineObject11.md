# Peertube::InlineObject11

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**captionfile** | **File** | The file to upload. | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject11.new(captionfile: null)
```


