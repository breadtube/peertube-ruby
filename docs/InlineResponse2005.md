# Peertube::InlineResponse2005

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**video_playlist** | [**InlineResponse2005VideoPlaylist**](InlineResponse2005VideoPlaylist.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineResponse2005.new(video_playlist: null)
```


