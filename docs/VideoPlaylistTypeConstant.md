# Peertube::VideoPlaylistTypeConstant

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**VideoPlaylistTypeSet**](VideoPlaylistTypeSet.md) |  | [optional] 
**label** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoPlaylistTypeConstant.new(id: null,
                                 label: null)
```


