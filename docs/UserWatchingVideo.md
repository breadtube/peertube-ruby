# Peertube::UserWatchingVideo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**current_time** | **Integer** | timestamp within the video, in seconds | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::UserWatchingVideo.new(current_time: 5)
```


