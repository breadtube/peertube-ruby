# Peertube::FeedsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**feeds_video_comments_format_get**](FeedsApi.md#feeds_video_comments_format_get) | **GET** /feeds/video-comments.{format} | List comments on videos
[**feeds_videos_format_get**](FeedsApi.md#feeds_videos_format_get) | **GET** /feeds/videos.{format} | List videos



## feeds_video_comments_format_get

> Array&lt;Object&gt; feeds_video_comments_format_get(format, opts)

List comments on videos

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::FeedsApi.new
format = 'format_example' # String | format expected (we focus on making `rss` the most featureful ; it serves [Media RSS](https://www.rssboard.org/media-rss))
opts = {
  video_id: 'video_id_example', # String | limit listing to a specific video
  account_id: 'account_id_example', # String | limit listing to a specific account
  account_name: 'account_name_example', # String | limit listing to a specific account
  video_channel_id: 'video_channel_id_example', # String | limit listing to a specific video channel
  video_channel_name: 'video_channel_name_example' # String | limit listing to a specific video channel
}

begin
  #List comments on videos
  result = api_instance.feeds_video_comments_format_get(format, opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling FeedsApi->feeds_video_comments_format_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **String**| format expected (we focus on making &#x60;rss&#x60; the most featureful ; it serves [Media RSS](https://www.rssboard.org/media-rss)) | 
 **video_id** | **String**| limit listing to a specific video | [optional] 
 **account_id** | **String**| limit listing to a specific account | [optional] 
 **account_name** | **String**| limit listing to a specific account | [optional] 
 **video_channel_id** | **String**| limit listing to a specific video channel | [optional] 
 **video_channel_name** | **String**| limit listing to a specific video channel | [optional] 

### Return type

**Array&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/xml, application/rss+xml, text/xml, application/atom+xml, application/json


## feeds_videos_format_get

> Array&lt;Object&gt; feeds_videos_format_get(format, opts)

List videos

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::FeedsApi.new
format = 'format_example' # String | format expected (we focus on making `rss` the most featureful ; it serves [Media RSS](https://www.rssboard.org/media-rss))
opts = {
  account_id: 'account_id_example', # String | limit listing to a specific account
  account_name: 'account_name_example', # String | limit listing to a specific account
  video_channel_id: 'video_channel_id_example', # String | limit listing to a specific video channel
  video_channel_name: 'video_channel_name_example', # String | limit listing to a specific video channel
  sort: '-createdAt', # String | Sort column
  nsfw: 'nsfw_example', # String | whether to include nsfw videos, if any
  filter: 'filter_example' # String | Special filters (local for instance) which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges) 
}

begin
  #List videos
  result = api_instance.feeds_videos_format_get(format, opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling FeedsApi->feeds_videos_format_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **String**| format expected (we focus on making &#x60;rss&#x60; the most featureful ; it serves [Media RSS](https://www.rssboard.org/media-rss)) | 
 **account_id** | **String**| limit listing to a specific account | [optional] 
 **account_name** | **String**| limit listing to a specific account | [optional] 
 **video_channel_id** | **String**| limit listing to a specific video channel | [optional] 
 **video_channel_name** | **String**| limit listing to a specific video channel | [optional] 
 **sort** | **String**| Sort column | [optional] 
 **nsfw** | **String**| whether to include nsfw videos, if any | [optional] 
 **filter** | **String**| Special filters (local for instance) which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  | [optional] 

### Return type

**Array&lt;Object&gt;**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/xml, application/rss+xml, text/xml, application/atom+xml, application/json

