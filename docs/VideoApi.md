# Peertube::VideoApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accounts_name_videos_get**](VideoApi.md#accounts_name_videos_get) | **GET** /accounts/{name}/videos | List videos of an account
[**video_channels_channel_handle_videos_get**](VideoApi.md#video_channels_channel_handle_videos_get) | **GET** /video-channels/{channelHandle}/videos | List videos of a video channel
[**videos_categories_get**](VideoApi.md#videos_categories_get) | **GET** /videos/categories | List available video categories
[**videos_get**](VideoApi.md#videos_get) | **GET** /videos | List videos
[**videos_id_delete**](VideoApi.md#videos_id_delete) | **DELETE** /videos/{id} | Delete a video
[**videos_id_description_get**](VideoApi.md#videos_id_description_get) | **GET** /videos/{id}/description | Get complete video description
[**videos_id_get**](VideoApi.md#videos_id_get) | **GET** /videos/{id} | Get a video
[**videos_id_put**](VideoApi.md#videos_id_put) | **PUT** /videos/{id} | Update a video
[**videos_id_views_post**](VideoApi.md#videos_id_views_post) | **POST** /videos/{id}/views | Add a view to a video
[**videos_id_watching_put**](VideoApi.md#videos_id_watching_put) | **PUT** /videos/{id}/watching | Set watching progress of a video
[**videos_imports_post**](VideoApi.md#videos_imports_post) | **POST** /videos/imports | Import a video
[**videos_languages_get**](VideoApi.md#videos_languages_get) | **GET** /videos/languages | List available video languages
[**videos_licences_get**](VideoApi.md#videos_licences_get) | **GET** /videos/licences | List available video licences
[**videos_privacies_get**](VideoApi.md#videos_privacies_get) | **GET** /videos/privacies | List available video privacies
[**videos_upload_post**](VideoApi.md#videos_upload_post) | **POST** /videos/upload | Upload a video



## accounts_name_videos_get

> VideoListResponse accounts_name_videos_get(name, opts)

List videos of an account

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoApi.new
name = 'chocobozzz | chocobozzz@example.org' # String | The name of the account
opts = {
  category_one_of: Peertube::OneOfintegerarray.new, # OneOfintegerarray | category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get))
  tags_one_of: Peertube::OneOfstringarray.new, # OneOfstringarray | tag(s) of the video
  tags_all_of: Peertube::OneOfstringarray.new, # OneOfstringarray | tag(s) of the video, where all should be present in the video
  licence_one_of: Peertube::OneOfintegerarray.new, # OneOfintegerarray | licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get))
  language_one_of: Peertube::OneOfstringarray.new, # OneOfstringarray | language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use `_unknown` to filter on videos that don't have a video language
  nsfw: 'nsfw_example', # String | whether to include nsfw videos, if any
  filter: 'filter_example', # String | Special filters (local for instance) which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges) 
  skip_count: 'false', # String | if you don't need the `total` in the response
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: 'sort_example' # String | Sort videos by criteria
}

begin
  #List videos of an account
  result = api_instance.accounts_name_videos_get(name, opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoApi->accounts_name_videos_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| The name of the account | 
 **category_one_of** | [**OneOfintegerarray**](.md)| category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get)) | [optional] 
 **tags_one_of** | [**OneOfstringarray**](.md)| tag(s) of the video | [optional] 
 **tags_all_of** | [**OneOfstringarray**](.md)| tag(s) of the video, where all should be present in the video | [optional] 
 **licence_one_of** | [**OneOfintegerarray**](.md)| licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get)) | [optional] 
 **language_one_of** | [**OneOfstringarray**](.md)| language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | [optional] 
 **nsfw** | **String**| whether to include nsfw videos, if any | [optional] 
 **filter** | **String**| Special filters (local for instance) which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  | [optional] 
 **skip_count** | **String**| if you don&#39;t need the &#x60;total&#x60; in the response | [optional] [default to &#39;false&#39;]
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort videos by criteria | [optional] 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## video_channels_channel_handle_videos_get

> VideoListResponse video_channels_channel_handle_videos_get(channel_handle, opts)

List videos of a video channel

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoApi.new
channel_handle = 'my_username | my_username@example.com' # String | The video channel handle
opts = {
  category_one_of: Peertube::OneOfintegerarray.new, # OneOfintegerarray | category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get))
  tags_one_of: Peertube::OneOfstringarray.new, # OneOfstringarray | tag(s) of the video
  tags_all_of: Peertube::OneOfstringarray.new, # OneOfstringarray | tag(s) of the video, where all should be present in the video
  licence_one_of: Peertube::OneOfintegerarray.new, # OneOfintegerarray | licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get))
  language_one_of: Peertube::OneOfstringarray.new, # OneOfstringarray | language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use `_unknown` to filter on videos that don't have a video language
  nsfw: 'nsfw_example', # String | whether to include nsfw videos, if any
  filter: 'filter_example', # String | Special filters (local for instance) which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges) 
  skip_count: 'false', # String | if you don't need the `total` in the response
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: 'sort_example' # String | Sort videos by criteria
}

begin
  #List videos of a video channel
  result = api_instance.video_channels_channel_handle_videos_get(channel_handle, opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoApi->video_channels_channel_handle_videos_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_handle** | **String**| The video channel handle | 
 **category_one_of** | [**OneOfintegerarray**](.md)| category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get)) | [optional] 
 **tags_one_of** | [**OneOfstringarray**](.md)| tag(s) of the video | [optional] 
 **tags_all_of** | [**OneOfstringarray**](.md)| tag(s) of the video, where all should be present in the video | [optional] 
 **licence_one_of** | [**OneOfintegerarray**](.md)| licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get)) | [optional] 
 **language_one_of** | [**OneOfstringarray**](.md)| language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | [optional] 
 **nsfw** | **String**| whether to include nsfw videos, if any | [optional] 
 **filter** | **String**| Special filters (local for instance) which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  | [optional] 
 **skip_count** | **String**| if you don&#39;t need the &#x60;total&#x60; in the response | [optional] [default to &#39;false&#39;]
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort videos by criteria | [optional] 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## videos_categories_get

> Array&lt;String&gt; videos_categories_get

List available video categories

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoApi.new

begin
  #List available video categories
  result = api_instance.videos_categories_get
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoApi->videos_categories_get: #{e}"
end
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Array&lt;String&gt;**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## videos_get

> VideoListResponse videos_get(opts)

List videos

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoApi.new
opts = {
  category_one_of: Peertube::OneOfintegerarray.new, # OneOfintegerarray | category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get))
  tags_one_of: Peertube::OneOfstringarray.new, # OneOfstringarray | tag(s) of the video
  tags_all_of: Peertube::OneOfstringarray.new, # OneOfstringarray | tag(s) of the video, where all should be present in the video
  licence_one_of: Peertube::OneOfintegerarray.new, # OneOfintegerarray | licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get))
  language_one_of: Peertube::OneOfstringarray.new, # OneOfstringarray | language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use `_unknown` to filter on videos that don't have a video language
  nsfw: 'nsfw_example', # String | whether to include nsfw videos, if any
  filter: 'filter_example', # String | Special filters (local for instance) which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges) 
  skip_count: 'false', # String | if you don't need the `total` in the response
  start: 56, # Integer | Offset used to paginate results
  count: 15, # Integer | Number of items to return
  sort: 'sort_example' # String | Sort videos by criteria
}

begin
  #List videos
  result = api_instance.videos_get(opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoApi->videos_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category_one_of** | [**OneOfintegerarray**](.md)| category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get)) | [optional] 
 **tags_one_of** | [**OneOfstringarray**](.md)| tag(s) of the video | [optional] 
 **tags_all_of** | [**OneOfstringarray**](.md)| tag(s) of the video, where all should be present in the video | [optional] 
 **licence_one_of** | [**OneOfintegerarray**](.md)| licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get)) | [optional] 
 **language_one_of** | [**OneOfstringarray**](.md)| language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | [optional] 
 **nsfw** | **String**| whether to include nsfw videos, if any | [optional] 
 **filter** | **String**| Special filters (local for instance) which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  | [optional] 
 **skip_count** | **String**| if you don&#39;t need the &#x60;total&#x60; in the response | [optional] [default to &#39;false&#39;]
 **start** | **Integer**| Offset used to paginate results | [optional] 
 **count** | **Integer**| Number of items to return | [optional] [default to 15]
 **sort** | **String**| Sort videos by criteria | [optional] 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## videos_id_delete

> videos_id_delete(id)

Delete a video

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid

begin
  #Delete a video
  api_instance.videos_id_delete(id)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoApi->videos_id_delete: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## videos_id_description_get

> String videos_id_description_get(id)

Get complete video description

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid

begin
  #Get complete video description
  result = api_instance.videos_id_description_get(id)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoApi->videos_id_description_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## videos_id_get

> VideoDetails videos_id_get(id)

Get a video

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid

begin
  #Get a video
  result = api_instance.videos_id_get(id)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoApi->videos_id_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

[**VideoDetails**](VideoDetails.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## videos_id_put

> videos_id_put(id, opts)

Update a video

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid
opts = {
  thumbnailfile: File.new('/path/to/file'), # File | Video thumbnail file
  previewfile: File.new('/path/to/file'), # File | Video preview file
  category: 56, # Integer | Video category
  licence: 56, # Integer | Video licence
  language: 'language_example', # String | Video language
  privacy: Peertube::VideoPrivacySet.new, # VideoPrivacySet | 
  description: 'description_example', # String | Video description
  wait_transcoding: 'wait_transcoding_example', # String | Whether or not we wait transcoding before publish the video
  support: 'support_example', # String | A text tell the audience how to support the video creator
  nsfw: true, # Boolean | Whether or not this video contains sensitive content
  name: 'name_example', # String | Video name
  tags: 'tags_example', # Array<String> | Video tags (maximum 5 tags each between 2 and 30 characters)
  comments_enabled: true, # Boolean | Enable or disable comments for this video
  originally_published_at: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Date when the content was originally published
  schedule_update: Peertube::VideoScheduledUpdate.new # VideoScheduledUpdate | 
}

begin
  #Update a video
  api_instance.videos_id_put(id, opts)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoApi->videos_id_put: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **thumbnailfile** | **File**| Video thumbnail file | [optional] 
 **previewfile** | **File**| Video preview file | [optional] 
 **category** | **Integer**| Video category | [optional] 
 **licence** | **Integer**| Video licence | [optional] 
 **language** | **String**| Video language | [optional] 
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md)|  | [optional] 
 **description** | **String**| Video description | [optional] 
 **wait_transcoding** | **String**| Whether or not we wait transcoding before publish the video | [optional] 
 **support** | **String**| A text tell the audience how to support the video creator | [optional] 
 **nsfw** | **Boolean**| Whether or not this video contains sensitive content | [optional] 
 **name** | **String**| Video name | [optional] 
 **tags** | [**Array&lt;String&gt;**](String.md)| Video tags (maximum 5 tags each between 2 and 30 characters) | [optional] 
 **comments_enabled** | **Boolean**| Enable or disable comments for this video | [optional] 
 **originally_published_at** | **DateTime**| Date when the content was originally published | [optional] 
 **schedule_update** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md)|  | [optional] 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: Not defined


## videos_id_views_post

> videos_id_views_post(id)

Add a view to a video

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid

begin
  #Add a view to a video
  api_instance.videos_id_views_post(id)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoApi->videos_id_views_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## videos_id_watching_put

> videos_id_watching_put(id, user_watching_video)

Set watching progress of a video

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid
user_watching_video = Peertube::UserWatchingVideo.new # UserWatchingVideo | 

begin
  #Set watching progress of a video
  api_instance.videos_id_watching_put(id, user_watching_video)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoApi->videos_id_watching_put: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **user_watching_video** | [**UserWatchingVideo**](UserWatchingVideo.md)|  | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## videos_imports_post

> VideoUploadResponse videos_imports_post(channel_id, name, opts)

Import a video

Import a torrent or magnetURI or HTTP resource (if enabled by the instance administrator)

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoApi.new
channel_id = 56 # Integer | Channel id that will contain this video
name = 'name_example' # String | Video name
opts = {
  torrentfile: File.new('/path/to/file'), # File | Torrent File
  target_url: 'target_url_example', # String | HTTP target URL
  magnet_uri: 'magnet_uri_example', # String | Magnet URI
  thumbnailfile: File.new('/path/to/file'), # File | Video thumbnail file
  previewfile: File.new('/path/to/file'), # File | Video preview file
  privacy: Peertube::VideoPrivacySet.new, # VideoPrivacySet | 
  category: 'category_example', # String | Video category
  licence: 'licence_example', # String | Video licence
  language: 'language_example', # String | Video language
  description: 'description_example', # String | Video description
  wait_transcoding: 'wait_transcoding_example', # String | Whether or not we wait transcoding before publish the video
  support: 'support_example', # String | A text tell the audience how to support the video creator
  nsfw: 'nsfw_example', # String | Whether or not this video contains sensitive content
  tags: 'tags_example', # Array<String> | Video tags (maximum 5 tags each between 2 and 30 characters)
  comments_enabled: 'comments_enabled_example', # String | Enable or disable comments for this video
  schedule_update: Peertube::VideoScheduledUpdate.new # VideoScheduledUpdate | 
}

begin
  #Import a video
  result = api_instance.videos_imports_post(channel_id, name, opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoApi->videos_imports_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_id** | **Integer**| Channel id that will contain this video | 
 **name** | **String**| Video name | 
 **torrentfile** | **File**| Torrent File | [optional] 
 **target_url** | **String**| HTTP target URL | [optional] 
 **magnet_uri** | **String**| Magnet URI | [optional] 
 **thumbnailfile** | **File**| Video thumbnail file | [optional] 
 **previewfile** | **File**| Video preview file | [optional] 
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md)|  | [optional] 
 **category** | **String**| Video category | [optional] 
 **licence** | **String**| Video licence | [optional] 
 **language** | **String**| Video language | [optional] 
 **description** | **String**| Video description | [optional] 
 **wait_transcoding** | **String**| Whether or not we wait transcoding before publish the video | [optional] 
 **support** | **String**| A text tell the audience how to support the video creator | [optional] 
 **nsfw** | **String**| Whether or not this video contains sensitive content | [optional] 
 **tags** | [**Array&lt;String&gt;**](String.md)| Video tags (maximum 5 tags each between 2 and 30 characters) | [optional] 
 **comments_enabled** | **String**| Enable or disable comments for this video | [optional] 
 **schedule_update** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md)|  | [optional] 

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## videos_languages_get

> Array&lt;String&gt; videos_languages_get

List available video languages

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoApi.new

begin
  #List available video languages
  result = api_instance.videos_languages_get
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoApi->videos_languages_get: #{e}"
end
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Array&lt;String&gt;**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## videos_licences_get

> Array&lt;String&gt; videos_licences_get

List available video licences

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoApi.new

begin
  #List available video licences
  result = api_instance.videos_licences_get
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoApi->videos_licences_get: #{e}"
end
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Array&lt;String&gt;**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## videos_privacies_get

> Array&lt;String&gt; videos_privacies_get

List available video privacies

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoApi.new

begin
  #List available video privacies
  result = api_instance.videos_privacies_get
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoApi->videos_privacies_get: #{e}"
end
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Array&lt;String&gt;**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## videos_upload_post

> VideoUploadResponse videos_upload_post(videofile, channel_id, name, opts)

Upload a video

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoApi.new
videofile = File.new('/path/to/file') # File | Video file
channel_id = 56 # Integer | Channel id that will contain this video
name = 'name_example' # String | Video name
opts = {
  thumbnailfile: File.new('/path/to/file'), # File | Video thumbnail file
  previewfile: File.new('/path/to/file'), # File | Video preview file
  privacy: Peertube::VideoPrivacySet.new, # VideoPrivacySet | 
  category: 56, # Integer | Video category
  licence: 'licence_example', # String | Video licence
  language: 56, # Integer | Video language
  description: 'description_example', # String | Video description
  wait_transcoding: 'wait_transcoding_example', # String | Whether or not we wait transcoding before publish the video
  support: 'support_example', # String | A text tell the audience how to support the video creator
  nsfw: true, # Boolean | Whether or not this video contains sensitive content
  tags: 'tags_example', # Array<String> | Video tags (maximum 5 tags each between 2 and 30 characters)
  comments_enabled: true, # Boolean | Enable or disable comments for this video
  originally_published_at: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Date when the content was originally published
  schedule_update: Peertube::VideoScheduledUpdate.new # VideoScheduledUpdate | 
}

begin
  #Upload a video
  result = api_instance.videos_upload_post(videofile, channel_id, name, opts)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoApi->videos_upload_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videofile** | **File**| Video file | 
 **channel_id** | **Integer**| Channel id that will contain this video | 
 **name** | **String**| Video name | 
 **thumbnailfile** | **File**| Video thumbnail file | [optional] 
 **previewfile** | **File**| Video preview file | [optional] 
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md)|  | [optional] 
 **category** | **Integer**| Video category | [optional] 
 **licence** | **String**| Video licence | [optional] 
 **language** | **Integer**| Video language | [optional] 
 **description** | **String**| Video description | [optional] 
 **wait_transcoding** | **String**| Whether or not we wait transcoding before publish the video | [optional] 
 **support** | **String**| A text tell the audience how to support the video creator | [optional] 
 **nsfw** | **Boolean**| Whether or not this video contains sensitive content | [optional] 
 **tags** | [**Array&lt;String&gt;**](String.md)| Video tags (maximum 5 tags each between 2 and 30 characters) | [optional] 
 **comments_enabled** | **Boolean**| Enable or disable comments for this video | [optional] 
 **originally_published_at** | **DateTime**| Date when the content was originally published | [optional] 
 **schedule_update** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md)|  | [optional] 

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

