# Peertube::VideoStreamingPlaylists

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**type** | **Integer** | Playlist type (HLS &#x3D; &#x60;1&#x60;) | [optional] 
**playlist_url** | **String** |  | [optional] 
**segments_sha256_url** | **String** |  | [optional] 
**files** | [**Array&lt;VideoFile&gt;**](VideoFile.md) |  | [optional] 
**redundancies** | [**Array&lt;VideoStreamingPlaylistsRedundancies&gt;**](VideoStreamingPlaylistsRedundancies.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoStreamingPlaylists.new(id: null,
                                 type: null,
                                 playlist_url: null,
                                 segments_sha256_url: null,
                                 files: null,
                                 redundancies: null)
```


