# Peertube::ServerConfigCustomFollowersInstance

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **Boolean** |  | [optional] 
**manual_approval** | **Boolean** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigCustomFollowersInstance.new(enabled: null,
                                 manual_approval: null)
```


