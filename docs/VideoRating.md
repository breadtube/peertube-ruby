# Peertube::VideoRating

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**video** | [**Video**](Video.md) |  | 
**rating** | **Float** | Rating of the video | 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoRating.new(video: null,
                                 rating: null)
```


