# Peertube::VideoStreamingPlaylistsRedundancies

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**base_url** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoStreamingPlaylistsRedundancies.new(base_url: null)
```


