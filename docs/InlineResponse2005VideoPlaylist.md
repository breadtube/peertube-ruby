# Peertube::InlineResponse2005VideoPlaylist

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**uuid** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineResponse2005VideoPlaylist.new(id: null,
                                 uuid: null)
```


