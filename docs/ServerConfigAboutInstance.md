# Peertube::ServerConfigAboutInstance

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**short_description** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**terms** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigAboutInstance.new(name: null,
                                 short_description: null,
                                 description: null,
                                 terms: null)
```


