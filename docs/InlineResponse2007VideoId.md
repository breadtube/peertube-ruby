# Peertube::InlineResponse2007VideoId

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**playlist_element_id** | **Integer** |  | [optional] 
**playlist_id** | **Integer** |  | [optional] 
**start_timestamp** | **Integer** |  | [optional] 
**stop_timestamp** | **Integer** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineResponse2007VideoId.new(playlist_element_id: null,
                                 playlist_id: null,
                                 start_timestamp: null,
                                 stop_timestamp: null)
```


