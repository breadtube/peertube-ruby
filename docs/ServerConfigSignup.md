# Peertube::ServerConfigSignup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allowed** | **Boolean** |  | [optional] 
**allowed_for_current_ip** | **Boolean** |  | [optional] 
**requires_email_verification** | **Boolean** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigSignup.new(allowed: null,
                                 allowed_for_current_ip: null,
                                 requires_email_verification: null)
```


