# Peertube::AddUserResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user** | [**AddUserResponseUser**](AddUserResponseUser.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::AddUserResponse.new(user: null)
```


