# Peertube::AccountSummary

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**name** | **String** |  | [optional] 
**display_name** | **String** |  | [optional] 
**url** | **String** |  | [optional] 
**host** | **String** |  | [optional] 
**avatar** | [**Avatar**](Avatar.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::AccountSummary.new(id: null,
                                 name: null,
                                 display_name: null,
                                 url: null,
                                 host: null,
                                 avatar: null)
```


