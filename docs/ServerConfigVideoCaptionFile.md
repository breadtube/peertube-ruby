# Peertube::ServerConfigVideoCaptionFile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**size** | [**ServerConfigAvatarFileSize**](ServerConfigAvatarFileSize.md) |  | [optional] 
**extensions** | **Array&lt;String&gt;** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigVideoCaptionFile.new(size: null,
                                 extensions: null)
```


