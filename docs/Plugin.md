# Peertube::Plugin

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**type** | **Integer** | - &#x60;1&#x60;: PLUGIN - &#x60;2&#x60;: THEME  | [optional] 
**latest_version** | **String** |  | [optional] 
**version** | **String** |  | [optional] 
**enabled** | **Boolean** |  | [optional] 
**uninstalled** | **Boolean** |  | [optional] 
**peertube_engine** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**homepage** | **String** |  | [optional] 
**settings** | **Hash&lt;String, Object&gt;** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::Plugin.new(name: peertube-plugin-auth-ldap,
                                 type: null,
                                 latest_version: 0.0.3,
                                 version: 0.0.1,
                                 enabled: null,
                                 uninstalled: null,
                                 peertube_engine: 2.2.0,
                                 description: null,
                                 homepage: https://framagit.org/framasoft/peertube/official-plugins/tree/master/peertube-plugin-auth-ldap,
                                 settings: null,
                                 created_at: null,
                                 updated_at: null)
```


