# Peertube::InlineObject1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uri** | **String** | uri of the video channels to subscribe to | 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject1.new(uri: null)
```


