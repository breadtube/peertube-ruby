# Peertube::VideoImport

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**target_url** | **String** |  | [optional] 
**magnet_uri** | **String** |  | [optional] 
**torrent_name** | **String** |  | [optional] 
**state** | [**VideoImportStateConstant**](VideoImportStateConstant.md) |  | [optional] 
**error** | **String** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 
**video** | [**Video**](Video.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoImport.new(id: 2,
                                 target_url: https://framatube.org/videos/watch/9c9de5e8-0a1e-484a-b099-e80766180a6d,
                                 magnet_uri: magnet:?xs&#x3D;https%3A%2F%2Fframatube.org%2Fstatic%2Ftorrents%2F9c9de5e8-0a1e-484a-b099-e80766180a6d-240.torrent&amp;xt&#x3D;urn:btih:38b4747ff788b30bf61f59d1965cd38f9e48e01f&amp;dn&#x3D;What+is+PeerTube%3F&amp;tr&#x3D;wss%3A%2F%2Fframatube.org%2Ftracker%2Fsocket&amp;tr&#x3D;https%3A%2F%2Fframatube.org%2Ftracker%2Fannounce&amp;ws&#x3D;https%3A%2F%2Fframatube.org%2Fstatic%2Fwebseed%2F9c9de5e8-0a1e-484a-b099-e80766180a6d-240.mp4,
                                 torrent_name: null,
                                 state: null,
                                 error: null,
                                 created_at: null,
                                 updated_at: null,
                                 video: null)
```


