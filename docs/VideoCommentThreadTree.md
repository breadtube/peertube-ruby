# Peertube::VideoCommentThreadTree

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**comment** | [**VideoComment**](VideoComment.md) |  | [optional] 
**children** | [**Array&lt;VideoCommentThreadTree&gt;**](VideoCommentThreadTree.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoCommentThreadTree.new(comment: null,
                                 children: null)
```


