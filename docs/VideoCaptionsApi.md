# Peertube::VideoCaptionsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**videos_id_captions_caption_language_delete**](VideoCaptionsApi.md#videos_id_captions_caption_language_delete) | **DELETE** /videos/{id}/captions/{captionLanguage} | Delete a video caption
[**videos_id_captions_caption_language_put**](VideoCaptionsApi.md#videos_id_captions_caption_language_put) | **PUT** /videos/{id}/captions/{captionLanguage} | Add or replace a video caption
[**videos_id_captions_get**](VideoCaptionsApi.md#videos_id_captions_get) | **GET** /videos/{id}/captions | List captions of a video



## videos_id_captions_caption_language_delete

> videos_id_captions_caption_language_delete(id, caption_language)

Delete a video caption

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoCaptionsApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid
caption_language = 'caption_language_example' # String | The caption language

begin
  #Delete a video caption
  api_instance.videos_id_captions_caption_language_delete(id, caption_language)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoCaptionsApi->videos_id_captions_caption_language_delete: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **caption_language** | **String**| The caption language | 

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## videos_id_captions_caption_language_put

> videos_id_captions_caption_language_put(id, caption_language, opts)

Add or replace a video caption

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoCaptionsApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid
caption_language = 'caption_language_example' # String | The caption language
opts = {
  captionfile: File.new('/path/to/file') # File | The file to upload.
}

begin
  #Add or replace a video caption
  api_instance.videos_id_captions_caption_language_put(id, caption_language, opts)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoCaptionsApi->videos_id_captions_caption_language_put: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **caption_language** | **String**| The caption language | 
 **captionfile** | **File**| The file to upload. | [optional] 

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: Not defined


## videos_id_captions_get

> InlineResponse2002 videos_id_captions_get(id)

List captions of a video

### Example

```ruby
# load the gem
require 'peertube'

api_instance = Peertube::VideoCaptionsApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid

begin
  #List captions of a video
  result = api_instance.videos_id_captions_get(id)
  p result
rescue Peertube::ApiError => e
  puts "Exception when calling VideoCaptionsApi->videos_id_captions_get: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

