# Peertube::CommentThreadResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Integer** |  | [optional] 
**data** | [**Array&lt;VideoComment&gt;**](VideoComment.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::CommentThreadResponse.new(total: 1,
                                 data: null)
```


