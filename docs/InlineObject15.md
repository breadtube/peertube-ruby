# Peertube::InlineObject15

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_position** | **Integer** | Start position of the element to reorder | 
**insert_after_position** | **Integer** | New position for the block to reorder, to add the block before the first element | 
**reorder_length** | **Integer** | How many element from &#x60;startPosition&#x60; to reorder | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject15.new(start_position: null,
                                 insert_after_position: null,
                                 reorder_length: null)
```


