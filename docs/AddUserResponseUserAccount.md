# Peertube::AddUserResponseUserAccount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::AddUserResponseUserAccount.new(id: 37)
```


