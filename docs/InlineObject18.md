# Peertube::InlineObject18

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**text** | **String** | Text comment | 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject18.new(text: null)
```


