# Peertube::VideoFile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**magnet_uri** | **String** |  | [optional] 
**resolution** | [**VideoResolutionConstant**](VideoResolutionConstant.md) |  | [optional] 
**size** | **Integer** | Video file size in bytes | [optional] 
**torrent_url** | **String** |  | [optional] 
**torrent_download_url** | **String** |  | [optional] 
**file_url** | **String** |  | [optional] 
**file_download_url** | **String** |  | [optional] 
**fps** | **Float** |  | [optional] 
**metadata_url** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoFile.new(magnet_uri: null,
                                 resolution: null,
                                 size: null,
                                 torrent_url: null,
                                 torrent_download_url: null,
                                 file_url: null,
                                 file_download_url: null,
                                 fps: null,
                                 metadata_url: null)
```


