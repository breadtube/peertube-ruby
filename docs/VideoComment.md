# Peertube::VideoComment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**url** | **String** |  | [optional] 
**text** | **String** |  | [optional] 
**thread_id** | **Integer** |  | [optional] 
**in_reply_to_comment_id** | **Integer** |  | [optional] 
**video_id** | **Integer** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 
**total_replies_from_video_author** | **Integer** |  | [optional] 
**total_replies** | **Integer** |  | [optional] 
**account** | [**Account**](Account.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::VideoComment.new(id: null,
                                 url: null,
                                 text: null,
                                 thread_id: null,
                                 in_reply_to_comment_id: null,
                                 video_id: null,
                                 created_at: null,
                                 updated_at: null,
                                 total_replies_from_video_author: null,
                                 total_replies: null,
                                 account: null)
```


