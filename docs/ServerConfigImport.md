# Peertube::ServerConfigImport

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**videos** | [**ServerConfigImportVideos**](ServerConfigImportVideos.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigImport.new(videos: null)
```


