# Peertube::ServerConfig

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**instance** | [**ServerConfigInstance**](ServerConfigInstance.md) |  | [optional] 
**search** | [**ServerConfigSearch**](ServerConfigSearch.md) |  | [optional] 
**plugin** | [**ServerConfigPlugin**](ServerConfigPlugin.md) |  | [optional] 
**theme** | [**ServerConfigPlugin**](ServerConfigPlugin.md) |  | [optional] 
**email** | [**ServerConfigEmail**](ServerConfigEmail.md) |  | [optional] 
**contact_form** | [**ServerConfigEmail**](ServerConfigEmail.md) |  | [optional] 
**server_version** | **String** |  | [optional] 
**server_commit** | **String** |  | [optional] 
**signup** | [**ServerConfigSignup**](ServerConfigSignup.md) |  | [optional] 
**transcoding** | [**ServerConfigTranscoding**](ServerConfigTranscoding.md) |  | [optional] 
**import** | [**ServerConfigImport**](ServerConfigImport.md) |  | [optional] 
**auto_blacklist** | [**ServerConfigAutoBlacklist**](ServerConfigAutoBlacklist.md) |  | [optional] 
**avatar** | [**ServerConfigAvatar**](ServerConfigAvatar.md) |  | [optional] 
**video** | [**ServerConfigVideo**](ServerConfigVideo.md) |  | [optional] 
**video_caption** | [**ServerConfigVideoCaption**](ServerConfigVideoCaption.md) |  | [optional] 
**user** | [**ServerConfigUser**](ServerConfigUser.md) |  | [optional] 
**trending** | [**ServerConfigTrending**](ServerConfigTrending.md) |  | [optional] 
**tracker** | [**ServerConfigEmail**](ServerConfigEmail.md) |  | [optional] 
**followings** | [**ServerConfigFollowings**](ServerConfigFollowings.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfig.new(instance: null,
                                 search: null,
                                 plugin: null,
                                 theme: null,
                                 email: null,
                                 contact_form: null,
                                 server_version: null,
                                 server_commit: null,
                                 signup: null,
                                 transcoding: null,
                                 import: null,
                                 auto_blacklist: null,
                                 avatar: null,
                                 video: null,
                                 video_caption: null,
                                 user: null,
                                 trending: null,
                                 tracker: null,
                                 followings: null)
```


