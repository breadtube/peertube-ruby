# Peertube::InlineResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Integer** |  | [optional] 
**data** | [**Array&lt;Job&gt;**](Job.md) |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineResponse200.new(total: 1,
                                 data: null)
```


