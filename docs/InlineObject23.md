# Peertube::InlineObject23

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**npm_name** | **String** | name of the plugin/theme in its package.json | 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject23.new(npm_name: peertube-plugin-auth-ldap)
```


