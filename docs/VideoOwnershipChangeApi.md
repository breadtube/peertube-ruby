# Peertube::VideoOwnershipChangeApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**videos_id_give_ownership_post**](VideoOwnershipChangeApi.md#videos_id_give_ownership_post) | **POST** /videos/{id}/give-ownership | Request ownership change
[**videos_ownership_get**](VideoOwnershipChangeApi.md#videos_ownership_get) | **GET** /videos/ownership | List video ownership changes
[**videos_ownership_id_accept_post**](VideoOwnershipChangeApi.md#videos_ownership_id_accept_post) | **POST** /videos/ownership/{id}/accept | Accept ownership change request
[**videos_ownership_id_refuse_post**](VideoOwnershipChangeApi.md#videos_ownership_id_refuse_post) | **POST** /videos/ownership/{id}/refuse | Refuse ownership change request



## videos_id_give_ownership_post

> videos_id_give_ownership_post(id, username)

Request ownership change

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoOwnershipChangeApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid
username = 'username_example' # String | 

begin
  #Request ownership change
  api_instance.videos_id_give_ownership_post(id, username)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoOwnershipChangeApi->videos_id_give_ownership_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **username** | **String**|  | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: Not defined


## videos_ownership_get

> videos_ownership_get

List video ownership changes

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoOwnershipChangeApi.new

begin
  #List video ownership changes
  api_instance.videos_ownership_get
rescue Peertube::ApiError => e
  puts "Exception when calling VideoOwnershipChangeApi->videos_ownership_get: #{e}"
end
```

### Parameters

This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## videos_ownership_id_accept_post

> videos_ownership_id_accept_post(id)

Accept ownership change request

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoOwnershipChangeApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid

begin
  #Accept ownership change request
  api_instance.videos_ownership_id_accept_post(id)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoOwnershipChangeApi->videos_ownership_id_accept_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## videos_ownership_id_refuse_post

> videos_ownership_id_refuse_post(id)

Refuse ownership change request

### Example

```ruby
# load the gem
require 'peertube'
# setup authorization
Peertube.configure do |config|
  # Configure OAuth2 access token for authorization: OAuth2
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = Peertube::VideoOwnershipChangeApi.new
id = Peertube::OneOfintegerUUID.new # OneOfintegerUUID | The object id or uuid

begin
  #Refuse ownership change request
  api_instance.videos_ownership_id_refuse_post(id)
rescue Peertube::ApiError => e
  puts "Exception when calling VideoOwnershipChangeApi->videos_ownership_id_refuse_post: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

nil (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

