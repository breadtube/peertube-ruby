# Peertube::AbuseStateConstant

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**AbuseStateSet**](AbuseStateSet.md) |  | [optional] 
**label** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::AbuseStateConstant.new(id: null,
                                 label: null)
```


