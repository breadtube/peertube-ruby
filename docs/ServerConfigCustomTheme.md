# Peertube::ServerConfigCustomTheme

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**default** | **String** |  | [optional] 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::ServerConfigCustomTheme.new(default: null)
```


