# Peertube::InlineObject19

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_name** | **String** | account to block, in the form &#x60;username@domain&#x60; | 

## Code Sample

```ruby
require 'Peertube'

instance = Peertube::InlineObject19.new(account_name: chocobozzz@example.org)
```


