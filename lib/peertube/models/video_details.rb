=begin
#PeerTube

## Introduction  The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [Quick Start guide](https://docs.joinpeertube.org/#/api-rest-getting-started) so you can play with the PeerTube API.  # Authentication  When you sign up for an account, you are given the possibility to generate sessions, and authenticate using this session token. One session token can currently be used at a time.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/#/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call. The body of the response will be JSON in the following format.  ``` {   \"code\": \"unauthorized_request\", // example inner error code   \"error\": \"Token is invalid.\" // example exposed error message } ``` 

The version of the OpenAPI document: 2.3.0

Generated by: https://openapi-generator.tech
OpenAPI Generator version: 4.3.1

=end

require 'date'

module Peertube
  class VideoDetails
    attr_accessor :id

    attr_accessor :uuid

    attr_accessor :created_at

    attr_accessor :published_at

    attr_accessor :updated_at

    attr_accessor :originally_published_at

    attr_accessor :category

    attr_accessor :licence

    attr_accessor :language

    attr_accessor :privacy

    attr_accessor :description

    attr_accessor :duration

    attr_accessor :is_local

    attr_accessor :name

    attr_accessor :thumbnail_path

    attr_accessor :preview_path

    attr_accessor :embed_path

    attr_accessor :views

    attr_accessor :likes

    attr_accessor :dislikes

    attr_accessor :nsfw

    attr_accessor :wait_transcoding

    attr_accessor :state

    attr_accessor :scheduled_update

    attr_accessor :blacklisted

    attr_accessor :blacklisted_reason

    attr_accessor :account

    attr_accessor :channel

    attr_accessor :user_history

    attr_accessor :description_path

    # A text tell the audience how to support the video creator
    attr_accessor :support

    attr_accessor :tags

    attr_accessor :files

    attr_accessor :comments_enabled

    attr_accessor :download_enabled

    attr_accessor :tracker_urls

    attr_accessor :streaming_playlists

    # Attribute mapping from ruby-style variable name to JSON key.
    def self.attribute_map
      {
        :'id' => :'id',
        :'uuid' => :'uuid',
        :'created_at' => :'createdAt',
        :'published_at' => :'publishedAt',
        :'updated_at' => :'updatedAt',
        :'originally_published_at' => :'originallyPublishedAt',
        :'category' => :'category',
        :'licence' => :'licence',
        :'language' => :'language',
        :'privacy' => :'privacy',
        :'description' => :'description',
        :'duration' => :'duration',
        :'is_local' => :'isLocal',
        :'name' => :'name',
        :'thumbnail_path' => :'thumbnailPath',
        :'preview_path' => :'previewPath',
        :'embed_path' => :'embedPath',
        :'views' => :'views',
        :'likes' => :'likes',
        :'dislikes' => :'dislikes',
        :'nsfw' => :'nsfw',
        :'wait_transcoding' => :'waitTranscoding',
        :'state' => :'state',
        :'scheduled_update' => :'scheduledUpdate',
        :'blacklisted' => :'blacklisted',
        :'blacklisted_reason' => :'blacklistedReason',
        :'account' => :'account',
        :'channel' => :'channel',
        :'user_history' => :'userHistory',
        :'description_path' => :'descriptionPath',
        :'support' => :'support',
        :'tags' => :'tags',
        :'files' => :'files',
        :'comments_enabled' => :'commentsEnabled',
        :'download_enabled' => :'downloadEnabled',
        :'tracker_urls' => :'trackerUrls',
        :'streaming_playlists' => :'streamingPlaylists'
      }
    end

    # Attribute type mapping.
    def self.openapi_types
      {
        :'id' => :'Integer',
        :'uuid' => :'String',
        :'created_at' => :'DateTime',
        :'published_at' => :'DateTime',
        :'updated_at' => :'DateTime',
        :'originally_published_at' => :'DateTime',
        :'category' => :'VideoConstantNumber',
        :'licence' => :'VideoConstantNumber',
        :'language' => :'VideoConstantString',
        :'privacy' => :'VideoPrivacyConstant',
        :'description' => :'String',
        :'duration' => :'Integer',
        :'is_local' => :'Boolean',
        :'name' => :'String',
        :'thumbnail_path' => :'String',
        :'preview_path' => :'String',
        :'embed_path' => :'String',
        :'views' => :'Integer',
        :'likes' => :'Integer',
        :'dislikes' => :'Integer',
        :'nsfw' => :'Boolean',
        :'wait_transcoding' => :'Boolean',
        :'state' => :'VideoStateConstant',
        :'scheduled_update' => :'VideoScheduledUpdate',
        :'blacklisted' => :'Boolean',
        :'blacklisted_reason' => :'String',
        :'account' => :'Account',
        :'channel' => :'VideoChannel',
        :'user_history' => :'VideoUserHistory',
        :'description_path' => :'String',
        :'support' => :'String',
        :'tags' => :'Array<String>',
        :'files' => :'Array<VideoFile>',
        :'comments_enabled' => :'Boolean',
        :'download_enabled' => :'Boolean',
        :'tracker_urls' => :'Array<String>',
        :'streaming_playlists' => :'Array<VideoStreamingPlaylists>'
      }
    end

    # List of attributes with nullable: true
    def self.openapi_nullable
      Set.new([
        :'wait_transcoding',
        :'scheduled_update',
        :'blacklisted',
        :'blacklisted_reason',
        :'user_history',
      ])
    end

    # List of class defined in allOf (OpenAPI v3)
    def self.openapi_all_of
      [
      :'Video',
      :'VideoDetailsAllOf'
      ]
    end

    # Initializes the object
    # @param [Hash] attributes Model attributes in the form of hash
    def initialize(attributes = {})
      if (!attributes.is_a?(Hash))
        fail ArgumentError, "The input argument (attributes) must be a hash in `Peertube::VideoDetails` initialize method"
      end

      # check to see if the attribute exists and convert string to symbol for hash key
      attributes = attributes.each_with_object({}) { |(k, v), h|
        if (!self.class.attribute_map.key?(k.to_sym))
          fail ArgumentError, "`#{k}` is not a valid attribute in `Peertube::VideoDetails`. Please check the name to make sure it's valid. List of attributes: " + self.class.attribute_map.keys.inspect
        end
        h[k.to_sym] = v
      }

      if attributes.key?(:'id')
        self.id = attributes[:'id']
      end

      if attributes.key?(:'uuid')
        self.uuid = attributes[:'uuid']
      end

      if attributes.key?(:'created_at')
        self.created_at = attributes[:'created_at']
      end

      if attributes.key?(:'published_at')
        self.published_at = attributes[:'published_at']
      end

      if attributes.key?(:'updated_at')
        self.updated_at = attributes[:'updated_at']
      end

      if attributes.key?(:'originally_published_at')
        self.originally_published_at = attributes[:'originally_published_at']
      end

      if attributes.key?(:'category')
        self.category = attributes[:'category']
      end

      if attributes.key?(:'licence')
        self.licence = attributes[:'licence']
      end

      if attributes.key?(:'language')
        self.language = attributes[:'language']
      end

      if attributes.key?(:'privacy')
        self.privacy = attributes[:'privacy']
      end

      if attributes.key?(:'description')
        self.description = attributes[:'description']
      end

      if attributes.key?(:'duration')
        self.duration = attributes[:'duration']
      end

      if attributes.key?(:'is_local')
        self.is_local = attributes[:'is_local']
      end

      if attributes.key?(:'name')
        self.name = attributes[:'name']
      end

      if attributes.key?(:'thumbnail_path')
        self.thumbnail_path = attributes[:'thumbnail_path']
      end

      if attributes.key?(:'preview_path')
        self.preview_path = attributes[:'preview_path']
      end

      if attributes.key?(:'embed_path')
        self.embed_path = attributes[:'embed_path']
      end

      if attributes.key?(:'views')
        self.views = attributes[:'views']
      end

      if attributes.key?(:'likes')
        self.likes = attributes[:'likes']
      end

      if attributes.key?(:'dislikes')
        self.dislikes = attributes[:'dislikes']
      end

      if attributes.key?(:'nsfw')
        self.nsfw = attributes[:'nsfw']
      end

      if attributes.key?(:'wait_transcoding')
        self.wait_transcoding = attributes[:'wait_transcoding']
      end

      if attributes.key?(:'state')
        self.state = attributes[:'state']
      end

      if attributes.key?(:'scheduled_update')
        self.scheduled_update = attributes[:'scheduled_update']
      end

      if attributes.key?(:'blacklisted')
        self.blacklisted = attributes[:'blacklisted']
      end

      if attributes.key?(:'blacklisted_reason')
        self.blacklisted_reason = attributes[:'blacklisted_reason']
      end

      if attributes.key?(:'account')
        self.account = attributes[:'account']
      end

      if attributes.key?(:'channel')
        self.channel = attributes[:'channel']
      end

      if attributes.key?(:'user_history')
        self.user_history = attributes[:'user_history']
      end

      if attributes.key?(:'description_path')
        self.description_path = attributes[:'description_path']
      end

      if attributes.key?(:'support')
        self.support = attributes[:'support']
      end

      if attributes.key?(:'tags')
        if (value = attributes[:'tags']).is_a?(Array)
          self.tags = value
        end
      end

      if attributes.key?(:'files')
        if (value = attributes[:'files']).is_a?(Array)
          self.files = value
        end
      end

      if attributes.key?(:'comments_enabled')
        self.comments_enabled = attributes[:'comments_enabled']
      end

      if attributes.key?(:'download_enabled')
        self.download_enabled = attributes[:'download_enabled']
      end

      if attributes.key?(:'tracker_urls')
        if (value = attributes[:'tracker_urls']).is_a?(Array)
          self.tracker_urls = value
        end
      end

      if attributes.key?(:'streaming_playlists')
        if (value = attributes[:'streaming_playlists']).is_a?(Array)
          self.streaming_playlists = value
        end
      end
    end

    # Show invalid properties with the reasons. Usually used together with valid?
    # @return Array for valid properties with the reasons
    def list_invalid_properties
      invalid_properties = Array.new
      invalid_properties
    end

    # Check to see if the all the properties in the model are valid
    # @return true if the model is valid
    def valid?
      true
    end

    # Checks equality by comparing each attribute.
    # @param [Object] Object to be compared
    def ==(o)
      return true if self.equal?(o)
      self.class == o.class &&
          id == o.id &&
          uuid == o.uuid &&
          created_at == o.created_at &&
          published_at == o.published_at &&
          updated_at == o.updated_at &&
          originally_published_at == o.originally_published_at &&
          category == o.category &&
          licence == o.licence &&
          language == o.language &&
          privacy == o.privacy &&
          description == o.description &&
          duration == o.duration &&
          is_local == o.is_local &&
          name == o.name &&
          thumbnail_path == o.thumbnail_path &&
          preview_path == o.preview_path &&
          embed_path == o.embed_path &&
          views == o.views &&
          likes == o.likes &&
          dislikes == o.dislikes &&
          nsfw == o.nsfw &&
          wait_transcoding == o.wait_transcoding &&
          state == o.state &&
          scheduled_update == o.scheduled_update &&
          blacklisted == o.blacklisted &&
          blacklisted_reason == o.blacklisted_reason &&
          account == o.account &&
          channel == o.channel &&
          user_history == o.user_history &&
          description_path == o.description_path &&
          support == o.support &&
          tags == o.tags &&
          files == o.files &&
          comments_enabled == o.comments_enabled &&
          download_enabled == o.download_enabled &&
          tracker_urls == o.tracker_urls &&
          streaming_playlists == o.streaming_playlists
    end

    # @see the `==` method
    # @param [Object] Object to be compared
    def eql?(o)
      self == o
    end

    # Calculates hash code according to all attributes.
    # @return [Integer] Hash code
    def hash
      [id, uuid, created_at, published_at, updated_at, originally_published_at, category, licence, language, privacy, description, duration, is_local, name, thumbnail_path, preview_path, embed_path, views, likes, dislikes, nsfw, wait_transcoding, state, scheduled_update, blacklisted, blacklisted_reason, account, channel, user_history, description_path, support, tags, files, comments_enabled, download_enabled, tracker_urls, streaming_playlists].hash
    end

    # Builds the object from hash
    # @param [Hash] attributes Model attributes in the form of hash
    # @return [Object] Returns the model itself
    def self.build_from_hash(attributes)
      new.build_from_hash(attributes)
    end

    # Builds the object from hash
    # @param [Hash] attributes Model attributes in the form of hash
    # @return [Object] Returns the model itself
    def build_from_hash(attributes)
      return nil unless attributes.is_a?(Hash)
      self.class.openapi_types.each_pair do |key, type|
        if type =~ /\AArray<(.*)>/i
          # check to ensure the input is an array given that the attribute
          # is documented as an array but the input is not
          if attributes[self.class.attribute_map[key]].is_a?(Array)
            self.send("#{key}=", attributes[self.class.attribute_map[key]].map { |v| _deserialize($1, v) })
          end
        elsif !attributes[self.class.attribute_map[key]].nil?
          self.send("#{key}=", _deserialize(type, attributes[self.class.attribute_map[key]]))
        end # or else data not found in attributes(hash), not an issue as the data can be optional
      end

      self
    end

    # Deserializes the data based on type
    # @param string type Data type
    # @param string value Value to be deserialized
    # @return [Object] Deserialized data
    def _deserialize(type, value)
      case type.to_sym
      when :DateTime
        DateTime.parse(value)
      when :Date
        Date.parse(value)
      when :String
        value.to_s
      when :Integer
        value.to_i
      when :Float
        value.to_f
      when :Boolean
        if value.to_s =~ /\A(true|t|yes|y|1)\z/i
          true
        else
          false
        end
      when :Object
        # generic object (usually a Hash), return directly
        value
      when /\AArray<(?<inner_type>.+)>\z/
        inner_type = Regexp.last_match[:inner_type]
        value.map { |v| _deserialize(inner_type, v) }
      when /\AHash<(?<k_type>.+?), (?<v_type>.+)>\z/
        k_type = Regexp.last_match[:k_type]
        v_type = Regexp.last_match[:v_type]
        {}.tap do |hash|
          value.each do |k, v|
            hash[_deserialize(k_type, k)] = _deserialize(v_type, v)
          end
        end
      else # model
        Peertube.const_get(type).build_from_hash(value)
      end
    end

    # Returns the string representation of the object
    # @return [String] String presentation of the object
    def to_s
      to_hash.to_s
    end

    # to_body is an alias to to_hash (backward compatibility)
    # @return [Hash] Returns the object in the form of hash
    def to_body
      to_hash
    end

    # Returns the object in the form of hash
    # @return [Hash] Returns the object in the form of hash
    def to_hash
      hash = {}
      self.class.attribute_map.each_pair do |attr, param|
        value = self.send(attr)
        if value.nil?
          is_nullable = self.class.openapi_nullable.include?(attr)
          next if !is_nullable || (is_nullable && !instance_variable_defined?(:"@#{attr}"))
        end
        
        hash[param] = _to_hash(value)
      end
      hash
    end

    # Outputs non-array value in the form of hash
    # For object, use to_hash. Otherwise, just return the value
    # @param [Object] value Any valid value
    # @return [Hash] Returns the value in the form of hash
    def _to_hash(value)
      if value.is_a?(Array)
        value.compact.map { |v| _to_hash(v) }
      elsif value.is_a?(Hash)
        {}.tap do |hash|
          value.each { |k, v| hash[k] = _to_hash(v) }
        end
      elsif value.respond_to? :to_hash
        value.to_hash
      else
        value
      end
    end
  end
end
