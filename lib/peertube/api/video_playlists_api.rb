=begin
#PeerTube

## Introduction  The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [Quick Start guide](https://docs.joinpeertube.org/#/api-rest-getting-started) so you can play with the PeerTube API.  # Authentication  When you sign up for an account, you are given the possibility to generate sessions, and authenticate using this session token. One session token can currently be used at a time.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/#/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call. The body of the response will be JSON in the following format.  ``` {   \"code\": \"unauthorized_request\", // example inner error code   \"error\": \"Token is invalid.\" // example exposed error message } ``` 

The version of the OpenAPI document: 2.3.0

Generated by: https://openapi-generator.tech
OpenAPI Generator version: 4.3.1

=end

require 'cgi'

module Peertube
  class VideoPlaylistsApi
    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end
    # Check video exists in my playlists
    # @param video_ids [Array<Integer>] The video ids to check
    # @param [Hash] opts the optional parameters
    # @return [InlineResponse2007]
    def users_me_video_playlists_videos_exist_get(video_ids, opts = {})
      data, _status_code, _headers = users_me_video_playlists_videos_exist_get_with_http_info(video_ids, opts)
      data
    end

    # Check video exists in my playlists
    # @param video_ids [Array<Integer>] The video ids to check
    # @param [Hash] opts the optional parameters
    # @return [Array<(InlineResponse2007, Integer, Hash)>] InlineResponse2007 data, response status code and response headers
    def users_me_video_playlists_videos_exist_get_with_http_info(video_ids, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: VideoPlaylistsApi.users_me_video_playlists_videos_exist_get ...'
      end
      # verify the required parameter 'video_ids' is set
      if @api_client.config.client_side_validation && video_ids.nil?
        fail ArgumentError, "Missing the required parameter 'video_ids' when calling VideoPlaylistsApi.users_me_video_playlists_videos_exist_get"
      end
      # resource path
      local_var_path = '/users/me/video-playlists/videos-exist'

      # query parameters
      query_params = opts[:query_params] || {}
      query_params[:'videoIds'] = @api_client.build_collection_param(video_ids, :multi)

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      # return_type
      return_type = opts[:return_type] || 'InlineResponse2007' 

      # auth_names
      auth_names = opts[:auth_names] || ['OAuth2']

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:GET, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: VideoPlaylistsApi#users_me_video_playlists_videos_exist_get\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # List video playlists
    # @param [Hash] opts the optional parameters
    # @option opts [Integer] :start Offset used to paginate results
    # @option opts [Integer] :count Number of items to return (default to 15)
    # @option opts [String] :sort Sort column
    # @return [InlineResponse2004]
    def video_playlists_get(opts = {})
      data, _status_code, _headers = video_playlists_get_with_http_info(opts)
      data
    end

    # List video playlists
    # @param [Hash] opts the optional parameters
    # @option opts [Integer] :start Offset used to paginate results
    # @option opts [Integer] :count Number of items to return
    # @option opts [String] :sort Sort column
    # @return [Array<(InlineResponse2004, Integer, Hash)>] InlineResponse2004 data, response status code and response headers
    def video_playlists_get_with_http_info(opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: VideoPlaylistsApi.video_playlists_get ...'
      end
      if @api_client.config.client_side_validation && !opts[:'start'].nil? && opts[:'start'] < 0
        fail ArgumentError, 'invalid value for "opts[:"start"]" when calling VideoPlaylistsApi.video_playlists_get, must be greater than or equal to 0.'
      end

      if @api_client.config.client_side_validation && !opts[:'count'].nil? && opts[:'count'] > 100
        fail ArgumentError, 'invalid value for "opts[:"count"]" when calling VideoPlaylistsApi.video_playlists_get, must be smaller than or equal to 100.'
      end

      if @api_client.config.client_side_validation && !opts[:'count'].nil? && opts[:'count'] < 1
        fail ArgumentError, 'invalid value for "opts[:"count"]" when calling VideoPlaylistsApi.video_playlists_get, must be greater than or equal to 1.'
      end

      # resource path
      local_var_path = '/video-playlists'

      # query parameters
      query_params = opts[:query_params] || {}
      query_params[:'start'] = opts[:'start'] if !opts[:'start'].nil?
      query_params[:'count'] = opts[:'count'] if !opts[:'count'].nil?
      query_params[:'sort'] = opts[:'sort'] if !opts[:'sort'].nil?

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      # return_type
      return_type = opts[:return_type] || 'InlineResponse2004' 

      # auth_names
      auth_names = opts[:auth_names] || []

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:GET, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: VideoPlaylistsApi#video_playlists_get\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # Delete a video playlist
    # @param id [OneOfintegerUUID] The object id or uuid
    # @param [Hash] opts the optional parameters
    # @return [nil]
    def video_playlists_id_delete(id, opts = {})
      video_playlists_id_delete_with_http_info(id, opts)
      nil
    end

    # Delete a video playlist
    # @param id [OneOfintegerUUID] The object id or uuid
    # @param [Hash] opts the optional parameters
    # @return [Array<(nil, Integer, Hash)>] nil, response status code and response headers
    def video_playlists_id_delete_with_http_info(id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: VideoPlaylistsApi.video_playlists_id_delete ...'
      end
      # verify the required parameter 'id' is set
      if @api_client.config.client_side_validation && id.nil?
        fail ArgumentError, "Missing the required parameter 'id' when calling VideoPlaylistsApi.video_playlists_id_delete"
      end
      # resource path
      local_var_path = '/video-playlists/{id}'.sub('{' + 'id' + '}', CGI.escape(id.to_s))

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      # return_type
      return_type = opts[:return_type] 

      # auth_names
      auth_names = opts[:auth_names] || ['OAuth2']

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:DELETE, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: VideoPlaylistsApi#video_playlists_id_delete\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # Get a video playlist
    # @param id [OneOfintegerUUID] The object id or uuid
    # @param [Hash] opts the optional parameters
    # @return [VideoPlaylist]
    def video_playlists_id_get(id, opts = {})
      data, _status_code, _headers = video_playlists_id_get_with_http_info(id, opts)
      data
    end

    # Get a video playlist
    # @param id [OneOfintegerUUID] The object id or uuid
    # @param [Hash] opts the optional parameters
    # @return [Array<(VideoPlaylist, Integer, Hash)>] VideoPlaylist data, response status code and response headers
    def video_playlists_id_get_with_http_info(id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: VideoPlaylistsApi.video_playlists_id_get ...'
      end
      # verify the required parameter 'id' is set
      if @api_client.config.client_side_validation && id.nil?
        fail ArgumentError, "Missing the required parameter 'id' when calling VideoPlaylistsApi.video_playlists_id_get"
      end
      # resource path
      local_var_path = '/video-playlists/{id}'.sub('{' + 'id' + '}', CGI.escape(id.to_s))

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      # return_type
      return_type = opts[:return_type] || 'VideoPlaylist' 

      # auth_names
      auth_names = opts[:auth_names] || []

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:GET, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: VideoPlaylistsApi#video_playlists_id_get\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # Update a video playlist
    # If the video playlist is set as public, the playlist must have a assigned channel.
    # @param id [OneOfintegerUUID] The object id or uuid
    # @param [Hash] opts the optional parameters
    # @option opts [String] :display_name Video playlist display name
    # @option opts [File] :thumbnailfile Video playlist thumbnail file
    # @option opts [VideoPlaylistPrivacySet] :privacy 
    # @option opts [String] :description Video playlist description
    # @option opts [Integer] :video_channel_id Video channel in which the playlist will be published
    # @return [nil]
    def video_playlists_id_put(id, opts = {})
      video_playlists_id_put_with_http_info(id, opts)
      nil
    end

    # Update a video playlist
    # If the video playlist is set as public, the playlist must have a assigned channel.
    # @param id [OneOfintegerUUID] The object id or uuid
    # @param [Hash] opts the optional parameters
    # @option opts [String] :display_name Video playlist display name
    # @option opts [File] :thumbnailfile Video playlist thumbnail file
    # @option opts [VideoPlaylistPrivacySet] :privacy 
    # @option opts [String] :description Video playlist description
    # @option opts [Integer] :video_channel_id Video channel in which the playlist will be published
    # @return [Array<(nil, Integer, Hash)>] nil, response status code and response headers
    def video_playlists_id_put_with_http_info(id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: VideoPlaylistsApi.video_playlists_id_put ...'
      end
      # verify the required parameter 'id' is set
      if @api_client.config.client_side_validation && id.nil?
        fail ArgumentError, "Missing the required parameter 'id' when calling VideoPlaylistsApi.video_playlists_id_put"
      end
      # resource path
      local_var_path = '/video-playlists/{id}'.sub('{' + 'id' + '}', CGI.escape(id.to_s))

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['multipart/form-data'])

      # form parameters
      form_params = opts[:form_params] || {}
      form_params['displayName'] = opts[:'display_name'] if !opts[:'display_name'].nil?
      form_params['thumbnailfile'] = opts[:'thumbnailfile'] if !opts[:'thumbnailfile'].nil?
      form_params['privacy'] = opts[:'privacy'] if !opts[:'privacy'].nil?
      form_params['description'] = opts[:'description'] if !opts[:'description'].nil?
      form_params['videoChannelId'] = opts[:'video_channel_id'] if !opts[:'video_channel_id'].nil?

      # http body (model)
      post_body = opts[:body] 

      # return_type
      return_type = opts[:return_type] 

      # auth_names
      auth_names = opts[:auth_names] || ['OAuth2']

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:PUT, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: VideoPlaylistsApi#video_playlists_id_put\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # List videos of a playlist
    # @param id [OneOfintegerUUID] The object id or uuid
    # @param [Hash] opts the optional parameters
    # @return [VideoListResponse]
    def video_playlists_id_videos_get(id, opts = {})
      data, _status_code, _headers = video_playlists_id_videos_get_with_http_info(id, opts)
      data
    end

    # List videos of a playlist
    # @param id [OneOfintegerUUID] The object id or uuid
    # @param [Hash] opts the optional parameters
    # @return [Array<(VideoListResponse, Integer, Hash)>] VideoListResponse data, response status code and response headers
    def video_playlists_id_videos_get_with_http_info(id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: VideoPlaylistsApi.video_playlists_id_videos_get ...'
      end
      # verify the required parameter 'id' is set
      if @api_client.config.client_side_validation && id.nil?
        fail ArgumentError, "Missing the required parameter 'id' when calling VideoPlaylistsApi.video_playlists_id_videos_get"
      end
      # resource path
      local_var_path = '/video-playlists/{id}/videos'.sub('{' + 'id' + '}', CGI.escape(id.to_s))

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      # return_type
      return_type = opts[:return_type] || 'VideoListResponse' 

      # auth_names
      auth_names = opts[:auth_names] || []

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:GET, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: VideoPlaylistsApi#video_playlists_id_videos_get\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # Delete an element from a playlist
    # @param id [OneOfintegerUUID] The object id or uuid
    # @param playlist_element_id [Integer] Playlist element id
    # @param [Hash] opts the optional parameters
    # @return [nil]
    def video_playlists_id_videos_playlist_element_id_delete(id, playlist_element_id, opts = {})
      video_playlists_id_videos_playlist_element_id_delete_with_http_info(id, playlist_element_id, opts)
      nil
    end

    # Delete an element from a playlist
    # @param id [OneOfintegerUUID] The object id or uuid
    # @param playlist_element_id [Integer] Playlist element id
    # @param [Hash] opts the optional parameters
    # @return [Array<(nil, Integer, Hash)>] nil, response status code and response headers
    def video_playlists_id_videos_playlist_element_id_delete_with_http_info(id, playlist_element_id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: VideoPlaylistsApi.video_playlists_id_videos_playlist_element_id_delete ...'
      end
      # verify the required parameter 'id' is set
      if @api_client.config.client_side_validation && id.nil?
        fail ArgumentError, "Missing the required parameter 'id' when calling VideoPlaylistsApi.video_playlists_id_videos_playlist_element_id_delete"
      end
      # verify the required parameter 'playlist_element_id' is set
      if @api_client.config.client_side_validation && playlist_element_id.nil?
        fail ArgumentError, "Missing the required parameter 'playlist_element_id' when calling VideoPlaylistsApi.video_playlists_id_videos_playlist_element_id_delete"
      end
      # resource path
      local_var_path = '/video-playlists/{id}/videos/{playlistElementId}'.sub('{' + 'id' + '}', CGI.escape(id.to_s)).sub('{' + 'playlistElementId' + '}', CGI.escape(playlist_element_id.to_s))

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      # return_type
      return_type = opts[:return_type] 

      # auth_names
      auth_names = opts[:auth_names] || ['OAuth2']

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:DELETE, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: VideoPlaylistsApi#video_playlists_id_videos_playlist_element_id_delete\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # Update a playlist element
    # @param id [OneOfintegerUUID] The object id or uuid
    # @param playlist_element_id [Integer] Playlist element id
    # @param [Hash] opts the optional parameters
    # @option opts [InlineObject16] :inline_object16 
    # @return [nil]
    def video_playlists_id_videos_playlist_element_id_put(id, playlist_element_id, opts = {})
      video_playlists_id_videos_playlist_element_id_put_with_http_info(id, playlist_element_id, opts)
      nil
    end

    # Update a playlist element
    # @param id [OneOfintegerUUID] The object id or uuid
    # @param playlist_element_id [Integer] Playlist element id
    # @param [Hash] opts the optional parameters
    # @option opts [InlineObject16] :inline_object16 
    # @return [Array<(nil, Integer, Hash)>] nil, response status code and response headers
    def video_playlists_id_videos_playlist_element_id_put_with_http_info(id, playlist_element_id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: VideoPlaylistsApi.video_playlists_id_videos_playlist_element_id_put ...'
      end
      # verify the required parameter 'id' is set
      if @api_client.config.client_side_validation && id.nil?
        fail ArgumentError, "Missing the required parameter 'id' when calling VideoPlaylistsApi.video_playlists_id_videos_playlist_element_id_put"
      end
      # verify the required parameter 'playlist_element_id' is set
      if @api_client.config.client_side_validation && playlist_element_id.nil?
        fail ArgumentError, "Missing the required parameter 'playlist_element_id' when calling VideoPlaylistsApi.video_playlists_id_videos_playlist_element_id_put"
      end
      # resource path
      local_var_path = '/video-playlists/{id}/videos/{playlistElementId}'.sub('{' + 'id' + '}', CGI.escape(id.to_s)).sub('{' + 'playlistElementId' + '}', CGI.escape(playlist_element_id.to_s))

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] || @api_client.object_to_http_body(opts[:'inline_object16']) 

      # return_type
      return_type = opts[:return_type] 

      # auth_names
      auth_names = opts[:auth_names] || ['OAuth2']

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:PUT, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: VideoPlaylistsApi#video_playlists_id_videos_playlist_element_id_put\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # Add a video in a playlist
    # @param id [OneOfintegerUUID] The object id or uuid
    # @param [Hash] opts the optional parameters
    # @option opts [InlineObject14] :inline_object14 
    # @return [InlineResponse2006]
    def video_playlists_id_videos_post(id, opts = {})
      data, _status_code, _headers = video_playlists_id_videos_post_with_http_info(id, opts)
      data
    end

    # Add a video in a playlist
    # @param id [OneOfintegerUUID] The object id or uuid
    # @param [Hash] opts the optional parameters
    # @option opts [InlineObject14] :inline_object14 
    # @return [Array<(InlineResponse2006, Integer, Hash)>] InlineResponse2006 data, response status code and response headers
    def video_playlists_id_videos_post_with_http_info(id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: VideoPlaylistsApi.video_playlists_id_videos_post ...'
      end
      # verify the required parameter 'id' is set
      if @api_client.config.client_side_validation && id.nil?
        fail ArgumentError, "Missing the required parameter 'id' when calling VideoPlaylistsApi.video_playlists_id_videos_post"
      end
      # resource path
      local_var_path = '/video-playlists/{id}/videos'.sub('{' + 'id' + '}', CGI.escape(id.to_s))

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] || @api_client.object_to_http_body(opts[:'inline_object14']) 

      # return_type
      return_type = opts[:return_type] || 'InlineResponse2006' 

      # auth_names
      auth_names = opts[:auth_names] || ['OAuth2']

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:POST, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: VideoPlaylistsApi#video_playlists_id_videos_post\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # Reorder a playlist
    # @param id [OneOfintegerUUID] The object id or uuid
    # @param [Hash] opts the optional parameters
    # @option opts [InlineObject15] :inline_object15 
    # @return [nil]
    def video_playlists_id_videos_reorder_post(id, opts = {})
      video_playlists_id_videos_reorder_post_with_http_info(id, opts)
      nil
    end

    # Reorder a playlist
    # @param id [OneOfintegerUUID] The object id or uuid
    # @param [Hash] opts the optional parameters
    # @option opts [InlineObject15] :inline_object15 
    # @return [Array<(nil, Integer, Hash)>] nil, response status code and response headers
    def video_playlists_id_videos_reorder_post_with_http_info(id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: VideoPlaylistsApi.video_playlists_id_videos_reorder_post ...'
      end
      # verify the required parameter 'id' is set
      if @api_client.config.client_side_validation && id.nil?
        fail ArgumentError, "Missing the required parameter 'id' when calling VideoPlaylistsApi.video_playlists_id_videos_reorder_post"
      end
      # resource path
      local_var_path = '/video-playlists/{id}/videos/reorder'.sub('{' + 'id' + '}', CGI.escape(id.to_s))

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] || @api_client.object_to_http_body(opts[:'inline_object15']) 

      # return_type
      return_type = opts[:return_type] 

      # auth_names
      auth_names = opts[:auth_names] || ['OAuth2']

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:POST, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: VideoPlaylistsApi#video_playlists_id_videos_reorder_post\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # Create a video playlist
    # If the video playlist is set as public, the videoChannelId is mandatory.
    # @param display_name [String] Video playlist display name
    # @param [Hash] opts the optional parameters
    # @option opts [File] :thumbnailfile Video playlist thumbnail file
    # @option opts [VideoPlaylistPrivacySet] :privacy 
    # @option opts [String] :description Video playlist description
    # @option opts [Integer] :video_channel_id Video channel in which the playlist will be published
    # @return [InlineResponse2005]
    def video_playlists_post(display_name, opts = {})
      data, _status_code, _headers = video_playlists_post_with_http_info(display_name, opts)
      data
    end

    # Create a video playlist
    # If the video playlist is set as public, the videoChannelId is mandatory.
    # @param display_name [String] Video playlist display name
    # @param [Hash] opts the optional parameters
    # @option opts [File] :thumbnailfile Video playlist thumbnail file
    # @option opts [VideoPlaylistPrivacySet] :privacy 
    # @option opts [String] :description Video playlist description
    # @option opts [Integer] :video_channel_id Video channel in which the playlist will be published
    # @return [Array<(InlineResponse2005, Integer, Hash)>] InlineResponse2005 data, response status code and response headers
    def video_playlists_post_with_http_info(display_name, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: VideoPlaylistsApi.video_playlists_post ...'
      end
      # verify the required parameter 'display_name' is set
      if @api_client.config.client_side_validation && display_name.nil?
        fail ArgumentError, "Missing the required parameter 'display_name' when calling VideoPlaylistsApi.video_playlists_post"
      end
      # resource path
      local_var_path = '/video-playlists'

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['multipart/form-data'])

      # form parameters
      form_params = opts[:form_params] || {}
      form_params['displayName'] = display_name
      form_params['thumbnailfile'] = opts[:'thumbnailfile'] if !opts[:'thumbnailfile'].nil?
      form_params['privacy'] = opts[:'privacy'] if !opts[:'privacy'].nil?
      form_params['description'] = opts[:'description'] if !opts[:'description'].nil?
      form_params['videoChannelId'] = opts[:'video_channel_id'] if !opts[:'video_channel_id'].nil?

      # http body (model)
      post_body = opts[:body] 

      # return_type
      return_type = opts[:return_type] || 'InlineResponse2005' 

      # auth_names
      auth_names = opts[:auth_names] || ['OAuth2']

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:POST, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: VideoPlaylistsApi#video_playlists_post\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # List available playlist privacies
    # @param [Hash] opts the optional parameters
    # @return [Array<String>]
    def video_playlists_privacies_get(opts = {})
      data, _status_code, _headers = video_playlists_privacies_get_with_http_info(opts)
      data
    end

    # List available playlist privacies
    # @param [Hash] opts the optional parameters
    # @return [Array<(Array<String>, Integer, Hash)>] Array<String> data, response status code and response headers
    def video_playlists_privacies_get_with_http_info(opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: VideoPlaylistsApi.video_playlists_privacies_get ...'
      end
      # resource path
      local_var_path = '/video-playlists/privacies'

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      # return_type
      return_type = opts[:return_type] || 'Array<String>' 

      # auth_names
      auth_names = opts[:auth_names] || []

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:GET, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: VideoPlaylistsApi#video_playlists_privacies_get\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
