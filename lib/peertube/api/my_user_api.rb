=begin
#PeerTube

## Introduction  The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [Quick Start guide](https://docs.joinpeertube.org/#/api-rest-getting-started) so you can play with the PeerTube API.  # Authentication  When you sign up for an account, you are given the possibility to generate sessions, and authenticate using this session token. One session token can currently be used at a time.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/#/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call. The body of the response will be JSON in the following format.  ``` {   \"code\": \"unauthorized_request\", // example inner error code   \"error\": \"Token is invalid.\" // example exposed error message } ``` 

The version of the OpenAPI document: 2.3.0

Generated by: https://openapi-generator.tech
OpenAPI Generator version: 4.3.1

=end

require 'cgi'

module Peertube
  class MyUserApi
    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end
    # Update my user avatar
    # @param [Hash] opts the optional parameters
    # @option opts [File] :avatarfile The file to upload.
    # @return [Avatar]
    def users_me_avatar_pick_post(opts = {})
      data, _status_code, _headers = users_me_avatar_pick_post_with_http_info(opts)
      data
    end

    # Update my user avatar
    # @param [Hash] opts the optional parameters
    # @option opts [File] :avatarfile The file to upload.
    # @return [Array<(Avatar, Integer, Hash)>] Avatar data, response status code and response headers
    def users_me_avatar_pick_post_with_http_info(opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: MyUserApi.users_me_avatar_pick_post ...'
      end
      # resource path
      local_var_path = '/users/me/avatar/pick'

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['multipart/form-data'])

      # form parameters
      form_params = opts[:form_params] || {}
      form_params['avatarfile'] = opts[:'avatarfile'] if !opts[:'avatarfile'].nil?

      # http body (model)
      post_body = opts[:body] 

      # return_type
      return_type = opts[:return_type] || 'Avatar' 

      # auth_names
      auth_names = opts[:auth_names] || ['OAuth2']

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:POST, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: MyUserApi#users_me_avatar_pick_post\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # Get my user information
    # @param [Hash] opts the optional parameters
    # @return [Array<User>]
    def users_me_get(opts = {})
      data, _status_code, _headers = users_me_get_with_http_info(opts)
      data
    end

    # Get my user information
    # @param [Hash] opts the optional parameters
    # @return [Array<(Array<User>, Integer, Hash)>] Array<User> data, response status code and response headers
    def users_me_get_with_http_info(opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: MyUserApi.users_me_get ...'
      end
      # resource path
      local_var_path = '/users/me'

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      # return_type
      return_type = opts[:return_type] || 'Array<User>' 

      # auth_names
      auth_names = opts[:auth_names] || ['OAuth2']

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:GET, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: MyUserApi#users_me_get\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # Update my user information
    # @param update_me [UpdateMe] 
    # @param [Hash] opts the optional parameters
    # @return [nil]
    def users_me_put(update_me, opts = {})
      users_me_put_with_http_info(update_me, opts)
      nil
    end

    # Update my user information
    # @param update_me [UpdateMe] 
    # @param [Hash] opts the optional parameters
    # @return [Array<(nil, Integer, Hash)>] nil, response status code and response headers
    def users_me_put_with_http_info(update_me, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: MyUserApi.users_me_put ...'
      end
      # verify the required parameter 'update_me' is set
      if @api_client.config.client_side_validation && update_me.nil?
        fail ArgumentError, "Missing the required parameter 'update_me' when calling MyUserApi.users_me_put"
      end
      # resource path
      local_var_path = '/users/me'

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] || @api_client.object_to_http_body(update_me) 

      # return_type
      return_type = opts[:return_type] 

      # auth_names
      auth_names = opts[:auth_names] || ['OAuth2']

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:PUT, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: MyUserApi#users_me_put\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # Get my user used quota
    # @param [Hash] opts the optional parameters
    # @return [Float]
    def users_me_video_quota_used_get(opts = {})
      data, _status_code, _headers = users_me_video_quota_used_get_with_http_info(opts)
      data
    end

    # Get my user used quota
    # @param [Hash] opts the optional parameters
    # @return [Array<(Float, Integer, Hash)>] Float data, response status code and response headers
    def users_me_video_quota_used_get_with_http_info(opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: MyUserApi.users_me_video_quota_used_get ...'
      end
      # resource path
      local_var_path = '/users/me/video-quota-used'

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      # return_type
      return_type = opts[:return_type] || 'Float' 

      # auth_names
      auth_names = opts[:auth_names] || ['OAuth2']

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:GET, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: MyUserApi#users_me_video_quota_used_get\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # Get videos of my user
    # @param [Hash] opts the optional parameters
    # @option opts [Integer] :start Offset used to paginate results
    # @option opts [Integer] :count Number of items to return (default to 15)
    # @option opts [String] :sort Sort column
    # @return [VideoListResponse]
    def users_me_videos_get(opts = {})
      data, _status_code, _headers = users_me_videos_get_with_http_info(opts)
      data
    end

    # Get videos of my user
    # @param [Hash] opts the optional parameters
    # @option opts [Integer] :start Offset used to paginate results
    # @option opts [Integer] :count Number of items to return
    # @option opts [String] :sort Sort column
    # @return [Array<(VideoListResponse, Integer, Hash)>] VideoListResponse data, response status code and response headers
    def users_me_videos_get_with_http_info(opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: MyUserApi.users_me_videos_get ...'
      end
      if @api_client.config.client_side_validation && !opts[:'start'].nil? && opts[:'start'] < 0
        fail ArgumentError, 'invalid value for "opts[:"start"]" when calling MyUserApi.users_me_videos_get, must be greater than or equal to 0.'
      end

      if @api_client.config.client_side_validation && !opts[:'count'].nil? && opts[:'count'] > 100
        fail ArgumentError, 'invalid value for "opts[:"count"]" when calling MyUserApi.users_me_videos_get, must be smaller than or equal to 100.'
      end

      if @api_client.config.client_side_validation && !opts[:'count'].nil? && opts[:'count'] < 1
        fail ArgumentError, 'invalid value for "opts[:"count"]" when calling MyUserApi.users_me_videos_get, must be greater than or equal to 1.'
      end

      # resource path
      local_var_path = '/users/me/videos'

      # query parameters
      query_params = opts[:query_params] || {}
      query_params[:'start'] = opts[:'start'] if !opts[:'start'].nil?
      query_params[:'count'] = opts[:'count'] if !opts[:'count'].nil?
      query_params[:'sort'] = opts[:'sort'] if !opts[:'sort'].nil?

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      # return_type
      return_type = opts[:return_type] || 'VideoListResponse' 

      # auth_names
      auth_names = opts[:auth_names] || ['OAuth2']

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:GET, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: MyUserApi#users_me_videos_get\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # Get video imports of my user
    # @param [Hash] opts the optional parameters
    # @option opts [Integer] :start Offset used to paginate results
    # @option opts [Integer] :count Number of items to return (default to 15)
    # @option opts [String] :sort Sort column
    # @return [VideoImport]
    def users_me_videos_imports_get(opts = {})
      data, _status_code, _headers = users_me_videos_imports_get_with_http_info(opts)
      data
    end

    # Get video imports of my user
    # @param [Hash] opts the optional parameters
    # @option opts [Integer] :start Offset used to paginate results
    # @option opts [Integer] :count Number of items to return
    # @option opts [String] :sort Sort column
    # @return [Array<(VideoImport, Integer, Hash)>] VideoImport data, response status code and response headers
    def users_me_videos_imports_get_with_http_info(opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: MyUserApi.users_me_videos_imports_get ...'
      end
      if @api_client.config.client_side_validation && !opts[:'start'].nil? && opts[:'start'] < 0
        fail ArgumentError, 'invalid value for "opts[:"start"]" when calling MyUserApi.users_me_videos_imports_get, must be greater than or equal to 0.'
      end

      if @api_client.config.client_side_validation && !opts[:'count'].nil? && opts[:'count'] > 100
        fail ArgumentError, 'invalid value for "opts[:"count"]" when calling MyUserApi.users_me_videos_imports_get, must be smaller than or equal to 100.'
      end

      if @api_client.config.client_side_validation && !opts[:'count'].nil? && opts[:'count'] < 1
        fail ArgumentError, 'invalid value for "opts[:"count"]" when calling MyUserApi.users_me_videos_imports_get, must be greater than or equal to 1.'
      end

      # resource path
      local_var_path = '/users/me/videos/imports'

      # query parameters
      query_params = opts[:query_params] || {}
      query_params[:'start'] = opts[:'start'] if !opts[:'start'].nil?
      query_params[:'count'] = opts[:'count'] if !opts[:'count'].nil?
      query_params[:'sort'] = opts[:'sort'] if !opts[:'sort'].nil?

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      # return_type
      return_type = opts[:return_type] || 'VideoImport' 

      # auth_names
      auth_names = opts[:auth_names] || ['OAuth2']

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:GET, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: MyUserApi#users_me_videos_imports_get\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    # Get rate of my user for a video
    # @param video_id [String] The video id 
    # @param [Hash] opts the optional parameters
    # @return [GetMeVideoRating]
    def users_me_videos_video_id_rating_get(video_id, opts = {})
      data, _status_code, _headers = users_me_videos_video_id_rating_get_with_http_info(video_id, opts)
      data
    end

    # Get rate of my user for a video
    # @param video_id [String] The video id 
    # @param [Hash] opts the optional parameters
    # @return [Array<(GetMeVideoRating, Integer, Hash)>] GetMeVideoRating data, response status code and response headers
    def users_me_videos_video_id_rating_get_with_http_info(video_id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: MyUserApi.users_me_videos_video_id_rating_get ...'
      end
      # verify the required parameter 'video_id' is set
      if @api_client.config.client_side_validation && video_id.nil?
        fail ArgumentError, "Missing the required parameter 'video_id' when calling MyUserApi.users_me_videos_video_id_rating_get"
      end
      # resource path
      local_var_path = '/users/me/videos/{videoId}/rating'.sub('{' + 'videoId' + '}', CGI.escape(video_id.to_s))

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      # return_type
      return_type = opts[:return_type] || 'GetMeVideoRating' 

      # auth_names
      auth_names = opts[:auth_names] || ['OAuth2']

      new_options = opts.merge(
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type
      )

      data, status_code, headers = @api_client.call_api(:GET, local_var_path, new_options)
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: MyUserApi#users_me_videos_video_id_rating_get\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
