=begin
#PeerTube

## Introduction  The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [Quick Start guide](https://docs.joinpeertube.org/#/api-rest-getting-started) so you can play with the PeerTube API.  # Authentication  When you sign up for an account, you are given the possibility to generate sessions, and authenticate using this session token. One session token can currently be used at a time.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/#/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call. The body of the response will be JSON in the following format.  ``` {   \"code\": \"unauthorized_request\", // example inner error code   \"error\": \"Token is invalid.\" // example exposed error message } ``` 

The version of the OpenAPI document: 2.3.0

Generated by: https://openapi-generator.tech
OpenAPI Generator version: 4.3.1

=end

require 'spec_helper'
require 'json'

# Unit tests for Peertube::MyNotificationsApi
# Automatically generated by openapi-generator (https://openapi-generator.tech)
# Please update as you see appropriate
describe 'MyNotificationsApi' do
  before do
    # run before each test
    @api_instance = Peertube::MyNotificationsApi.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of MyNotificationsApi' do
    it 'should create an instance of MyNotificationsApi' do
      expect(@api_instance).to be_instance_of(Peertube::MyNotificationsApi)
    end
  end

  # unit tests for users_me_notification_settings_put
  # Update my notification settings
  # @param [Hash] opts the optional parameters
  # @option opts [InlineObject3] :inline_object3 
  # @return [nil]
  describe 'users_me_notification_settings_put test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for users_me_notifications_get
  # List my notifications
  # @param [Hash] opts the optional parameters
  # @option opts [Boolean] :unread only list unread notifications
  # @option opts [Integer] :start Offset used to paginate results
  # @option opts [Integer] :count Number of items to return
  # @option opts [String] :sort Sort column
  # @return [NotificationListResponse]
  describe 'users_me_notifications_get test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for users_me_notifications_read_all_post
  # Mark all my notification as read
  # @param [Hash] opts the optional parameters
  # @return [nil]
  describe 'users_me_notifications_read_all_post test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for users_me_notifications_read_post
  # Mark notifications as read by their id
  # @param [Hash] opts the optional parameters
  # @option opts [InlineObject2] :inline_object2 
  # @return [nil]
  describe 'users_me_notifications_read_post test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end
