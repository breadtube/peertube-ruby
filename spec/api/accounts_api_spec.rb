=begin
#PeerTube

## Introduction  The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [Quick Start guide](https://docs.joinpeertube.org/#/api-rest-getting-started) so you can play with the PeerTube API.  # Authentication  When you sign up for an account, you are given the possibility to generate sessions, and authenticate using this session token. One session token can currently be used at a time.  ## Roles  Accounts are given permissions based on their role. There are three roles on PeerTube: Administrator, Moderator, and User. See the [roles guide](https://docs.joinpeertube.org/#/admin-managing-users?id=roles) for a detail of their permissions.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call. The body of the response will be JSON in the following format.  ``` {   \"code\": \"unauthorized_request\", // example inner error code   \"error\": \"Token is invalid.\" // example exposed error message } ``` 

The version of the OpenAPI document: 2.3.0

Generated by: https://openapi-generator.tech
OpenAPI Generator version: 4.3.1

=end

require 'spec_helper'
require 'json'

# Unit tests for Peertube::AccountsApi
# Automatically generated by openapi-generator (https://openapi-generator.tech)
# Please update as you see appropriate
describe 'AccountsApi' do
  before do
    # run before each test
    @api_instance = Peertube::AccountsApi.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of AccountsApi' do
    it 'should create an instance of AccountsApi' do
      expect(@api_instance).to be_instance_of(Peertube::AccountsApi)
    end
  end

  # unit tests for accounts_get
  # List accounts
  # @param [Hash] opts the optional parameters
  # @option opts [Integer] :start Offset used to paginate results
  # @option opts [Integer] :count Number of items to return
  # @option opts [String] :sort Sort column
  # @return [Array<Account>]
  describe 'accounts_get test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for accounts_name_get
  # Get an account
  # @param name The name of the account
  # @param [Hash] opts the optional parameters
  # @return [Account]
  describe 'accounts_name_get test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for accounts_name_ratings_get
  # List ratings of an account
  # @param name The name of the account
  # @param [Hash] opts the optional parameters
  # @option opts [Integer] :start Offset used to paginate results
  # @option opts [Integer] :count Number of items to return
  # @option opts [String] :sort Sort column
  # @option opts [String] :rating Optionally filter which ratings to retrieve
  # @return [Array<VideoRating>]
  describe 'accounts_name_ratings_get test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for accounts_name_video_channels_get
  # List video channels of an account
  # @param name The name of the account
  # @param [Hash] opts the optional parameters
  # @option opts [Boolean] :with_stats include view statistics for the last 30 days (only if authentified as the account user)
  # @option opts [Integer] :start Offset used to paginate results
  # @option opts [Integer] :count Number of items to return
  # @option opts [String] :sort Sort column
  # @return [Array<VideoChannel>]
  describe 'accounts_name_video_channels_get test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for accounts_name_videos_get
  # List videos of an account
  # @param name The name of the account
  # @param [Hash] opts the optional parameters
  # @option opts [OneOfintegerarray] :category_one_of category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get))
  # @option opts [OneOfstringarray] :tags_one_of tag(s) of the video
  # @option opts [OneOfstringarray] :tags_all_of tag(s) of the video, where all should be present in the video
  # @option opts [OneOfintegerarray] :licence_one_of licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get))
  # @option opts [OneOfstringarray] :language_one_of language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language
  # @option opts [String] :nsfw whether to include nsfw videos, if any
  # @option opts [String] :filter Special filters (local for instance) which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges) 
  # @option opts [String] :skip_count if you don&#39;t need the &#x60;total&#x60; in the response
  # @option opts [Integer] :start Offset used to paginate results
  # @option opts [Integer] :count Number of items to return
  # @option opts [String] :sort Sort videos by criteria
  # @return [VideoListResponse]
  describe 'accounts_name_videos_get test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end
